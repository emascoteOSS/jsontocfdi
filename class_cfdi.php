<?
/**
 * @author Miguel A. Lopez Gayosso
 * Fecha: 01 de Noviembre 2017
 * Se modifica y actualiza el archivo anteriormente llamado funciones_facturacion_electronica_cfdi.php que anteriormente
 * personalizado para cada instancia y se centraliza a esta clase/objeto (un archivo por servidor) pudiendo personalizar
 * por medio de archivos las funciones propias de cada proveedor de timbrado (PAC por lo pronto estan DELFENO y DIVERZA)
 * asi como tambien se prepara esta clase para poder cargar archivos personalizado donde se definan las ADDENDAS para
 * cada innstancia.
 *
 **/
class CFDi
{
  var $VERSION;
  var $PAC;
  var $CSD;
  var $URL_WEBSERVICE;
  var $MULTIMONEDA;
  var $MONEDA_DEFAULT;
  var $MODO_OPERACION;
  var $EMISOR;
  var $RECEPTOR;
  var $TIPO_DOCUMENTO;
  var $CONF_DOCUMENTO;
  var $DOCUMENTO_PADRE;
  var $ID_SUCURSAL_ACTIVA;
  var $ES_DOCUMENTO_ELECTRONICO;
  var $PATH_CERTIFICADOS;
  var $CONF_IMPUESTOS;
  var $USAR_COMERCIO_EXTERIOR;
  var $DATOS_EMISOR_NODO_CE;

  function __construct($e_DOC_ID_PADRE)
  {
    $this->ES_DOCUMENTO_ELECTRONICO = false;
    $this->DOCUMENTO_PADRE          = new stdClass();

    if( 'MyISAM' == ($GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'ENGINE', "SELECT ENGINE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = 'sys_facturacion_electronica'")) )
    {
      $this->USAR_COMERCIO_EXTERIOR   = ( $this->obtener_configuracion('facturacion_electronica', 'UTILIZA_NODO_COMERCIO_EXTERIOR') == 'SI');

      $e_CONF_DOCTO_FACTURA  = $this->obtener_configuracion('facturacion_electronica', 'DOCUMENTO_FACTURA');
      $m_CONF_DOCTO_FACTURA  = $this->obtener_attributes_nodoxml( $e_CONF_DOCTO_FACTURA );

      if( $m_CONF_DOCTO_FACTURA['DOC_ID'] == $e_DOC_ID_PADRE )
      {
        $this->TIPO_DOCUMENTO                                    = 'FACTURA';
        $this->CONF_DOCUMENTO['FACTURA']                         = $m_CONF_DOCTO_FACTURA;
        $this->CONF_DOCUMENTO['FACTURA']['TIPO_COMPROBANTE']     =  Utils::catalogo_obtener_atributo('sys_documentos', 'DOC_TIPO_COMPROBANTE', 'DOC_ID', $this->CONF_DOCUMENTO['FACTURA']['DOC_ID']);
        $this->CONF_DOCUMENTO['FACTURA']['LISTA_TIPOS_RELACION'] = "'04','07'";
        $this->ES_DOCUMENTO_ELECTRONICO  = true;

        $m_SERIES_FACTURAS = unserialize( $this->obtener_configuracion('facturacion_electronica', 'SERIES_DOCUMENTO_FACTURA' ) );
        foreach( $m_SERIES_FACTURAS as $e_KEY => $ID_SERIE )
        {
          $this->CONF_DOCUMENTO['FACTURA']['SERIES']["{$ID_SERIE}"] = $this->obtener_datos_una_serie( $ID_SERIE );
        }

        if ( $this->USAR_COMERCIO_EXTERIOR )
        {
          $e_DOMICILIO_COMERCIO_EXTERIOR = $this->obtener_configuracion('facturacion_electronica', 'DATOS_DOMICILIO_EMISOR_CE');
          $a_DOMICILIO_COMERCIO_EXTERIOR = json_decode($e_DOMICILIO_COMERCIO_EXTERIOR, true);

          $this->DATOS_EMISOR_NODO_CE['COLONIA']   = $a_DOMICILIO_COMERCIO_EXTERIOR['COLONIA'];
          $this->DATOS_EMISOR_NODO_CE['LOCALIDAD'] = $a_DOMICILIO_COMERCIO_EXTERIOR['LOCALIDAD'];
          $this->DATOS_EMISOR_NODO_CE['MUNICIPIO'] = $a_DOMICILIO_COMERCIO_EXTERIOR['MUNICIPIO'];
        }
      }
      else
      {
        $e_CONF_DOCTO_CDP    = $this->obtener_configuracion('facturacion_electronica', 'DOCUMENTO_COMPROBANTE_PAGO');
        $m_CONF_DOCTO_CDP    = $this->obtener_attributes_nodoxml( $e_CONF_DOCTO_CDP );

        if( $m_CONF_DOCTO_CDP['DOC_ID'] == $e_DOC_ID_PADRE )
        {
          $this->TIPO_DOCUMENTO                            = 'CDP';
          $this->CONF_DOCUMENTO['CDP']                     = $m_CONF_DOCTO_CDP;
          $this->CONF_DOCUMENTO['CDP']['TIPO_COMPROBANTE'] =  Utils::catalogo_obtener_atributo('sys_documentos', 'DOC_TIPO_COMPROBANTE', 'DOC_ID', $this->CONF_DOCUMENTO['CDP']['DOC_ID']);
          $this->ES_DOCUMENTO_ELECTRONICO                  = true;

          $m_SERIES_CDP = unserialize( $this->obtener_configuracion('facturacion_electronica', 'SERIES_DOCUMENTO_COMPROBANTE_PAGO' ) );
          foreach( $m_SERIES_CDP as $e_KEY => $ID_SERIE )
          {
            $this->CONF_DOCUMENTO['CDP']['SERIES']["{$ID_SERIE}"] = $this->obtener_datos_una_serie( $ID_SERIE );
          }
        }
        else
        {
          $e_CONF_DOCTO_NDC  = $this->obtener_configuracion( 'facturacion_electronica', 'DOCUMENTO_NOTA_CREDITO' );
          $m_CONF_DOCTO_NDC  = $this->obtener_attributes_nodoxml( $e_CONF_DOCTO_NDC );
          if ( $m_CONF_DOCTO_NDC['DOC_ID'] == $e_DOC_ID_PADRE )
          {
            $this->TIPO_DOCUMENTO                                = 'NDC';
            $this->CONF_DOCUMENTO['NDC']                         = $m_CONF_DOCTO_NDC;
            $this->CONF_DOCUMENTO['NDC']['TIPO_COMPROBANTE']     =  Utils::catalogo_obtener_atributo('sys_documentos', 'DOC_TIPO_COMPROBANTE', 'DOC_ID', $this->CONF_DOCUMENTO['NDC']['DOC_ID']);
            $this->CONF_DOCUMENTO['NDC']['LISTA_TIPOS_RELACION'] = "'01','03','04'";
            $this->ES_DOCUMENTO_ELECTRONICO                      = true;

            $m_SERIES_NDC = unserialize( $this->obtener_configuracion('facturacion_electronica', 'SERIES_DOCUMENTO_NOTA_CREDITO' ) );
            foreach( $m_SERIES_NDC as $e_KEY => $ID_SERIE )
            {
              $this->CONF_DOCUMENTO['NDC']['SERIES']["{$ID_SERIE}"] = $this->obtener_datos_una_serie( $ID_SERIE );
            }
          }
          else
          {
            $e_CONF_DOCTO_NCC = $this->obtener_configuracion( 'facturacion_electronica', 'DOCUMENTO_NOTA_CARGO' );
            $m_CONF_DOCTO_NCC = $this->obtener_attributes_nodoxml( $e_CONF_DOCTO_NCC );
            if ( $m_CONF_DOCTO_NCC['DOC_ID'] == $e_DOC_ID_PADRE )
            {
              $this->TIPO_DOCUMENTO                                = 'NCC';
              $this->CONF_DOCUMENTO['NCC']                         = $m_CONF_DOCTO_NCC;
              $this->CONF_DOCUMENTO['NCC']['TIPO_COMPROBANTE']     =  Utils::catalogo_obtener_atributo('sys_documentos', 'DOC_TIPO_COMPROBANTE', 'DOC_ID', $this->CONF_DOCUMENTO['NCC']['DOC_ID']);
              $this->CONF_DOCUMENTO['NCC']['LISTA_TIPOS_RELACION'] = "'04','07'";
              $this->ES_DOCUMENTO_ELECTRONICO                      = true;

              $m_SERIES_NCC = unserialize( $this->obtener_configuracion('facturacion_electronica', 'SERIES_DOCUMENTO_NOTA_CARGO' ) );
              foreach( $m_SERIES_NCC as $e_KEY => $ID_SERIE )
              {
                $this->CONF_DOCUMENTO['NCC']['SERIES']["{$ID_SERIE}"] = $this->obtener_datos_una_serie( $ID_SERIE );
              }
            }
            else
            {
              $e_CONF_DOCTO_NOMINA= $this->obtener_configuracion( 'facturacion_electronica', 'DOCUMENTO_RECIBO_NOMINA' );
              $m_CONF_DOCTO_NOMINA= $this->obtener_attributes_nodoxml( $e_CONF_DOCTO_NOMINA );
              if ($m_CONF_DOCTO_NOMINA['DOC_ID'] == $e_DOC_ID_PADRE)
              {
                $this->TIPO_DOCUMENTO                                = 'NOMINA';
                $this->CONF_DOCUMENTO['NOMINA']                         = $m_CONF_DOCTO_NOMINA;
                $this->CONF_DOCUMENTO['NOMINA']['TIPO_COMPROBANTE']     =  Utils::catalogo_obtener_atributo('sys_documentos', 'DOC_TIPO_COMPROBANTE', 'DOC_ID', $this->CONF_DOCUMENTO['NOMINA']['DOC_ID']);
                $this->CONF_DOCUMENTO['NOMINA']['USOCFDI'] = "P01";
                $this->ES_DOCUMENTO_ELECTRONICO = true;

                $m_SERIES_NOMINA = unserialize( $this->obtener_configuracion('facturacion_electronica', 'SERIES_DOCUMENTO_RECIBO_NOMINA' ) );
                foreach($m_SERIES_NOMINA as $e_KEY => $ID_SERIE)
                {
                  $this->CONF_DOCUMENTO['NOMINA']['SERIES']["{$ID_SERIE}"] = $this->obtener_datos_una_serie( $ID_SERIE );
                }
              }
            }
          }
        }
      }

      if( $this->ES_DOCUMENTO_ELECTRONICO )
      {
        $this->PAC               = $this->obtener_configuracion( 'facturacion_electronica', 'PROVEEDOR' );
        $this->VERSION           = $this->obtener_configuracion( 'facturacion_electronica', 'VERSION' );
        $this->URL_WEBSERVICE    = $this->obtener_configuracion( 'facturacion_electronica', 'RUTAWEBSERVICE' );
        $this->MULTIMONEDA       =($this->obtener_configuracion( 'facturacion_electronica', 'MULTIMONEDAS' ) == 'SI');
        $this->OBTENER_UNIDAD    = $this->obtener_configuracion( 'facturacion_electronica', 'OBTENER_UNIDAD' );

        if( $this->obtener_configuracion( 'facturacion_electronica', 'UTILIZA_ADDENDA') == 'SI' )
        {
          $this->ADDENDAS = array
          (
            'UTILIZA_ADDENDA' => $this->obtener_configuracion( 'facturacion_electronica', 'UTILIZA_ADDENDA'),
            'CAMPO_ADDENDA'   => $this->obtener_configuracion( 'facturacion_electronica', 'CAMPO_ADDENDA' )
          );
        }

        $this->MODO_OPERACION    = $this->obtener_configuracion( 'facturacion_electronica', 'MOD_OPERACION' );
        $this->PATH_CERTIFICADOS = "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}bin/operacion/certificados";
        $this->EMISOR            = array();

        $m_SUCURSAL_ACTIVA                                                    = $GLOBALS[SYSTEM][DBS][DB]->get_array( 'QUERY_UNASUCURSAL', $GLOBALS[SYSTEM][CURRENT][SUC_ID] );
        if ( $this->MODO_OPERACION == 'VARIAS_RAZONES_SOCIALES' )
        {
          $this->ID_SUCURSAL_ACTIVA                                           = $m_SUCURSAL_ACTIVA['SUC_ID'];
          $e_XML_CONF_SUCURSALES                                              = $this->obtener_configuracion( 'facturacion_electronica', "DATOS_FISCALES_SUC_{$this->ID_SUCURSAL_ACTIVA}" );
          $m_XML_CONF_SUCURSALES                                              = $this->obtener_data_all_nodos_xml( $e_XML_CONF_SUCURSALES );
          list( $e_CODIGO_REGIMEN_FISCAL, $e_REGIMEN_FISCAL )                 = explode( " - ", $m_XML_CONF_SUCURSALES[REGIMEN_FISCAL] );
          $m_XML_CONF_SUCURSALES[CODIGO_REGIMEN_FISCAL]                       = $e_CODIGO_REGIMEN_FISCAL;
          $m_XML_CONF_SUCURSALES[REGIMEN_FISCAL]                              = $e_REGIMEN_FISCAL;

          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]                        = $m_XML_CONF_SUCURSALES;
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][SUCURSAL_NOMBRE]       = $m_SUCURSAL_ACTIVA['SUC_NOMBRE'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][CALLE]                 = $m_SUCURSAL_ACTIVA['CAM_PER_DOMICILIO_A'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][NO_EXTERIOR]           = $m_SUCURSAL_ACTIVA['CAM_PER_DOMICILIO_B'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][NO_INTERIOR]           = $m_SUCURSAL_ACTIVA['CAM_PER_DOMICILIO_c'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][COLONIA]               = $m_SUCURSAL_ACTIVA['CAM_PER_COLONIA'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][CODIGO_POSTAL]         = $m_SUCURSAL_ACTIVA['CAM_PER_CP'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][TELEFONO]              = $m_SUCURSAL_ACTIVA['CAM_PER_TELEFONO'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][PAIS]                  = $m_SUCURSAL_ACTIVA['SUC_PAIS'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][ESTADO]                = $m_SUCURSAL_ACTIVA['SUC_ESTADO'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][MUNICIPIO]             = $m_SUCURSAL_ACTIVA['SUC_CIUDAD'];
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][SIGLA_PAIS]            = Utils::catalogo_obtener_atributo( 'cat_paises', 'PAI_ABREVIATURA', 'PAI_ID', $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][PAIS] );
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][NOMBRE_PAIS]           = Utils::catalogo_obtener_atributo( 'cat_paises', 'PAI_NOMBRE', 'PAI_ID', $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][PAIS] );
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][SIGLA_ESTADO]          = Utils::catalogo_obtener_atributo( 'cat_estados', 'EST_ABREVIATURA', 'EST_ID', $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][ESTADO] );
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][NOMBRE_ESTADO]         = Utils::catalogo_obtener_atributo( 'cat_estados', 'EST_NOMBRE', 'EST_ID', $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][ESTADO] );
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][SIGLA_CIUDAD]          = Utils::catalogo_obtener_atributo( 'cat_ciudades', 'CIU_ABREVIATURA', 'CIU_ID', $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][MUNICIPIO] );
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][NOMBRE_CIUDAD]         = Utils::catalogo_obtener_atributo( 'cat_ciudades', 'CIU_NOMBRE', 'CIU_ID', $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][MUNICIPIO] );
          $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"][LUGAR_EXPEDICION]      = $m_SUCURSAL_ACTIVA['CAM_PER_CP'];
        }
        else
        {
          $this->ID_SUCURSAL_ACTIVA                           = '0';
          $e_XML_DATOS_FISCALES                               = $this->obtener_configuracion( 'configuracion', 'DATOS_EMPRESA' );
          $this->EMISOR['0']                                  = $this->obtener_data_all_nodos_xml( $e_XML_DATOS_FISCALES );
          list( $e_CODIGO_REGIMEN_FISCAL, $e_REGIMEN_FISCAL ) = explode( " - ", $this->EMISOR['0'][REGIMEN_FISCAL] );

          $this->EMISOR['0'][CODIGO_REGIMEN_FISCAL]           = $e_CODIGO_REGIMEN_FISCAL;
          $this->EMISOR['0'][REGIMEN_FISCAL]                  = $e_REGIMEN_FISCAL;
          $this->EMISOR['0'][SUCURSAL_NOMBRE]                 = $m_SUCURSAL_ACTIVA['SUC_NOMBRE'];
          $this->EMISOR['0'][SUCURSAL_TELEFONO]               = $m_SUCURSAL_ACTIVA['CAM_PER_TELEFONO'];
          $this->EMISOR['0'][SUCURSAL_CALLE]                  = $m_SUCURSAL_ACTIVA['CAM_PER_DOMICILIO_A'];
          $this->EMISOR['0'][SUCURSAL_NO_EXTERIOR]            = $m_SUCURSAL_ACTIVA['CAM_PER_DOMICILIO_B'];
          $this->EMISOR['0'][SUCURSAL_NO_INTERIOR]            = $m_SUCURSAL_ACTIVA['CAM_PER_DOMICILIO_c'];
          $this->EMISOR['0'][SUCURSAL_CODIGO_POSTAL]          = $m_SUCURSAL_ACTIVA['CAM_PER_CP'];
          $this->EMISOR['0'][SUCURSAL_COLONIA]                = $m_SUCURSAL_ACTIVA['CAM_PER_COLONIA'];
          $this->EMISOR['0'][SUCURSAL_ESTADO]                 = Utils::catalogo_obtener_atributo( 'cat_estados', 'EST_NOMBRE', 'EST_ID', $m_SUCURSAL_ACTIVA['SUC_ESTADO'] );
          $this->EMISOR['0'][SUCURSAL_CIUDAD]                 = Utils::catalogo_obtener_atributo( 'cat_ciudades', 'CIU_NOMBRE', 'CIU_ID', $m_SUCURSAL_ACTIVA['SUC_CIUDAD'] );
          $this->EMISOR['0'][SIGLA_PAIS]                      = Utils::catalogo_obtener_atributo( 'cat_paises', 'PAI_ABREVIATURA', 'PAI_ID', $this->EMISOR['0'][PAIS] );
          $this->EMISOR['0'][NOMBRE_PAIS]                     = Utils::catalogo_obtener_atributo( 'cat_paises', 'PAI_NOMBRE', 'PAI_ID', $this->EMISOR['0'][PAIS] );
          $this->EMISOR['0'][SIGLA_ESTADO]                    = Utils::catalogo_obtener_atributo( 'cat_estados', 'EST_ABREVIATURA', 'EST_ID', $this->EMISOR['0'][ESTADO] );
          $this->EMISOR['0'][NOMBRE_ESTADO]                   = Utils::catalogo_obtener_atributo( 'cat_estados', 'EST_NOMBRE', 'EST_ID', $this->EMISOR['0'][ESTADO] );
          $this->EMISOR['0'][SIGLA_CIUDAD]                    = Utils::catalogo_obtener_atributo( 'cat_ciudades', 'CIU_ABREVIATURA', 'CIU_ID', $this->EMISOR['0'][MUNICIPIO] );
          $this->EMISOR['0'][NOMBRE_CIUDAD]                   = Utils::catalogo_obtener_atributo( 'cat_ciudades', 'CIU_NOMBRE', 'CIU_ID', $this->EMISOR['0'][MUNICIPIO] );
          $this->EMISOR['0'][LUGAR_EXPEDICION]                = $m_SUCURSAL_ACTIVA['CAM_PER_CP'];

          $e_XML_DATOS_CSD                                    = $this->obtener_configuracion( 'facturacion_electronica', 'DATOS_FISCALES' );
          $m_DATOS_CSD                                        = $this->obtener_attributes_nodoxml( $e_XML_DATOS_CSD );

          $this->EMISOR['0'][CERTIFICADO_ELECTRONICO]         = $m_DATOS_CSD[CERTIFICADO_ELECTRONICO];
          $this->EMISOR['0'][LLAVE_CERTIFICADO_ELECTRONICO]   = $m_DATOS_CSD[LLAVE_CERTIFICADO_ELECTRONICO];
          $this->EMISOR['0'][CERTIFICADO_PAC_ARCHIVO_PFX]     = $m_DATOS_CSD[CERTIFICADO_PAC_ARCHIVO_PFX];
        }

        $m_CONF_MONEDA = $GLOBALS['SYSTEM']['DBS']['DB']->get_array('MY_QUERY', "SELECT MON_ID, MON_NOMBRE, MON_ABREVIATURA, MON_SIMBOLO FROM cat_monedas where MON_DEFAULT = '1' AND MON_ACTIVO ='1'");
        $this->MONEDA_DEFAULT = array
        (
          'ID'      => $m_CONF_MONEDA['MON_ID'],
          'NOMBRE'  => $m_CONF_MONEDA['MON_NOMBRE'],
          'SIGLA'   => $m_CONF_MONEDA['MON_ABREVIATURA'],
          'SIMBOLO' => $m_CONF_MONEDA['MON_SIMBOLO'],
        );
        $this->CSD['CADENA_CERTIFICADO'] = $this->cadena_certificados_limpia("{$this->PATH_CERTIFICADOS}/{$this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['CERTIFICADO_ELECTRONICO']}");
      }
    }
  }

  public function cadena_certificados_limpia($e_STRING)
  {
    $e_STRING_CERTIFICADO = file_get_contents($e_STRING);
    $this->CSD            = openssl_x509_parse( openssl_x509_read( $e_STRING_CERTIFICADO ) );

    preg_match('/-----BEGIN .+-----(.+)-----END .+-----/msi', $e_STRING_CERTIFICADO, $m_MATCHES);
    $e_TMP = $m_MATCHES[1];
    $e_TMP = preg_replace('/\n/', '', $e_TMP);
    $e_TMP = preg_replace('/\r/', '', $e_TMP);
    return $e_TMP;
  }

  public function validar_uuid_documento()
  {
    $e_SIGLA_STATUS    = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_SIGLA'];
    $e_TABLA           = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_CAMPO_DOCUMENTO = "{$this->DOCUMENTO_PADRE->SIGLA}_DOCUMENTO";
    $e_CAMPO_UUID      = "{$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO']}_UUID";

    $e_CONTENIDO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field("MY_QUERY",$e_CAMPO_UUID, "SELECT {$e_CAMPO_UUID} FROM {$e_TABLA} WHERE {$e_CAMPO_DOCUMENTO} = '{$this->DOCUMENTO_PADRE->ID}'");
    preg_match('/UUID=["\']([A-Fa-f0-9\-]{36})["\']/',$e_CONTENIDO,$m_MATCHES);
    if($m_MATCHES[1])
      return true;
    else
      return false;
  }

  public function obtener_certificados($e_STRING)
  {
    $m_PATH = explode(".pem",$e_STRING);
    $e_CONTENIDO = file_get_contents($m_PATH[0]);
    return base64_encode($e_CONTENIDO);
  }

  public function validar_serie_documento_electronico( $e_ID_SERIE_DOCUMENTO )
  {
    return is_array( $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['SERIES']["{$e_ID_SERIE_DOCUMENTO}"] );
  }

  public function validar_documento_alguardar( $e_SIGLA_STATUS )
  {
    if( !$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->SIGLA )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = 'Debe de especificar la sigla del STATUS de este documento para ejecutar las validaciones';
      return false;
    }

    $e_VALIDACION = true;

    $m_CONFIGURACIONES_SUCURSAL = array
    (
      'CERTIFICADO_ELECTRONICO'       => 'El <b>archivo CSD</b> (Certificado Sellos Digitales archivo "cer") no est&aacute; configurado',
      'LLAVE_CERTIFICADO_ELECTRONICO' => 'El <b>archivo de la llave privada para el certificado</b> (archivo "key") no est&aacute; configurado',
      'CERTIFICADO_PAC_ARCHIVO_PFX'   => 'El <b>archivo PFX</b> o <b>Certificado del PAC</b> (archivo "pem") no est&aacute; configurado',
      'RFC'                           => 'El <b>RFC</b> del emisor no est&aacute; configurado',
      'SIGLA_PAIS'                    => 'El <b>PAIS</b> del emisor no est&aacute; configurado',
      'CODIGO_POSTAL'                 => 'El <b>CODIGO POSTAL</b> del emisor no est&aacute; configurado',
      'CODIGO_REGIMEN_FISCAL'         => 'El <b>REGIMEN FISCAL</b> del emisor no est&aacute; configurado',
    );

    foreach( $m_CONFIGURACIONES_SUCURSAL as $e_CONFIGURACION => $e_MENSAJE )
    {
      if( !$this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]["{$e_CONFIGURACION}"] )
      {
        $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = $e_MENSAJE;
        $e_VALIDACION = false;
      }
    }

    //Validar datos del Cliente/Receptor
    if( $e_VALIDACION )
      $e_VALIDACION = $this->validar_datos_receptor( $e_SIGLA_STATUS );

    //Validar el certificado de sellos electronicos
    if( $e_VALIDACION )
      $e_VALIDACION = $this->validar_certificado();

    if( !$this->URL_WEBSERVICE )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = 'El <b>URL del WebService</b> no est&aacute; configurado';
      $e_VALIDACION = false;
    }

    //Validar "PATTERN" del RFC
    if( !$this->valida_rfc( $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['RFC'] ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El RFC del emisor: <b>{$this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['RFC']}</b> no es v&aacute;lido, favor de corregir antes de continuar.";
      $e_VALIDACION = false;
    }

    //Validar codigopostal
    if( !$this->valida_codigo_postal( $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['CODIGO_POSTAL'] ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El <b>c&oacute;digo postal: {$this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['CODIGO_POSTAL']}</b> del <b>emisor</b> tiene una longitud incorrecta, deben ser 5 d&iacute;gitos";
      $e_VALIDACION = false;
    }

    //Validar Atributo "LugarDeExpedicion
    if( !$this->valida_codigo_postal( $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['LUGAR_EXPEDICION'] ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El <b>c&oacute;digo postal: {$this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['LUGAR_EXPEDICION']}</b> para el atributo <b>Lugar de expedici&oacute;n</b> tiene una longitud incorrecta, deben ser 5 d&iacute;gitos";
      $e_VALIDACION = false;
    }

    //Validar que el documento (si no es comprobante de pago/ingreso) al menos contenga una partida/detalla y que cada una de las partidas tengan un importe mayor a $0.00
    if( $this->TIPO_DOCUMENTO !== 'CDP' && $this->TIPO_DOCUMENTO !== 'NOMINA')
    {
      if( $e_VALIDACION )
      {
        if( $this->DOCUMENTO_PADRE->CONSOLIDADO )
        {
          $e_NOMBRE_TABLA_D        = $this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->CONF['TABLA_D'];
          $e_PREFIJO_TABLA_D       = $this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->CONF['PREFIJO_D'];
          $e_DETALLES_CONSOLIDADOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'DETALLES', "SELECT COUNT(*) AS DETALLES FROM {$e_NOMBRE_TABLA_D} WHERE {$e_PREFIJO_TABLA_D}_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'");
          if( !$e_DETALLES_CONSOLIDADOS )
          {
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El documento electr&oacute;nico <b>{$this->DOCUMENTO_PADRE->CONF[DOC_NOMBRE]}</b> no se puede guardar sin alguna partida en el detalle";
            $e_VALIDACION = false;
          }
        }
        elseif ( !$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DETALLES )
        {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El documento electr&oacute;nico <b>{$this->DOCUMENTO_PADRE->CONF[DOC_NOMBRE]}</b> no se puede guardar sin alguna partida en el detalle";
          $e_VALIDACION = false;
        }

        if( $e_VALIDACION )
        {

          if ( $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRECIO_SIN_DSCTO'] )
            $e_QUERY_IMPORTE_DETALLES = "SELECT {$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRODUCTO']} AS PRO_ID,  ( ( {$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CANTIDAD']} * {$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRECIO_SIN_DSCTO']} ) + {$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO_D']}_IMPUESTOS ) AS IMPORTE_CON_IMPUESTOS FROM {$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA_D']} WHERE {$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO_D']}_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'";
          else
            $e_QUERY_IMPORTE_DETALLES = "SELECT {$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRODUCTO']} AS PRO_ID,  ( ( {$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CANTIDAD']} * {$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRECIO_IMPORTE']} ) + {$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO_D']}_IMPUESTOS ) AS IMPORTE_CON_IMPUESTOS FROM {$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA_D']} WHERE {$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO_D']}_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'";

          $r_DETALLES = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_IMPORTE_DETALLES );

          if ( $r_DETALLES )
          {
            $e_GRAN_TOTAL = 0.00;
            $e_ERRORES    = count($GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR']);
            while ( $a_UNDETALLE = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_DETALLES ) )
            {
              $e_GRAN_TOTAL += $a_UNDETALLE['IMPORTE_CON_IMPUESTOS'];

              if ( $a_UNDETALLE['IMPORTE_CON_IMPUESTOS']  <= 0.00 )
              {
                $e_PRODUCTO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'DESCRIPCION', "SELECT CONCAT(PRO_NOMBRE, '( ' , PRO_CODIGO, ' )' ) AS DESCRIPCION FROM cat_productos where PRO_ID = '{$a_UNDETALLE['PRO_ID']}'");
                $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El importe del producto <b>{$e_PRODUCTO}</b> no es v&aacute;lido, los documentos electr&oacute;nicos no permiten partidas con importe menor o igual \$0.00";
              }
            }
            $e_VALIDACION = ( count($GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR']) == $e_ERRORES );

            if( $e_VALIDACION )
            {

              if( $e_GRAN_TOTAL > 9999999999.99 )
              {
                $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "NO puede generar documentos electr&oacute;nicos con importes mayores a <b>\$9,999,999,999.99</b>";
                $e_VALIDACION = false;
              }
            }
          }
        }
      }
    }
    else
    {
      if($this->TIPO_DOCUMENTO == 'NOMINA')
      {
        $m_METODO_Y_FORMA_PAGO = $this->calcular_forma_pago( $e_SIGLA_STATUS );
      }
      else
      {
        $m_METODO_Y_FORMA_PAGO = $this->calcular_forma_pago( $e_SIGLA_STATUS );
        if( $m_METODO_Y_FORMA_PAGO['FORMA_PAGO'] == '99' )
        {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del campo FormaDePagoP debe ser distitnto de 99";
          $e_VALIDACION = false;
        }
      }
    }

    return $e_VALIDACION;
  }

  public function timbrar_documento($e_SIGLA_STATUS)
  {
    require_once("{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}/include/lib/php/facturacion_electronica/" . strtolower( $this->PAC ) . ".php");

    $e_XML_SELLADO = $this->generar_cadena_xml( $e_SIGLA_STATUS);

    if( !$e_XML_SELLADO )
    {
      $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR][] = "No se ha podido generar la estructura del XML, favor de ponerse en contacto con el &aacute;rea de soporte";
      return false;
    }

    $m_RESULTADO = $this->validar_esquema( $e_XML_SELLADO );

    if( $m_RESULTADO['VALIDACION'] )
    {
      if( $e_NODO_TFD = obtener_nodo_tfd_pac( $e_XML_SELLADO, $this ) )
      {
        $e_CADENA_TIMBRE = base64_decode($e_NODO_TFD);
        $e_CADENA_XML    = $this->agregar_timbre_cadena_xml($e_XML_SELLADO, $e_CADENA_TIMBRE);

        if($this->ADDENDAS && $this->TIPO_DOCUMENTO == 'FACTURA')
        {
          // obtener el campo addenda
          $e_QUERY_OBTENER_ADDENDA = "SELECT {$this->ADDENDAS['CAMPO_ADDENDA']} FROM cat_clientes WHERE CLI_NOMBRE  = '{$this->RECEPTOR['NOMBRE']}' AND CLI_ACTIVO = '1'";
          $e_ADDENDA_VALOR = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', "{$this->ADDENDAS['CAMPO_ADDENDA']}", $e_QUERY_OBTENER_ADDENDA );
          if($e_ADDENDA_VALOR)
            $e_CADENA_XML = $this->obtener_addendas($e_SIGLA_STATUS,$e_ADDENDA_VALOR,$e_CADENA_XML);
        }

        $this->guardar_archivo_xml_documento( $e_CADENA_XML );
        //Guardar Campo uuid
        $this->set_uuid_documento( $e_CADENA_TIMBRE );
      }
      else
      {
        return false;
      }
    }
    else
    {
      $this->guardar_archivo_xml_documento( "Errores en la validacion del esquema 3.3: ---------------------------------------\n{$m_RESULTADO['ERRORES']}\n\n-------------------------------------------------------------------------------------------------\n\n{$e_XML_SELLADO}");
      return false;
    }
  }

  public function recuperar_timbre_documento($e_SIGLA_STATUS)
  {
    require_once("{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}/include/lib/php/facturacion_electronica/" . strtolower( $this->PAC ) . ".php");
    $e_XML_SELLADO = $this->generar_cadena_xml( $e_SIGLA_STATUS);

    if( !$e_XML_SELLADO )
    {
      $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR][] = "No se ha podido generar la estructura del XML, favor de ponerse en contacto con el &aacute;rea de soporte";
      return false;
    }

    $m_RESULTADO = $this->validar_esquema( $e_XML_SELLADO );

    if( $m_RESULTADO['VALIDACION'] )
    {
      if( $e_NODO_TFD = recuperar_nodo_tfd_pac( $e_XML_SELLADO, $this ) )
      {

        $e_CADENA_TIMBRE = base64_decode( $e_NODO_TFD );
        $e_CADENA_XML    = $this->agregar_timbre_cadena_xml( $e_XML_SELLADO, $e_CADENA_TIMBRE );

        if( $this->ADDENDAS )
        {
          // obtener el campo addenda
          $e_QUERY_OBTENER_ADDENDA = "SELECT {$this->ADDENDAS['CAMPO_ADDENDA']} FROM cat_clientes WHERE CLI_NOMBRE  = '{$this->RECEPTOR['NOMBRE']}' AND CLI_ACTIVO = '1'";
          $e_ADDENDA_VALOR         = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', "{$this->ADDENDAS['CAMPO_ADDENDA']}", $e_QUERY_OBTENER_ADDENDA );

          if( $e_ADDENDA_VALOR )
            $e_CADENA_XML = $this->obtener_addendas($e_SIGLA_STATUS,$e_ADDENDA_VALOR,$e_CADENA_XML);
        }

        $this->guardar_archivo_xml_documento( $e_CADENA_XML );
        //Guardar Campo uuid
        $this->set_uuid_documento( $e_CADENA_TIMBRE );
      }
      else
        return false;
    }
    else
    {
      $this->guardar_archivo_xml_documento( "Errores en la validacion del esquema 3.3: ---------------------------------------\n{$m_RESULTADO['ERRORES']}\n\n-------------------------------------------------------------------------------------------------\n\n{$e_XML_SELLADO}");
      return false;
    }
  }

  public function cancelar_timbre_documento($o_CFDi)
  {
    require_once("{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}/include/lib/php/facturacion_electronica/" . strtolower( $this->PAC ) . ".php");
    return cancelar_timbre_pac($o_CFDi);
  }

  # Nuevo esquema de cancelacion
  public function cancelar_timbre_documento33($o_CFDi)
  {
    require_once("{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}/include/lib/php/facturacion_electronica/" . strtolower( $this->PAC ) . ".php");
    $e_STRING_XML = $o_CFDi->leer_xml_documento();
    $o_XML = new DomDocument();
    $o_XML->loadXML($e_STRING_XML);

    foreach ($o_XML->getElementsByTagNameNS('http://www.sat.gob.mx/cfd/3', 'Comprobante') as $element)
      $e_TOTAL_DOCUMENTO = $element->getAttribute('Total');

    $e_SIGLA_STATUS    = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_SIGLA'];
    if($o_CFDi->TIPO_DOCUMENTO == 'NOMINA')
      $o_CFDi->obtener_datos_receptor_nomina($o_CFDi->DOCUMENTO_PADRE->ID);
    else
      $o_CFDi->obtener_datos_receptor( $o_CFDi->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$o_CFDi->CONF_DOCUMENTO["{$o_CFDi->TIPO_DOCUMENTO}"]['CLIENTE']}"] );

    return solicitar_cancelacion_timbre_pac($o_CFDi, $e_TOTAL_DOCUMENTO);
  }
  # Fin nuevo esquema de cancelacion

  public function generar_cadena_xml( $e_SIGLA_STATUS )
  {
    $e_CADENA_XML = $this->generar_cadena_xml_documento($e_SIGLA_STATUS);
    return $e_CADENA_XML;
  }

  public function guardar_archivo_xml_documento( $e_CADENA_XML )
  {
    $e_VALIDACION     = true;
    $e_ID_SERIE       = $this->DOCUMENTO_PADRE->DATA['DOC_DOC_SERIE'];
    $e_ID_DOC         = $this->DOCUMENTO_PADRE->ID;
    $e_PREFIJO        = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['SERIES']["{$e_ID_SERIE}"]['PREFIJO'];
    $e_FOLIO          = $this->DOCUMENTO_PADRE->DATA['DOC_DOC_FOLIO'];
    $e_NOMBRE_ARCHIVO = "XML_{$this->TIPO_DOCUMENTO}_CFDi_v33_{$e_PREFIJO}{$e_FOLIO}";

    if ( !strpos($GLOBALS[SYSTEM][CONFIG][PATH_PERSONAL_PRIVATE], 'archivos_klayfactory') )
    {
      $e_RUTA_ARCHIVO   = "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}tmp/DOCUMENTO_XML_{$e_ID_DOC}.xml";

      if ( file_exists( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}tmp/DOCUMENTO_XML_{$e_ID_DOC}.xml" ) )
        $f_ARCHIVO = fopen( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}tmp/DOCUMENTO_XML_{$e_ID_DOC}.xml", 'w' );
      else
        $f_ARCHIVO = fopen( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}tmp/DOCUMENTO_XML_{$e_ID_DOC}.xml", 'x+' );
    }
    else
    {
      $e_RUTA_ARCHIVO   = "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}DOCUMENTO_XML_{$e_ID_DOC}.xml";
      if ( file_exists( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}DOCUMENTO_XML_{$e_ID_DOC}.xml" ) )
        $f_ARCHIVO = fopen("{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}DOCUMENTO_XML_{$e_ID_DOC}.xml", 'w');
      else
        $f_ARCHIVO = fopen("{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}DOCUMENTO_XML_{$e_ID_DOC}.xml", 'x+');
    }
    fwrite( $f_ARCHIVO, $e_CADENA_XML );
    fclose( $f_ARCHIVO );

    if( $e_ID_ARCHIVO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'DOC_ARC_ID', "SELECT DOC_ARC_ID FROM doc_documentos_archivos WHERE DOC_ARC_NOMBRE='{$e_NOMBRE_ARCHIVO}'" ) )
      $o_DOC_ARCHIVO_FORM = new Form("{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}include/config/form/operacion/form_documentos_archivos.php", $e_ID_ARCHIVO);
    else
      $o_DOC_ARCHIVO_FORM = new Form("{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}include/config/form/operacion/form_documentos_archivos.php");

    if( $o_DOC_ARCHIVO_FORM->CONFIG['FORM_PREFIX'] == "DOC_ARC" )
    {
      $m_DATA_DOC_ARCHIVOS = array
      (
        'DOC_ARC_NOMBRE'    => $e_NOMBRE_ARCHIVO,
        'DOC_ARC_DOCUMENTO' => $e_ID_DOC
      );

      $o_DOC_ARCHIVO_FORM->set_data( $m_DATA_DOC_ARCHIVOS );
      if( ( !$o_DOC_ARCHIVO_FORM->ID ) && ( !$e_RUTA_ARCHIVO ) )
      {
        $GLOBALS['SYSTEM']['OUTPUT']['LOCAL']['ARCHIVO']['ERROR'][] = "No se pudo guardar el archivo";
        $e_VALIDACION = false;
      }

      if( !$o_DOC_ARCHIVO_FORM->verify_data() )
        $e_VALIDACION = false;

      if( $e_VALIDACION )
      {
        $DOC_ARC_ID  = $o_DOC_ARCHIVO_FORM->db_update();
        if( $e_RUTA_ARCHIVO )
        {
          if( !strpos($GLOBALS[SYSTEM][CONFIG][PATH_PERSONAL_PRIVATE], 'archivos_klayfactory') )
          {
            if ( !rename( $e_RUTA_ARCHIVO, "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}bin/operacion/archivos/{$DOC_ARC_ID}.xml" ) )
              $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR][] = "No se pudo guardar el Archivo";
          }
          else
          {
            if ( !rename( $e_RUTA_ARCHIVO, "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}{$DOC_ARC_ID}.xml" ) )
              $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR][] = "No se pudo guardar el Archivo";
          }
        }
      }
      else
      {
        $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR][] = "No se pudo guardar el archivo";
      }
      unset($o_DOC_ARCHIVO_FORM);
    }
  }

  public function set_uuid_documento( $e_NODO_TFD )
  {
    $e_SIGLA_STATUS    = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_SIGLA'];
    $e_TABLA           = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_CAMPO_DOCUMENTO = "{$this->DOCUMENTO_PADRE->SIGLA}_DOCUMENTO";
    $e_CAMPO_UUID      = "{$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO']}_UUID";

    if( $GLOBALS['SYSTEM']['DBS']['DB']->update( $e_TABLA, array( $e_CAMPO_UUID => $e_NODO_TFD ), $e_CAMPO_DOCUMENTO, $this->DOCUMENTO_PADRE->ID ) )
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['NOTICE'][] = "Se ha guardado correctamente el campo UUID";
    else
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se pudo guardar el campo UUID, favor de comunicarse con SOPORTE";
  }

  public function set_respuesta_cancelacion_documento()
  {
    return '';
  }

  public function obtener_datos_receptor($e_ID_CLIENTE)
  {
    if( $this->RECEPTOR['RAZON_SOCIAL'] != '' && $this->RECEPTOR['RFC'] != '' && $this->RECEPTOR['ID_PAIS'] != '' )
      return;

    $e_QUERY_UNCLIENTE  = "SELECT CLI_NOMBRE AS NOMBRE, CLI_RFC AS RFC, CLI_RAZON_SOCIAL AS RAZON_SOCIAL, CLI_PAIS AS ID_PAIS, CLI_CP AS COD_POSTAL,\n";
    $e_QUERY_UNCLIENTE .= "       CAM_PER_NUMREGIDTRIB AS NUMREGIDTRIB, CLI_DOMICILIOA AS CALLE_NUMERO, CLI_CIUDAD AS ID_CIUDAD, CLI_ESTADO AS ID_ESTADO,\n";
    $e_QUERY_UNCLIENTE .= "       CLI_CP AS COD_POSTAL, CLI_TELEFONO AS TELEFONO, CLI_DOMICILIOB AS COLONIA\n";
    $e_QUERY_UNCLIENTE .= "FROM cat_clientes where CLI_ID = '{$e_ID_CLIENTE}'";

    $this->RECEPTOR                  = $GLOBALS['SYSTEM']['DBS']['DB']->get_array('MY_QUERY', $e_QUERY_UNCLIENTE);
    $this->RECEPTOR['RFC']           = preg_replace("/([-]|[\.]|[|]|[_]|[[:space:]])/", "", $this->RECEPTOR['RFC']);
    $this->RECEPTOR['SIGLA_PAIS']    = Utils::catalogo_obtener_atributo('cat_paises',   'PAI_ABREVIATURA', 'PAI_ID', $this->RECEPTOR['ID_PAIS']);
    $this->RECEPTOR['NOMBRE_PAIS']   = Utils::catalogo_obtener_atributo('cat_paises',   'PAI_NOMBRE',      'PAI_ID', $this->RECEPTOR['ID_PAIS']);
    $this->RECEPTOR['SIGLA_ESTADO']  = Utils::catalogo_obtener_atributo('cat_estados',  'EST_ABREVIATURA', 'EST_ID', $this->RECEPTOR['ID_ESTADO']);
    $this->RECEPTOR['NOMBRE_ESTADO'] = Utils::catalogo_obtener_atributo('cat_estados',  'EST_NOMBRE',      'EST_ID', $this->RECEPTOR['ID_ESTADO']);
    $this->RECEPTOR['SIGLA_CIUDAD']  = Utils::catalogo_obtener_atributo('cat_ciudades', 'CIU_ABREVIATURA', 'CIU_ID', $this->RECEPTOR['ID_CIUDAD']);
    $this->RECEPTOR['NOMBRE_CIUDAD'] = Utils::catalogo_obtener_atributo('cat_ciudades', 'CIU_NOMBRE',      'CIU_ID', $this->RECEPTOR['ID_CIUDAD']);
  }

  public function obtener_datos_receptor_nomina($e_ID_TRABAJADOR)
  {
    $m_CAMPOS = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"];
    $e_CATALOGO = str_replace("cat_per_","", $m_CAMPOS['CATALOGOEMP']);
    $m_CAMPOS['PREFIJOCATALOGO'] = $GLOBALS[SYSTEM][DBS][DB]->get_field('MY_QUERY', "CAT_PER_PREFIJO", "SELECT CAT_PER_PREFIJO FROM sys_catalogos_personalizados WHERE CAT_PER_NOMBRE = '{$e_CATALOGO}'");

    $e_QUERY_UNTRABAJADOR  = "SELECT " . $m_CAMPOS['PREFIJOCATALOGO'] . "_ID AS ID_RECEPTOR, {$m_CAMPOS['NOMBREEMP']} AS NOMBRE, {$m_CAMPOS['NOMBREEMP']} AS RAZON_SOCIAL, {$m_CAMPOS['RFCEMP']} AS RFC, PAI_ID AS ID_PAIS, EST_ID AS ID_ESTADO\n";
    $e_QUERY_UNTRABAJADOR .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "\n";
    $e_QUERY_UNTRABAJADOR .= "INNER JOIN {$m_CAMPOS['CATALOGOEMP']} ON " . $m_CAMPOS['PREFIJOCATALOGO'] . "_ID = {$m_CAMPOS['CLIENTE']}\n";
    $e_QUERY_UNTRABAJADOR .= "LEFT JOIN cat_paises ON PAI_ID = " . $m_CAMPOS['PREFIJOCATALOGO'] . "_PAIS\n";
    $e_QUERY_UNTRABAJADOR .= "LEFT JOIN cat_estados ON EST_ID = " . $m_CAMPOS['PREFIJOCATALOGO'] . "_ESTADO\n";
    $e_QUERY_UNTRABAJADOR .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$e_ID_TRABAJADOR}'\n";


    $this->RECEPTOR = $GLOBALS['SYSTEM']['DBS']['DB']->get_array('MY_QUERY', $e_QUERY_UNTRABAJADOR);
    $this->RECEPTOR['RFC']           = preg_replace("/([-]|[\.]|[|]|[_]|[[:space:]])/", "", $this->RECEPTOR['RFC']);

    $this->RECEPTOR['SIGLA_PAIS']    = Utils::catalogo_obtener_atributo('cat_paises',   'PAI_ABREVIATURA', 'PAI_ID', $this->RECEPTOR['ID_PAIS']);
    $this->RECEPTOR['NOMBRE_PAIS']   = Utils::catalogo_obtener_atributo('cat_paises',   'PAI_NOMBRE',      'PAI_ID', $this->RECEPTOR['ID_PAIS']);
    $this->RECEPTOR['SIGLA_ESTADO']  = Utils::catalogo_obtener_atributo('cat_estados',  'EST_ABREVIATURA', 'EST_ID', $this->RECEPTOR['ID_ESTADO']);
    $this->RECEPTOR['NOMBRE_ESTADO'] = Utils::catalogo_obtener_atributo('cat_estados',  'EST_NOMBRE',      'EST_ID', $this->RECEPTOR['ID_ESTADO']);
   }

  public function leer_xml_documento()
  {
    $e_ID_SERIE       = $this->DOCUMENTO_PADRE->DATA['DOC_DOC_SERIE'];
    $e_PREFIJO        = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['SERIES']["{$e_ID_SERIE}"]['PREFIJO'];
    $e_FOLIO          = $this->DOCUMENTO_PADRE->DATA['DOC_DOC_FOLIO'];
    $e_NOMBRE_ARCHIVO = "XML_{$this->TIPO_DOCUMENTO}_CFDi_v33_{$e_PREFIJO}{$e_FOLIO}";
    $e_DOC_ARC_ID     = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'DOC_ARC_ID', "SELECT DOC_ARC_ID FROM doc_documentos_archivos WHERE DOC_ARC_DOCUMENTO='{$this->DOCUMENTO_PADRE->ID}' AND DOC_ARC_NOMBRE = '{$e_NOMBRE_ARCHIVO}'");


    if( !strpos( $GLOBALS[SYSTEM][CONFIG][PATH_PERSONAL_PRIVATE], 'archivos_klayfactory' ) )
      $e_RUTA_ARCHIVO   = "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL']}bin/operacion/archivos/{$e_DOC_ARC_ID}.xml";
    else
      $e_RUTA_ARCHIVO   = "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}{$e_DOC_ARC_ID}.xml";

    //obtencion del contenido xml del archivo
    if( file_exists( $e_RUTA_ARCHIVO ) )
    {
      $r_ARCHIVO   = fopen($e_RUTA_ARCHIVO, "r");
      $e_CONTENIDO = fread($r_ARCHIVO, filesize($e_RUTA_ARCHIVO));
      fclose($r_ARCHIVO);
    }

    return $e_CONTENIDO;
  }

  public function generar_qrcode( $e_SELLO, $e_TOTAL_COMPROBANTE, $e_UUID )
  {
    require_once("{$GLOBALS[SYSTEM][CONFIG][PATH_UTILS]}stx/tool/barcode_generator/a-qr-code/a-qr-code.php");

    //Ultimos 8 digitos del atributo sello del nodo comprobante
    $e_SELLO_COMPROBANTE  = substr($e_SELLO,-8);
    $e_ARCHIVO_IMAGEN_QR  = "qrCode_{$this->DOCUMENTO_PADRE->ID}";
    $e_CADENA_QR_CODE     = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx?re=' . $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['RFC'] . '&rr=' . $this->RECEPTOR['RFC'] . '&tt=' . number_format($e_TOTAL_COMPROBANTE, 6, '.', '') . '&id=' . $e_UUID . '&fe=' . $e_SELLO_COMPROBANTE;
    aQRCode($e_CADENA_QR_CODE, $e_ARCHIVO_IMAGEN_QR, 'M', 'J', '3');
  }

  public function generar_cadena_original_para_complemento_sat()
  {
    $e_ID_SERIE       = $this->DOCUMENTO_PADRE->DATA['DOC_DOC_SERIE'];
    $e_PREFIJO        = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['SERIES']["{$e_ID_SERIE}"]['PREFIJO'];
    $e_FOLIO          = $this->DOCUMENTO_PADRE->DATA['DOC_DOC_FOLIO'];
    $e_NOMBRE_ARCHIVO = "XML_{$this->TIPO_DOCUMENTO}_CFDi_v33_{$e_PREFIJO}{$e_FOLIO}";
    $ID_ARCHIVO       = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'DOC_ARC_ID', "SELECT DOC_ARC_ID FROM doc_documentos_archivos WHERE DOC_ARC_DOCUMENTO='{$this->DOCUMENTO_PADRE->ID}' AND DOC_ARC_NOMBRE = '{$e_NOMBRE_ARCHIVO}'");

    if(!strpos($GLOBALS[SYSTEM][CONFIG][PATH_PERSONAL_PRIVATE], 'archivos_klayfactory'))
      $e_RUTA_ARCHIVO   = "{$GLOBALS[SYSTEM][CONFIG][PATH_PERSONAL]}bin/operacion/archivos/{$ID_ARCHIVO}.xml";
    else
      $e_RUTA_ARCHIVO = "{$GLOBALS['SYSTEM']['CONFIG']['PATH_PERSONAL_PRIVATE']}{$ID_ARCHIVO}.xml";

    $xml = new SimpleXMLElement ($e_RUTA_ARCHIVO,null,true);

    $xml->registerXPathNamespace('tfd', 'http://www.sat.gob.mx/TimbreFiscalDigital');

    $m_TIMBRE       = $xml->xpath ('//cfdi:Comprobante//cfdi:Complemento//tfd:TimbreFiscalDigital');

    $e_VERSION      = $m_TIMBRE[0]->attributes()->Version;
    $e_UUID         = $m_TIMBRE[0]->attributes()->UUID;
    $e_FECHA        = $m_TIMBRE[0]->attributes()->FechaTimbrado;
    $e_SELLO        = $m_TIMBRE[0]->attributes()->SelloCFD;
    $e_NUM_CERT_SAT = $m_TIMBRE[0]->attributes()->NoCertificadoSAT;

    return "||{$e_VERSION}|{$e_UUID}|{$e_FECHA}|{$e_SELLO}|{$e_NUM_CERT_SAT}||";
  }

  public function obtener_configuracion($e_TABLA, $e_PARAMETRO)
  {
    $e_TABLA = strtolower($e_TABLA);
    $e_QUERY = "SELECT CON_VALOR FROM sys_{$e_TABLA} WHERE CON_PARAMETRO = '{$e_PARAMETRO}'";
    return $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'CON_VALOR', $e_QUERY);
  }

  /**
   * la siguiente funcion obtiene los decimales permitiodos para el documento segun hacienda
   * @param double $e_VALOR
   * @param int $e_CONF_DECIMALES
   * @return int $e_VALOR
   */
  public function formatear_decimales_hacienda( $e_VALOR, $e_CONF_DECIMALES = 6 )
  {
    $e_VALOR                         = round( $e_VALOR, $e_CONF_DECIMALES );
    list( $e_ENTEROS, $e_DECIMALES ) = explode('.', $e_VALOR);
    $e_DECIMALES                     = ( $e_DECIMALES == null ? 0 : $e_DECIMALES );
    $e_VALOR                         = abs($e_ENTEROS);
    /*if($e_VALOR == '-0')
      $e_VALOR = 0;*/
    
    if($e_CONF_DECIMALES)
      $e_VALOR                      .= '.' . str_pad( $e_DECIMALES, $e_CONF_DECIMALES, '0', STR_PAD_RIGHT );

    return $e_VALOR;
  }

  //se cambio la funcion a publica par autilizarla en la generacion de addendas
  public function remplazar_caracteres_xml( $e_CADENA, $e_ES_RFC = false )
  {
    $e_EXP    = "([\t]|[\n]|[|]|[[:space:]])";
    $e_CADENA = preg_replace($e_EXP, " ",$e_CADENA);
    if(!$e_ES_RFC)
      $e_CADENA = preg_replace('([&])', '&amp;', $e_CADENA);
    $e_CADENA = preg_replace('(["])', '&quot;',$e_CADENA);
    $e_CADENA = preg_replace('([<])', '&lt;',  $e_CADENA);
    $e_CADENA = preg_replace('([>])', '&gt;',  $e_CADENA);
    $e_CADENA = preg_replace("(['])",'&apos;', $e_CADENA);
    return trim($e_CADENA);
  }

  public function obtener_decimales_validos_documento($e_SIGLA_STATUS)
  {
    $e_TABLA_ENCABEZADO      = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_CAMPO_DOCUMENTO       = "{$this->DOCUMENTO_PADRE->SIGLA}_DOCUMENTO";
    $e_CAMPO_MONEDA          = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['MONEDA'];
    $e_QUERY_MON_ABREVIATURA = "SELECT MON_ABREVIATURA FROM {$e_TABLA_ENCABEZADO} JOIN cat_monedas ON MON_ID = {$e_CAMPO_MONEDA} WHERE {$e_CAMPO_DOCUMENTO} = '{$this->DOCUMENTO_PADRE->ID}'";
    $e_ABREVIATURA           = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'MON_ABREVIATURA', $e_QUERY_MON_ABREVIATURA);
    $e_DECIMALES             = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('QUERY_CFDI_MONEDA', 'DECIMALES', "AND MONEDA = '{$e_ABREVIATURA}'");

    return $e_DECIMALES ? $e_DECIMALES : 0;
  }

  public function validar_valor_uncatalogo_sat($e_CATALOGO, $e_CAMPO_BUSQUEDA, $e_VALOR)
  {
    $m_ENCONTRADO = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_array('MY_QUERY', "SELECT {$e_CAMPO_BUSQUEDA} FROM cat_cfdi_{$e_CATALOGO} WHERE {$e_CAMPO_BUSQUEDA} = '{$e_VALOR}'");

    return (is_array($m_ENCONTRADO));
  }

  /**
   * Seccion de funciones privadas/utilerias de acceso a la base de datos
   *
   * */
  private function generar_cadena_xml_documento( $e_SIGLA_STATUS )
  {
    $this->obtener_configuracion_impuestos();

    $e_DECIMALES_MONEDA_DOCUMENTO = $this->obtener_decimales_validos_documento($e_SIGLA_STATUS);
    $e_MAXIMO_DECIMALES_HACIENDA  = 6;
    $m_CONCEPTOS                  = $this->generar_elemento_conceptos($e_SIGLA_STATUS);

    if( $m_CONCEPTOS === false )
      return false;
    else
      $m_TOTALES_DOCUMENTO = array_pop( $m_CONCEPTOS );

    $m_COMPROBANTE = $this->generar_elemento_comprobante($e_SIGLA_STATUS);
    if( $m_COMPROBANTE === false )
      return false;

    $m_EMISOR = $this->generar_elemento_emisor();
    if( $m_EMISOR === false )
      return false;

    $m_RECEPTOR = $this->generar_elemento_receptor($e_SIGLA_STATUS);
    if( $m_RECEPTOR === false )
      return false;

    $m_DOCUMENTOS_RELACIONADOS = $this->generar_elemento_documentos_relacionados($e_SIGLA_STATUS);
    $e_TOTAL_DOCUMENTO         = ( $m_TOTALES_DOCUMENTO['SUBTOTAL'] - $m_TOTALES_DOCUMENTO['DESCUENTO'] ) + ( $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']['GRAN_TOTAL'] - $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']['GRAN_TOTAL'] );
    $e_CADENA_XML              = "<?xml version='1.0' encoding='UTF-8'?>";

    //--------------------- comprobante ---------------------
    $e_CADENA_XML   .= "\r\n<cfdi:Comprobante xmlns:cfdi='http://www.sat.gob.mx/cfd/3' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'";
	  $e_CADENA_XML   .=" xsi:schemaLocation='http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd'";

    $e_CADENA_XML   .= " Version='"             . $this->remplazar_caracteres_xml($m_COMPROBANTE['VERSION']) . "'";
    if( $m_COMPROBANTE['SERIE'] )
      $e_CADENA_XML .= " Serie='"               . $this->remplazar_caracteres_xml($m_COMPROBANTE['SERIE']) . "'";

    $e_CADENA_XML   .= " Folio='"               . $this->remplazar_caracteres_xml($m_COMPROBANTE['FOLIO']) . "'";
    $e_CADENA_XML   .= " Fecha='"               . $this->remplazar_caracteres_xml($m_COMPROBANTE['FECHA']) . "'";
    $e_CADENA_XML   .= " Sello=''";
    $e_CADENA_XML   .= " NoCertificado='"       . $this->remplazar_caracteres_xml($m_COMPROBANTE['NOCERTIFICADO']) . "'";
    $e_CADENA_XML   .= " Certificado='"         . $this->remplazar_caracteres_xml($m_COMPROBANTE['CERTIFICADO']) . "'";
    $e_CADENA_XML   .= " TipoDeComprobante='"   . $this->remplazar_caracteres_xml(($m_COMPROBANTE['TIPODECOMPROBANTE'])) . "'";
    $e_CADENA_XML   .= " LugarExpedicion='"     . $this->remplazar_caracteres_xml($m_COMPROBANTE['LUGAREXPEDICION']) . "'";

    if ( $this->TIPO_DOCUMENTO == 'CDP' )
    {
      $e_CADENA_XML .= " SubTotal='"            . "0'";
      $e_CADENA_XML .= " Total='"               . "0'";
      $e_CADENA_XML .= " Moneda='"              . "XXX'";
    }
    else
    {
      $e_CADENA_XML .= " MetodoPago='"          . $this->remplazar_caracteres_xml($m_COMPROBANTE['METODOPAGO']) . "'";
      $e_CADENA_XML .= " FormaPago='"           . $this->remplazar_caracteres_xml($m_COMPROBANTE['FORMAPAGO']) . "'";
      $e_CADENA_XML .= " Moneda='"              . $this->remplazar_caracteres_xml( $m_COMPROBANTE['MONEDA'] ). "'";

      if ( $m_TOTALES_DOCUMENTO['DESCUENTO'] > 0.0 )
      {
        $e_CADENA_XML .= " SubTotal='"          . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_TOTALES_DOCUMENTO['SUBTOTAL'],  $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
        $e_CADENA_XML .= " Descuento='"         . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_TOTALES_DOCUMENTO['DESCUENTO'], $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
        $e_CADENA_XML .= " Total='"             . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $e_TOTAL_DOCUMENTO,                $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
      }
      else
      {
        $e_CADENA_XML .= " SubTotal='"          . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_TOTALES_DOCUMENTO['SUBTOTAL'], $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
        $e_CADENA_XML .= " Total='"             . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $e_TOTAL_DOCUMENTO,               $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
      }

      if( $m_COMPROBANTE['CONDICIONESDEPAGO'] )
        $e_CADENA_XML.=" CondicionesDePago='"   . $this->remplazar_caracteres_xml($m_COMPROBANTE['CONDICIONESDEPAGO']) . "'";

      if( ( $m_COMPROBANTE['MONEDA'] != 'MXN' ) )
        $e_CADENA_XML .= " TipoCambio='"      . $this->remplazar_caracteres_xml($m_COMPROBANTE['TIPO_CAMBIO']) . "'";
    }

    $e_CADENA_XML .= ">";

    if( $m_DOCUMENTOS_RELACIONADOS )
    {
      $e_DOCUMENTO = $this->TIPO_DOCUMENTO;
      switch ( $e_DOCUMENTO )
      {
        case 'NDC':
          $e_TIPO_RELACION = $m_DOCUMENTOS_RELACIONADOS['TIPO_RELACION'];
          unset($m_DOCUMENTOS_RELACIONADOS['TIPO_RELACION']);
          break;

        case 'FACTURA':
          $e_TIPO_RELACION = $m_DOCUMENTOS_RELACIONADOS['TIPO_RELACION'];
          unset($m_DOCUMENTOS_RELACIONADOS['TIPO_RELACION']);
          break;
      }
      //--------------- CfdiRelacionados Fin --------------------------
      $e_CADENA_XML .=  "\r\n\t<cfdi:CfdiRelacionados";
      $e_CADENA_XML .=  " TipoRelacion='{$e_TIPO_RELACION}'";// ? DE DONDE Y COMO SE SE OBTIENE
      $e_CADENA_XML .=  " >";
      foreach( $m_DOCUMENTOS_RELACIONADOS['UUID_RELACIONADOS'] AS $m_UNDOCUMENTO_RELACIONADO )
      {
        $e_CADENA_XML .=  "\r\n\t\t<cfdi:CfdiRelacionado";
        $e_CADENA_XML .=  " UUID='"           . $this->remplazar_caracteres_xml($m_UNDOCUMENTO_RELACIONADO)."'";// ? DE DONDE Y COMO SE SE OBTIENE que pasa si no esta timbrado?
        $e_CADENA_XML .=  " />";
      }
      $e_CADENA_XML .=  "\r\n\t</cfdi:CfdiRelacionados>";
    }

    //--------------------- emisor ---------------------
    $e_CADENA_XML .= "\r\n\t<cfdi:Emisor";
    $e_CADENA_XML .= " Rfc='"                 . $this->remplazar_caracteres_xml( $m_EMISOR['RFC'] )           . "'";
    $e_CADENA_XML .= " Nombre='"              . $this->remplazar_caracteres_xml( $m_EMISOR['NOMBRE'] )        . "'";
    $e_CADENA_XML .= " RegimenFiscal='"       . $this->remplazar_caracteres_xml( $m_EMISOR['REGIMENFISCAL'] ) . "'";
    $e_CADENA_XML .= "/>";

    //--------------------- Receptor ---------------------
    $e_RFC         = str_replace(" ","",$m_RECEPTOR['RFC']);
    $e_CADENA_XML .= "\r\n\t<cfdi:Receptor";
    $e_CADENA_XML .= " Rfc='"                 . $this->remplazar_caracteres_xml( $e_RFC ) . "'";
    $e_CADENA_XML .= " UsoCFDI='"             . $this->remplazar_caracteres_xml( $m_RECEPTOR['USOCFDI'] ) . "'";


    if( trim( $m_RECEPTOR['NOMBRE'] ) )
      $e_CADENA_XML .= " Nombre='"            . $this->remplazar_caracteres_xml( $m_RECEPTOR['NOMBRE'] ) . "'";

    if ( $e_RFC == 'XEXX010101000' && $this->RECEPTOR['SIGLA_PAIS'] != "MEX" )
    {
      $e_CADENA_XML .= " ResidenciaFiscal='"  . $this->remplazar_caracteres_xml( $this->RECEPTOR['SIGLA_PAIS'] )   . "'";
      $e_CADENA_XML .= " NumRegIdTrib='"      . $this->remplazar_caracteres_xml( $this->RECEPTOR['NUMREGIDTRIB'] ) . "'";
    }

    $e_CADENA_XML .= "/>";

    //--------------------- conceptos ---------------------
    $e_CADENA_XML .= "\r\n\t<cfdi:Conceptos>";

    foreach( $m_CONCEPTOS as $m_PARTIDA )
    {
      $e_AGREGAR_CIERRE_NODO_CONCEPTO = false;
      $e_CADENA_XML   .= "\r\n\t\t<cfdi:Concepto";
      $e_CADENA_XML   .= " ClaveProdServ='"     . $this->remplazar_caracteres_xml( $m_PARTIDA['CLAVEPRODSERV'] )   ."'";

      if( $m_PARTIDA['NOIDENTIFICACION'] )
        $e_CADENA_XML .= " NoIdentificacion='" . $this->remplazar_caracteres_xml( $m_PARTIDA['NOIDENTIFICACION'] ) . "'";

      $e_CADENA_XML   .= " Cantidad='"          . $this->remplazar_caracteres_xml( $m_PARTIDA['CANTIDAD'] )        . "'";
      $e_CADENA_XML   .= " ClaveUnidad='"       . $this->remplazar_caracteres_xml( $m_PARTIDA['CLAVEUNIDAD'] )     . "'";
      $e_CADENA_XML   .= " Descripcion='"       . $this->remplazar_caracteres_xml( $m_PARTIDA['DESCRIPCION'] )     ."'";

      if($this->TIPO_DOCUMENTO == 'NOMINA')
      {
        $e_CADENA_XML .= " ValorUnitario='"     . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_PARTIDA['VALORUNITARIO'], $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
        $e_CADENA_XML .= " Importe='"           . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_PARTIDA['IMPORTE'], $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
      }
      else
      {
        $e_CADENA_XML   .= " ValorUnitario='"     . $this->remplazar_caracteres_xml( $m_PARTIDA['VALORUNITARIO'] )   . "'";
        $e_CADENA_XML   .= " Importe='"           . $this->remplazar_caracteres_xml( $m_PARTIDA['IMPORTE'] ) . "'";
      }

      if( $m_PARTIDA['DESCUENTO'] > 0.00 )
        $e_CADENA_XML .= " Descuento='"       . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_PARTIDA['DESCUENTO'], $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";

      $e_CADENA_XML   .= " #@->";
    
      if($m_PARTIDA['IMPUESTOS'])
      {
        $e_AGREGAR_CIERRE_NODO_CONCEPTO = true;
        $e_CADENA_XML .= "\r\n\t\t\t<cfdi:Impuestos>";

        $e_TRASLADOS = 1;
        foreach( $m_PARTIDA['IMPUESTOS'] as $a_UNIMPUESTO )
        {
          if( $a_UNIMPUESTO['TIPO'] == "T" )
          {
            if( $e_TRASLADOS == 1 )
            {
              $e_TRASLADOS   = 0;
              $e_CADENA_XML .= "\r\n\t\t\t\t<cfdi:Traslados>";
            }

            $e_CADENA_XML .= "\r\n\t\t\t\t\t<cfdi:Traslado";
            $e_CADENA_XML .= " Base='"        . $this->remplazar_caracteres_xml( $a_UNIMPUESTO['BASE'] ) . "'";
            $e_CADENA_XML .= " Impuesto='"    . $this->remplazar_caracteres_xml( $a_UNIMPUESTO['CLAVE_IMP'] ) . "'";
            $e_CADENA_XML .= " TipoFactor="   . "'Tasa'";
            $e_CADENA_XML .= " TasaOCuota='"  . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( ( $a_UNIMPUESTO['TASA'] / 100 ), $e_MAXIMO_DECIMALES_HACIENDA ) ) . "'";
            $e_CADENA_XML .= " Importe='"     . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $a_UNIMPUESTO['IMPORTE'], $e_DECIMALES_MONEDA_DOCUMENTO ) )       . "'";
            $e_CADENA_XML .= "/>";
          }
        }

        if( $e_TRASLADOS == 0 )
          $e_CADENA_XML .= "\r\n\t\t\t\t</cfdi:Traslados>";

        $e_RETENCIONES = 1; //variable usada para concatenar una sola vez la etiqueta <cfdi:Retenciones>
        foreach( $m_PARTIDA['IMPUESTOS'] as $a_UNIMPUESTO )
        {
          if( $a_UNIMPUESTO['TIPO'] != "T" )
          {
            if( $e_RETENCIONES == 1 )
            {
              $e_RETENCIONES = 0;
              $e_CADENA_XML .= "\r\n\t\t\t\t<cfdi:Retenciones>";
            }

            $e_CADENA_XML .= "\r\n\t\t\t\t\t<cfdi:Retencion";
            $e_CADENA_XML .= " Base='"        . $this->remplazar_caracteres_xml($a_UNIMPUESTO['BASE']) . "'";
            $e_CADENA_XML .= " Impuesto='"    . $this->remplazar_caracteres_xml($a_UNIMPUESTO['CLAVE_IMP']) . "'";
            $e_CADENA_XML .= " TipoFactor="   . "'Tasa'";
            $e_CADENA_XML .= " TasaOCuota='"  . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda(( $a_UNIMPUESTO['TASA'] / 100 ), $e_MAXIMO_DECIMALES_HACIENDA) ) . "'";
            $e_CADENA_XML .= " Importe='"     . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda($a_UNIMPUESTO['IMPORTE'], $e_DECIMALES_MONEDA_DOCUMENTO ) )      . "'";
            $e_CADENA_XML .= "/>";
          }
        }

        if( $e_RETENCIONES == 0 )
          $e_CADENA_XML .= "\r\n\t\t\t\t</cfdi:Retenciones>";

        $e_CADENA_XML .= "\r\n\t\t\t</cfdi:Impuestos>";
      }

      if ( $this->TIPO_DOCUMENTO != 'CDP' && $this->TIPO_DOCUMENTO != 'NOMINA')
      {
        // Validar si existe informacion aduanera
        if( isset( $m_PARTIDA['INFORMACIONADUANERA'] ) && !empty($m_PARTIDA['INFORMACIONADUANERA'] ) )
        {
          $e_AGREGAR_CIERRE_NODO_CONCEPTO = true;
          foreach( $m_PARTIDA['INFORMACIONADUANERA'] as $m_PEDIMENTOS )
          {
            $e_CADENA_XML .= "\r\n\t\t\t<cfdi:InformacionAduanera NumeroPedimento='" . $this->remplazar_caracteres_xml( $m_PEDIMENTOS['NUMERO'] ) . "' />";
          }
        }
      }

      if( $e_AGREGAR_CIERRE_NODO_CONCEPTO )
      {
        $e_CADENA_XML  = str_replace(' #@->',' >', $e_CADENA_XML);
        $e_CADENA_XML .= "\r\n\t\t</cfdi:Concepto>";
      }
      else
      {
        $e_CADENA_XML = str_replace(' #@->',' />', $e_CADENA_XML);
      }
    }
    $e_CADENA_XML .= "\r\n\t</cfdi:Conceptos>";

    //--------------------- COMPLEMENTO DE PAGO ---------------------
    if ( $this->TIPO_DOCUMENTO == 'CDP' )
    {
      $e_CADENA_XML .= "\r\n\t<cfdi:Complemento>";

      $m_DETALLE_PAGOS = $this->generar_elemento_pagos();
      if( $m_DETALLE_PAGOS )
      {
        $e_CADENA_XML .= "\r\n\t\t<pago10:Pagos xmlns:pago10='http://www.sat.gob.mx/Pagos' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd' Version='1.0'>";
        foreach ( $m_DETALLE_PAGOS as $m_UN_PAGO )
        {
          $m_FECHA_TS = explode(" ",$m_UN_PAGO['FECHA_TS_ORIGEN']);
          $e_FECHA_PAGO = $m_UN_PAGO['FECHA_ORIGEN']."T".$m_FECHA_TS[1];
          $e_CADENA_XML .= "\r\n\t\t\t<pago10:Pago";
          $e_CADENA_XML .= " FechaPago='"         . $this->remplazar_caracteres_xml( $e_FECHA_PAGO ) . "'";
          $e_CADENA_XML .= " FormaDePagoP='"      . $this->remplazar_caracteres_xml( $m_COMPROBANTE['FORMAPAGO'] ) ."'";
          $e_CADENA_XML .= " MonedaP='"           . $this->remplazar_caracteres_xml( $m_UN_PAGO['MONEDA'] ) . "'";
          $e_CADENA_XML .= " Monto='"             . $this->remplazar_caracteres_xml( $m_UN_PAGO['IMPORTE'] ) . "'";

          if( $m_UN_PAGO['MONEDA'] != "MXN" )
            $e_CADENA_XML   .= " TipoCambioP='"   . $this->remplazar_caracteres_xml( $m_UN_PAGO['DOCTOS_REL'][0]['TIPO_CAMBIO'] ) . "'";

          #$e_QUERY_ORDENANTE = "SELECT DESCRIPCION, CUENTA_ORDENANTE, NUMERO_OPERACION, NOMBRE_BANCO_EMISOR_EXTRANJERO, RFC_EMISOR_CUENTA_ORDENANTE, CUENTA_BENEFICIARIO, PATRON_CUENTA_BENEFICIARIO, RFC_EMISOR_CUENTA_BENEFICIARIO \n";
          #$e_QUERY_ORDENANTE .= "FROM cat_cfdi_formapago \n";
          #$e_QUERY_ORDENANTE .= "WHERE FORMAPAGO = '{$m_COMPROBANTE['FORMAPAGO']}' \n";

          #$e_DATOS_ORDENANTE = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_array('MY_QUERY', $e_QUERY_ORDENANTE);

          /*
          if( ($e_DATOS_ORDENANTE['RFC_EMISOR_CUENTA_ORDENANTE'] == "S" || $m_UN_PAGO['RFC_BANCO_ORDENANTE'] == "XEXX010101000" ) && $m_UN_PAGO['RFC_BANCO_ORDENANTE'] != "")
          	$e_CADENA_XML .= " RfcEmisorCtaOrd='".$this->remplazar_caracteres_xml( $m_UN_PAGO['RFC_BANCO_ORDENANTE'] ) . "'";

          if($e_DATOS_ORDENANTE['CUENTA_ORDENANTE'] == "S" && $m_UN_PAGO['CUENTA_ORDENANTE'] == "")
            $e_CADENA_XML .= " CtaOrdenante='"  . $this->remplazar_caracteres_xml( $m_UN_PAGO['CUENTA_ORDENANTE'] ) . "'";

          if( $e_DATOS_ORDENANTE['NUMERO_OPERACION'] == "O" && $m_UN_PAGO['NUMERO_OPERACION'] != "")
          {
          	$e_CADENA_XML .= " NumOperacion='"    . $this->remplazar_caracteres_xml( $m_UN_PAGO['NUMERO_OPERACION'] ) . "'";
          }

          if ( $m_UN_PAGO['RFC_BANCO_ORDENANTE'] == "XEXX010101000" )
            if( substr($e_DATOS_ORDENANTE['NOMBRE_BANCO_EMISOR_EXTRANJERO'],0,2) == 'Si' && $m_UN_PAGO['NOMBRE_BANCO_ORDENANTE'] )
          	  $e_CADENA_XML .=" NomBancoOrdExt='"   . $this->remplazar_caracteres_xml( $m_UN_PAGO['NOMBRE_BANCO_ORDENANTE'] ) . "'";



          if( $e_DATOS_ORDENANTE['CUENTA_BENEFICIARIO'] == "O" && ($m_UN_PAGO['CTA_BENEFICIARIO_CLABE'] != "" || $m_UN_PAGO['CTA_BENEFICIARIO'] != ""))
          {
          	if( $e_DATOS_ORDENANTE['PATRON_CUENTA_BENEFICIARIO'] != 'No' && $e_DATOS_ORDENANTE['PATRON_CUENTA_BENEFICIARIO'] != '' && $e_DATOS_ORDENANTE['PATRON_CUENTA_BENEFICIARIO'] != 'Opcional' )
          	{
          	  $e_PATRON_BENEFICIARIO = str_replace( "|", "$|^", $e_DATOS_ORDENANTE['PATRON_CUENTA_BENEFICIARIO'] );
          	  $e_PATRON_BENEFICIARIO = "/^".$e_PATRON_BENEFICIARIO."$/";

              if ( preg_match( $e_PATRON_BENEFICIARIO,$m_UN_PAGO['CTA_BENEFICIARIO_CLABE'] ))
                $e_CADENA_XML .=" CtaBeneficiario='". $this->remplazar_caracteres_xml( $m_UN_PAGO['CTA_BENEFICIARIO_CLABE'] ) . "'";
              elseif( preg_match( $e_PATRON_BENEFICIARIO,$m_UN_PAGO['CTA_BENEFICIARIO'] ))
                $e_CADENA_XML .=" CtaBeneficiario='". $this->remplazar_caracteres_xml( $m_UN_PAGO['CTA_BENEFICIARIO'] ) . "'";
          	  }
          }

          if($e_DATOS_ORDENANTE['RFC_EMISOR_CUENTA_BENEFICIARIO'] == "O" &&  $m_UN_PAGO['RFC_BANCO_BENEFICIARIO'] != "")
          		$e_CADENA_XML .=" RfcEmisorCtaBen='". $this->remplazar_caracteres_xml( $m_UN_PAGO['RFC_BANCO_BENEFICIARIO'] ) . "'";

          */
          if($m_UN_PAGO['TIPO_CAD_PAGO'] == "03")
          {
            $e_CADENA_XML .= " TipoCadPago='"      . $this->remplazar_caracteres_xml( $m_UN_PAGO['TIPO_CAD_PAGO'] ) . "'";
            $e_CADENA_XML .= " CertPago=''";
            $e_CADENA_XML .= " CadPago=''";
            $e_CADENA_XML .= " SelloPago=''";
          }

          $e_CADENA_XML .=">";

          if( $m_UN_PAGO['DOCTOS_REL'] )
          {
            foreach($m_UN_PAGO['DOCTOS_REL'] as $m_DOCTO_REL)
            {
              $e_CADENA_XML .= "\r\n\t\t\t\t<pago10:DoctoRelacionado";
              $e_CADENA_XML .= " IdDocumento='"        . $this->remplazar_caracteres_xml( $m_DOCTO_REL['IDDCOUMENTO'] )    . "'";
              $e_CADENA_XML .= " Folio='"              . $this->remplazar_caracteres_xml( $m_DOCTO_REL['FOLIO'] )          . "'";
              $e_CADENA_XML .= " MonedaDR='"           . $this->remplazar_caracteres_xml( $m_DOCTO_REL['MONEDA_DR'] )      . "'";
              $e_CADENA_XML .= " MetodoDePagoDR='"     . $this->remplazar_caracteres_xml( $m_DOCTO_REL['METODO_PAGO_DR'] ) . "'";

              if($m_UN_PAGO['SERIE'])
                $e_CADENA_XML .= " Serie='"            . $this->remplazar_caracteres_xml( $m_DOCTO_REL['SERIE'] ) . "'";

              if($m_UN_PAGO['MONEDA'] != $m_DOCTO_REL['MONEDA_DR'])
                $e_CADENA_XML .= " TipoCambioDR='"     . $this->remplazar_caracteres_xml( $m_DOCTO_REL['TIPO_CAMBIO'] ) . "'";

              if($m_DOCTO_REL['METODO_PAGO_DR'] == "PPD")
              {
                $e_CADENA_XML .= " NumParcialidad='"   . $this->remplazar_caracteres_xml( $m_DOCTO_REL['PARCIALIDAD'] )      . "'";
                $e_CADENA_XML .= " ImpSaldoAnt='"      . $this->remplazar_caracteres_xml( $m_DOCTO_REL['IMPSALDOANT'] )      . "'";
                $e_CADENA_XML .= " ImpSaldoInsoluto='" . $this->remplazar_caracteres_xml( $m_DOCTO_REL['IMPSALDOINSOLUTO'] ) . "'";
              }
              $e_CADENA_XML .= " ImpPagado='"          . $this->remplazar_caracteres_xml( $m_DOCTO_REL['IMPORTE_PAGADO'] )   . "'";

              $e_CADENA_XML .="/>";
            }
          }
          $e_CADENA_XML .="\r\n\t\t\t</pago10:Pago>";
        }
        $e_CADENA_XML .= "\r\n\t\t</pago10:Pagos>";
      }
      $e_CADENA_XML .= "\r\n\t</cfdi:Complemento>";
    }
    else
    {
      //--------------------- impuestos ---------------------
      if( is_array( $m_TOTALES_DOCUMENTO['IMPUESTOS']) && $this->TIPO_DOCUMENTO != 'NOMINA' )
      {
        $e_CADENA_XML .= "\r\n\t<cfdi:Impuestos";
        if(count($m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']) > 1)
          if( floatval( $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']['GRAN_TOTAL'] ) >= 0.00 )
            $e_CADENA_XML .= " TotalImpuestosRetenidos='"   . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']['GRAN_TOTAL'], $e_DECIMALES_MONEDA_DOCUMENTO ) )   . "'";

        if(count($m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']) > 1)
          if( floatval( $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']['GRAN_TOTAL'] ) >= 0.00 )
            $e_CADENA_XML .= " TotalImpuestosTrasladados='" . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']['GRAN_TOTAL'], $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";

        $e_CADENA_XML .= ">";

        //--------------------- retenciones  ---------------------
        if(count($m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']) > 1)
          if( floatval( $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']['GRAN_TOTAL'] ) >= 0.00 )
          {
            $e_CADENA_XML .= "\r\n\t\t<cfdi:Retenciones>";
            foreach ($m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS'] as $e_COD_IMPTO => $m_VALORES )
            {
              file_put_contents("RETENIDOS","COD_IMPTO: {$e_COD_IMPTO}\nVALOR: {$m_VALORES}");
              if ( $e_COD_IMPTO === 'GRAN_TOTAL' )
                continue;

              
              $e_CADENA_XML .= "\r\n\t\t\t<cfdi:Retencion";
              $e_CADENA_XML .= " Impuesto='"      . $this->remplazar_caracteres_xml( $e_COD_IMPTO ) . "'";
              $e_CADENA_XML .= " Importe='"       . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( floatval( current( $m_VALORES ) ), $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
              $e_CADENA_XML .= "/>";
            }
            $e_CADENA_XML .= "\r\n\t\t</cfdi:Retenciones>";
          }

        //--------------------- traslados ---------------------
        if(count($m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']) > 1)
          if( floatval( $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']['GRAN_TOTAL'] ) >= 0.00 )
          {
            $e_CADENA_XML .= "\r\n\t\t<cfdi:Traslados>";
            foreach ( $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS'] as $e_COD_IMPTO => $m_VALORES )
            {
              if ( $e_COD_IMPTO === 'GRAN_TOTAL' )
                continue;

              $e_CADENA_XML .= "\r\n\t\t\t<cfdi:Traslado";
              $e_CADENA_XML .= " TipoFactor="     . "'Tasa'";
              $e_CADENA_XML .= " Impuesto='"      . $this->remplazar_caracteres_xml( $e_COD_IMPTO ) . "'";
              $e_CADENA_XML .= " TasaOCuota='"    . $this->formatear_decimales_hacienda( ( floatval( key($m_VALORES) ) / 100 ) ) . "'";
              $e_CADENA_XML .= " Importe='"       . $this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( floatval( current( $m_VALORES ) ), $e_DECIMALES_MONEDA_DOCUMENTO ) ) . "'";
              $e_CADENA_XML .= "/>";
            }
            $e_CADENA_XML   .= "\r\n\t\t</cfdi:Traslados>";
          }
        $e_CADENA_XML     .= "\r\n\t</cfdi:Impuestos>";
      }
      else
      {
      	if($this->TIPO_DOCUMENTO != 'NOMINA')
          $e_CADENA_XML .= "\r\n\t<cfdi:Impuestos/>";
      }

      $e_CADENA_XML .= "\r\n\t<cfdi:Complemento>";

      $e_NODO_INE = $this->generar_nodo_complemento_ine( $e_SIGLA_STATUS );
      if ( $e_NODO_INE )
        $e_CADENA_XML .= $e_NODO_INE;

  	  //generar_nodo Comercio Exterior
      if ( $this->USAR_COMERCIO_EXTERIOR && ( $this->RECEPTOR['RFC'] == 'XEXX010101000' ) && ( $this->RECEPTOR['SIGLA_PAIS'] != "MEX" ) )
      {
        if ( $e_NODO_COMERCIO = $this->obtener_nodo_comercio_exterior( $e_SIGLA_STATUS ) )
          $e_CADENA_XML .= "{$e_NODO_COMERCIO}";
        else
        {
          //Cliente Extranjero pero No obtiene el nodo comercio Exterior
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Error Al Obtener Nodo Comercio Exterior";
          $e_CADENA_XML .="\r\n\t\t<cce11:ComercioExterior/>";
        }
      }

      //generar_nodonomina
      if ( $this->TIPO_DOCUMENTO == 'NOMINA' )
      {
      	if ( $e_NODO_NOMINA= $this->obtener_nodo_nomina( $e_SIGLA_STATUS ) )
      	{
      		$e_CADENA_XML .= "{$e_NODO_NOMINA}";
      	}
      	else
      	{
      		//Cliente Extranjero pero No obtiene el nodo comercio Exterior
      		$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Error Al Obtener Nodo N�mina";
      		$e_CADENA_XML .="\r\n\t\t<nomina12:Nomina/>";
      	}
      }
      $e_CADENA_XML .="\r\n\t</cfdi:Complemento>";
    }

    $e_CADENA_XML .= "\r\n</cfdi:Comprobante>";
    //Guardamos el archivo sin timbre y sin SELLO para que se pueda consultar aun cuando no se timbre o aun cuando no tenga la cadena
    $this->guardar_archivo_xml_documento( $e_CADENA_XML );

    //Se le agregar el atributo SELLO antes de regresar la cadena XML
    $e_CADENA_SELLADA = $this->add_atributo_sello_comprobante( $e_CADENA_XML );
    //Guardamos el archivo sin timbre para que se pueda consultar aun cuando no se timbre
    $this->guardar_archivo_xml_documento( $e_CADENA_SELLADA );

    if( count( $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR] ) )
      return false;

    return $e_CADENA_SELLADA;
  }

  private function obtener_nodo_nomina($e_SIGLA_STATUS)
  {
    $e_CADENA_XML = '';

    $m_EMISOR = $this->generar_elemento_emisor();
    if($m_EMISOR === false)
      return false;

    $m_DATOS_NOMINA = $this->generar_elementos_nomina($e_SIGLA_STATUS);
    if( $m_DATOS_NOMINA === false )
      return false;

    $e_TOTAL_INCAPACIDAD = $m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_INCAPACIDAD'] + $m_DATOS_NOMINA['DEDUCCIONES']['TOTAL_INCAPACIDAD'];

    if( $e_TOTAL_INCAPACIDAD != $m_DATOS_NOMINA['INCAPACIDADES']['TOTAL_IMP_MONETARIO'] )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La suma de los importes percepciones y/o deducciones de tipo incapacidad deben ser igual a la suma de los importes de las incapacidades. ";
      return false;
    }

    $e_DESCUENTO = 0.00;
    $e_ISR = 0.00;

    if(is_array($m_DATOS_NOMINA['DEDUCCIONES']['DETALLES']))
    {
      foreach ($m_DATOS_NOMINA['DEDUCCIONES']['DETALLES'] AS $e_KEY => $m_DEDUCCION)
      {
        if($m_DEDUCCION['TIPO_DEDUCCION'] != '002')
          $e_DESCUENTO += $m_DEDUCCION['IMPORTE_GRAVADO'] + $m_DEDUCCION['IMPORTE_EXCENTO'];
        else
          $e_ISR += $m_DEDUCCION['IMPORTE_GRAVADO'] + $m_DEDUCCION['IMPORTE_EXCENTO'];
      }
    }
    $e_TOTALOTRASDEDUCCIONES = ($e_DESCUENTO > 0) ? $this->formatear_decimales_hacienda($e_DESCUENTO,2) : 0;
    $e_TOTALIMPUESTOSRETENIDOS.= ($e_ISR > 0) ? $this->formatear_decimales_hacienda($e_ISR,2) : 0;
    $e_TOTALDEDUCCIONES = $this->formatear_decimales_hacienda($e_TOTALOTRASDEDUCCIONES + $e_TOTALIMPUESTOSRETENIDOS,2);
    $e_TOTALPERCEPCIONES = $this->formatear_decimales_hacienda($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_EXCENTO'] + $m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_GRAVADO'],2);
    $e_TOTALOTROSPAGOS= $this->formatear_decimales_hacienda($m_DATOS_NOMINA['OTROSPAGOS']['TOTAL_IMPORTE'],2);
    $e_SUBTOTAL = $this->formatear_decimales_hacienda($m_TOTALES_DOCUMENTO['SUBTOTAL'] - $e_DESCUENTO,2);
    $e_TOTAL = ($e_TOTALPERCEPCIONES + $e_TOTALOTROSPAGOS) - $e_TOTALDEDUCCIONES;

    //--------------------- Inicio Nomina ---------------------//
    $e_CADENA_XML .= "\r\n\t\t<nomina12:Nomina xmlns:nomina12='http://www.sat.gob.mx/nomina12'";
    //Version
    $e_CADENA_XML.= " Version='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['VERSION'])."'";
    //TipoNomina
    $e_CADENA_XML.= " TipoNomina='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['TIPO_NOMINA'])."'";
    //FechaPago
    $e_CADENA_XML.= " FechaPago='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['FECHA_PAGO'])."'";
    //FechaInicialPago
    $e_CADENA_XML.= " FechaInicialPago='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['FECHA_INICIAL_PAGO'])."'";
    //FechaFinalPago
    $e_CADENA_XML.= " FechaFinalPago='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['FECHA_FINAL_PAGO'])."'";
    //NumDiasPagados
    $e_CADENA_XML.= " NumDiasPagados='".$this->remplazar_caracteres_xml($this->formatear_decimales_hacienda($m_DATOS_NOMINA['ENCABEZADO']['DIAS_PAGADOS'],3))."'";
    //TotalPercepciones
    $e_CADENA_XML.= " TotalPercepciones='".$this->remplazar_caracteres_xml($e_TOTALPERCEPCIONES)."'";
    //TotalDeducciones
    if($e_TOTALDEDUCCIONES > 0)
    {
      $e_CADENA_XML.= " TotalDeducciones='".$this->remplazar_caracteres_xml($e_TOTALDEDUCCIONES)."'";
    }
    //totalOtrosPagos
    if($m_DATOS_NOMINA['OTROSPAGOS']['DETALLES'])
    {
      $e_CADENA_XML.= " TotalOtrosPagos='".$this->remplazar_caracteres_xml($e_TOTALOTROSPAGOS)."'";
    }
    $e_CADENA_XML .= " >";

    //--------------------- Inicio Nomina Emisor---------------------//
    $e_CADENA_XML .= "\r\n\t\t\t<nomina12:Emisor";
    //RegistroPatronal
    $e_CADENA_XML.= " RegistroPatronal='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['REGISTRO_PATRONAL'])."'";

    if(strlen($m_EMISOR['RFC']) == 13)
    {
      //persona fisica
      //Curp
      $e_CADENA_XML.= " Curp='".$this->remplazar_caracteres_xml($m_EMISOR['CURP'])."'";
    }

    $e_CADENA_XML .="/>";
    //--------------------- Final  Nomina Emisor---------------------//
    //--------------------- Inicio Nomina Receptor---------------------//
    $e_CADENA_XML .= "\r\n\t\t\t<nomina12:Receptor";
    //Curp
    $e_CADENA_XML.= " Curp='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['CURP'])."'";
    //NumSeguridadSocial
    //if($m_DATOS_NOMINA['ENCABEZADO']['SEGURIDAD_SOCIAL'] != 0 && strlen($m_DATOS_NOMINA['ENCABEZADO']['SEGURIDAD_SOCIAL']) <= 15 )
    $e_CADENA_XML.= " NumSeguridadSocial='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['SEGURIDAD_SOCIAL'])."'";
    //FechaInicioRelLaboral
    $e_CADENA_XML.= " FechaInicioRelLaboral='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['FECHA_INICIO_LABORAL'])."'";
    //TipoContrato
    $e_CADENA_XML.= " TipoContrato='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['CONTRATO'])."'";
    //Sindicalizado
    $e_CADENA_XML.= " Sindicalizado='".($m_DATOS_NOMINA['ENCABEZADO']['SINDICALIZADO'] == "SI" ? "S�" : "No")."'";
    //TipoRegimen
    $e_CADENA_XML.= " TipoRegimen='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['TIPO_REGIMEN'])."'";
    //NumEmpleado
    $e_CADENA_XML.= " NumEmpleado='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['NUM_EMPLEADO'])."'";
    //Departamento
    $e_CADENA_XML.= " Departamento='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['DEPARTAMENTO'])."'";
    //Puesto
    $e_CADENA_XML.= " Puesto='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['PUESTO'])."'";
    //RiesgoPuesto
    $e_CADENA_XML.= " RiesgoPuesto='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['RIESGO_PUESTO'])."'";
    //PeriodicidadPago
    $e_CADENA_XML.= " PeriodicidadPago='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['PERIODICIDAD_PAGO'])."'";
    //Banco
    if(strlen($m_DATOS_NOMINA['ENCABEZADO']['CUENTA']) != '18')
      $e_CADENA_XML.= " Banco='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['BANCO'])."'";

    //CuentaBancaria
    $e_CADENA_XML.= " CuentaBancaria='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['CUENTA'])."'";
    //SalarioBaseCotApor
    $e_CADENA_XML.= " SalarioBaseCotApor='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['SALARIO_BASE'])."'";
    //SalarioDiarioIntegrado
    $e_CADENA_XML.= " SalarioDiarioIntegrado='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['SALARIO_DIARIO'])."'";
    //ClaveEntFed
    $e_CADENA_XML.= " ClaveEntFed='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['ENCABEZADO']['CLAVEENTFED'])."'";
    //Antiguedad
    $e_ANTIGUEDAD = "P";
    $e_ANTIGUEDAD .= $m_DATOS_NOMINA['ENCABEZADO']['ANTIGUEDAD'] ? "{$m_DATOS_NOMINA['ENCABEZADO']['ANTIGUEDAD']}W" : "";
    $e_CADENA_XML .= " Antig�edad='".$this->remplazar_caracteres_xml($e_ANTIGUEDAD)."'";
    //--------------------- Inicio Nomina Subcontratacion---------------------//
    if($m_DATOS_NOMINA['SUBCONTRATACION']['DETALLES'])
    {
      $e_CADENA_XML .= " >";
      foreach($m_DATOS_NOMINA['SUBCONTRATACION']['DETALLES'] as $m_DETALLES)
      {
        //Identificador
        $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:SubContratacion";
        //RfcLabora
        $e_CADENA_XML.= " RfcLabora='".$this->remplazar_caracteres_xml($m_DETALLES['RFC_LABORAL'])."'";
        //PorcentajeTiempo
        $e_CADENA_XML.= " PorcentajeTiempo='".$this->remplazar_caracteres_xml($this->formatear_decimales_hacienda($m_DETALLES['PORCENTAJE_TIEMPO'],3))."'";
        $e_CADENA_XML .= "/>";
      }
      $e_CADENA_XML .= "\r\n\t\t\t</nomina12:Receptor>";
    }
    else
    {
    //$e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:SubContratacion/>";

    }
    //--------------------- Final  Nomina Subcontratacion---------------------//
    if($m_DATOS_NOMINA['SUBCONTRATACION']['DETALLES'])
    {
      //$e_CADENA_XML .= "\r\n\t\t\t</nomina12:Receptor>";
    }
    else
    {
      $e_CADENA_XML .= "/>";
    }
    //$e_CADENA_XML .= "\r\n\t\t\t</nomina12:Receptor>";
    //--------------------- Final  Nomina Receptor---------------------//
    //--------------------- Inicio Nomina Percepciones---------------------//
    $e_CADENA_XML .= "\r\n\t\t\t<nomina12:Percepciones";
    //TotalSueldos
    //$e_CADENA_XML.= " TotalSueldos='".$this->remplazar_caracteres_xml($this->formatear_decimales_hacienda($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_GRAVADO'] + $m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_EXCENTO'],2))."'";
    if($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_SUELDOS'])
      $e_CADENA_XML.= " TotalSueldos='".$this->remplazar_caracteres_xml($this->formatear_decimales_hacienda($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_SUELDOS'],2))."'";

    //TotalGravado
    $e_CADENA_XML.= " TotalGravado='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_GRAVADO'])."'";
    //TotalExento
    $e_CADENA_XML.= " TotalExento='".$this->remplazar_caracteres_xml($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_EXCENTO'])."'";

    if($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_INDEMNIZACION'] > 0)
      $e_CADENA_XML.= " TotalSeparacionIndemnizacion='".$this->remplazar_caracteres_xml($this->formatear_decimales_hacienda($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_INDEMNIZACION'],2))."'";

    if($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_JUBILACION'] > 0)
      $e_CADENA_XML.= " TotalJubilacionPensionRetiro='".$this->remplazar_caracteres_xml($this->formatear_decimales_hacienda($m_DATOS_NOMINA['PERCEPCIONES']['TOTAL_JUBILACION'],2))."'";

    $e_CADENA_XML .= ">";
    //--------------------- Inicio Nomina Percepcion---------------------//
    if($m_DATOS_NOMINA['PERCEPCIONES']['DETALLES'])
    {
      foreach($m_DATOS_NOMINA['PERCEPCIONES']['DETALLES'] as $m_DETALLES)
      {
         //--------------------- Inicio HorasExtra ---------------------//
        if($m_DETALLES['TIPO_PERCEPCION'] == '019')
        {
          $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Percepcion";
          //TipoPercepcion
          $e_CADENA_XML.= " TipoPercepcion='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_PERCEPCION'])."'";
          //Clave
          $e_CADENA_XML.= " Clave='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_PERCEPCION'])."'";
          //Concepto
          $e_CADENA_XML.= " Concepto='".$this->remplazar_caracteres_xml($m_DETALLES['CONCEPTO'])."'";
          //ImporteGravado
          $e_CADENA_XML.= " ImporteGravado='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_GRAVADO'])."'";
          //ImporteExento
          $e_CADENA_XML.= " ImporteExento='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_EXCENTO'])."'";
          $e_CADENA_XML .= ">";

          if($m_DATOS_NOMINA['HORASEXTRAS']['DETALLES'])
          {
            foreach($m_DATOS_NOMINA['HORASEXTRAS']['DETALLES'] as $m_DETALLES)
            {
              $e_CADENA_XML .= "\r\n\t\t\t\t\t<nomina12:HorasExtra";
              //Dias
              $e_CADENA_XML.= " Dias='".$this->remplazar_caracteres_xml($m_DETALLES['DIAS'])."'";
              //TipoHoras
              $e_CADENA_XML.= " TipoHoras='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_HORAS'])."'";
              //HorasExtra
              $e_CADENA_XML.= " HorasExtra='".$this->remplazar_caracteres_xml($m_DETALLES['HORAS_EXTRA'])."'";
              //ImportePagado
              $e_CADENA_XML.= " ImportePagado='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE'])."'";
              $e_CADENA_XML .= "/>";
            }
          }
          else
          {
            $e_CADENA_XML .= "\r\n\t\t\t\t\t<nomina12:HorasExtra/>";
          }
          $e_CADENA_XML .= "\r\n\t\t\t\t</nomina12:Percepcion>";
        }
        //----------------------- Fin HorasExtra ----------------------//
        //--------------------- Inicio Indemnizacion ---------------------//
        /*elseif($m_DETALLES['TIPO_PERCEPCION'] == '022' || $m_DETALLES['TIPO_PERCEPCION'] == '023' || $m_DETALLES['TIPO_PERCEPCION'] == '025')
        {
          $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Percepcion";
          //TipoPercepcion
          $e_CADENA_XML.= " TipoPercepcion='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_PERCEPCION'])."'";
          //Clave
          $e_CADENA_XML.= " Clave='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_PERCEPCION'])."'";
          //Concepto
          $e_CADENA_XML.= " Concepto='".$this->remplazar_caracteres_xml($m_DETALLES['CONCEPTO'])."'";
          //ImporteGravado
          $e_CADENA_XML.= " ImporteGravado='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_GRAVADO'])."'";
          //ImporteExento
          $e_CADENA_XML.= " ImporteExento='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_EXCENTO'])."'";
          $e_CADENA_XML .= ">";

          $e_CADENA_XML .= "\r\n\t\t\t\t\t<nomina12:SeparacionIndemnizacion";
          //TotalPagado
          $e_CADENA_XML.= " TotalPagado='".$this->remplazar_caracteres_xml('0.00')."'";
          //NumA�osServicio
          $e_CADENA_XML.= " NumA�osServicio='".$this->remplazar_caracteres_xml('0.00')."'";
          //UltimoSueldoMensOrd
          $e_CADENA_XML.= " UltimoSueldoMensOrd='".$this->remplazar_caracteres_xml('0.00')."'";
          //IngresoAcumulable
          $e_CADENA_XML.= " IngresoAcumulable='".$this->remplazar_caracteres_xml('0.00')."'";
          //IngresoNoAcumulable
          $e_CADENA_XML.= " IngresoNoAcumulable='".$this->remplazar_caracteres_xml('0.00')."'";
          $e_CADENA_XML .= "/>";

          $e_CADENA_XML .= "\r\n\t\t\t\t</nomina12:Percepcion>";
        }
        //----------------------- Fin Indemnizacion ----------------------//
        //--------------------- Inicio Jubilacion ---------------------//
        elseif($m_DETALLES['TIPO_PERCEPCION'] == '039' || $m_DETALLES['TIPO_PERCEPCION'] == '034' )
        {
          $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Percepcion";
          //TipoPercepcion
          $e_CADENA_XML.= " TipoPercepcion='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_PERCEPCION'])."'";
          //Clave
          $e_CADENA_XML.= " Clave='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_PERCEPCION'])."'";
          //Concepto
          $e_CADENA_XML.= " Concepto='".$this->remplazar_caracteres_xml($m_DETALLES['CONCEPTO'])."'";
          //ImporteGravado
          $e_CADENA_XML.= " ImporteGravado='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_GRAVADO'])."'";
          //ImporteExento
          $e_CADENA_XML.= " ImporteExento='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_EXCENTO'])."'";
          $e_CADENA_XML .= ">";

          $e_CADENA_XML .= "\r\n\t\t\t\t\t<nomina12:JubilacionPensionRetiro";
          //TotalUnaExhibicion
          $e_CADENA_XML.= " TotalUnaExhibicion='".$this->remplazar_caracteres_xml('0.00')."'";
          //TotalParcialidad
          $e_CADENA_XML.= " TotalParcialidad='".$this->remplazar_caracteres_xml('0.00')."'";
          //MontoDiario
          $e_CADENA_XML.= " MontoDiario='".$this->remplazar_caracteres_xml('0.00')."'";
          //IngresoAcumulable
          $e_CADENA_XML.= " IngresoAcumulable='".$this->remplazar_caracteres_xml('0.00')."'";
          //IngresoNoAcumulable
          $e_CADENA_XML.= " IngresoNoAcumulable='".$this->remplazar_caracteres_xml('0.00')."'";
          $e_CADENA_XML .= "/>";

          $e_CADENA_XML .= "\r\n\t\t\t\t</nomina12:Percepcion>";
        }*/
        //----------------------- Fin Jubilacion ----------------------//

        else
        {
          $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Percepcion";
          //TipoPercepcion
          $e_CADENA_XML.= " TipoPercepcion='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_PERCEPCION'])."'";
          //Clave
          $e_CADENA_XML.= " Clave='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_PERCEPCION'])."'";
          //Concepto
          $e_CADENA_XML.= " Concepto='".$this->remplazar_caracteres_xml($m_DETALLES['CONCEPTO'])."'";
          //ImporteGravado
          $e_CADENA_XML.= " ImporteGravado='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_GRAVADO'])."'";
          //ImporteExento
          $e_CADENA_XML.= " ImporteExento='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_EXCENTO'])."'";

          $e_CADENA_XML .= "/>";
        }
      }
    }
    else
    {
      $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Percepcion/>";
    }
    //--------------------- Final  Nomina Percepcion---------------------//
    $e_CADENA_XML .= "\r\n\t\t\t</nomina12:Percepciones>";
    //--------------------- Final  Nomina Percepciones---------------------//
    //Modificacion Deducciones
    // Verificamos si existen deducciones
    $e_EXISTE_DEDUCCIONES = $GLOBALS[SYSTEM][DBS][DB]->get_field('MY_QUERY','EXISTE_DEDC',"SELECT COUNT(D_RECN_DEDC_ID) AS EXISTE_DEDC FROM doc_recn_dedc WHERE D_RECN_DEDC_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'");
    if( $e_EXISTE_DEDUCCIONES > 0)
    {
      //--------------------- Inicio Nomina Deducciones---------------------//
      $e_CADENA_XML .= "\r\n\t\t\t<nomina12:Deducciones";
      //TotalOtrasDeducciones

      if($m_DATOS_NOMINA['DEDUCCIONES']['DETALLES'])
      {
        $e_CADENA_XML .= " TotalOtrasDeducciones='".(($e_DESCUENTO > 0) ? $this->remplazar_caracteres_xml($e_DESCUENTO) : "0.00")."'";
        //$e_CADENA_XML.= " TotalOtrasDeducciones='12'";
        //TotalImpuestosRetenidos
        if($e_ISR > 0)
        {
          $e_CADENA_XML .= " TotalImpuestosRetenidos='".(($e_ISR > 0) ? $this->remplazar_caracteres_xml($e_ISR) : "0.0")."'";
        }

        $e_CADENA_XML .= ">";
        //--------------------- Inicio Nomina Deduccion---------------------//
        if($m_DATOS_NOMINA['DEDUCCIONES']['DETALLES'])
        {
          foreach($m_DATOS_NOMINA['DEDUCCIONES']['DETALLES'] as $m_DETALLES)
          {
            $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Deduccion";
            //TipoDeduccion
            $e_CADENA_XML.= " TipoDeduccion='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_DEDUCCION'])."'";
            //Clave
            $e_CADENA_XML.= " Clave='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_DEDUCCION'])."'";
            //Concepto
            $e_CADENA_XML.= " Concepto='".$this->remplazar_caracteres_xml($m_DETALLES['CONCEPTO'])."'";
            //Importe
            $e_CADENA_XML.= " Importe='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE_GRAVADO'] + $m_DETALLES['IMPORTE_EXCENTO'])."'";
            $e_CADENA_XML .= "/>";
          }
          $e_CADENA_XML .= "\r\n\t\t\t</nomina12:Deducciones>";
        }
      }
      else
      {
        //$e_CADENA_XML .= "\r\n\t\t\t<nomina12:Deduccion/>";
        $e_CADENA_XML .= "/>";
      }
    }
    //--------------------- Final  Nomina Deduccion---------------------//
    // $e_CADENA_XML .= "\r\n\t\t\t</nomina12:Deducciones>";
    //--------------------- Final  Nomina Deducciones---------------------//
    if($m_DATOS_NOMINA['OTROSPAGOS']['DETALLES'])
    {
      //--------------------- Inicio Nomina OtrosPagos---------------------//
      $e_CADENA_XML .= "\r\n\t\t\t<nomina12:OtrosPagos>";
      //--------------------- Inicio Nomina OtroPago---------------------//

      foreach($m_DATOS_NOMINA['OTROSPAGOS']['DETALLES'] as $m_DETALLES)
      {
        $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:OtroPago";
        //TipoOtroPago
        $e_CADENA_XML.= " TipoOtroPago='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_P'])."'";
        //Clave
        $e_CADENA_XML.= " Clave='".$this->remplazar_caracteres_xml($m_DETALLES['CLAVE_OTROS_PAGOS'])."'";
        //Concepto
        $e_CADENA_XML.= " Concepto='".$this->remplazar_caracteres_xml($m_DETALLES['CONCEPTO'])."'";
        //Importe
        $e_CADENA_XML.= " Importe='".$this->remplazar_caracteres_xml($m_DETALLES['IMPORTE'])."'";

        //--------------------- Inicio Nomina SubsidioAlEmpleo---------------------//
        if($m_DETALLES['CLAVE_P'] == '002')
        {
          $e_CADENA_XML .= ">";
          $e_CADENA_XML .= "\r\n\t\t\t\t\t<nomina12:SubsidioAlEmpleo";
          //SubsidioCausado
          $e_CADENA_XML.= " SubsidioCausado='".$this->remplazar_caracteres_xml($m_DETALLES['SUBSIDIO'])."'";
          $e_CADENA_XML .= "/>";

          $e_CADENA_XML .= "\r\n\t\t\t</nomina12:OtroPago>";
        }
        elseif($m_DETALLES['CLAVE_P'] == '004')
        {
          $e_CADENA_XML .= ">";
          //--------------------- Inicio Nomina CompensacionSaldosAFavor---------------------//
          $e_CADENA_XML .= "\r\n\t\t\t\t\t<nomina12:CompensacionSaldosAFavor";
          //SaldoAFavor
          $e_CADENA_XML.= " SaldoAFavor='".$this->remplazar_caracteres_xml($m_DETALLES['SALDO_A_FAVOR'])."'";
          //A�o
          $e_CADENA_XML.= " A�o='".$this->remplazar_caracteres_xml($m_DETALLES['ANNIO'])."'";
          //RemanenteSalFav
          //$e_CADENA_XML.= " RemanenteSalFav='".$this->remplazar_caracteres_xml('0.00')."'";
          $e_CADENA_XML.= " RemanenteSalFav='".$this->remplazar_caracteres_xml($m_DETALLES['REMANENTE_SALDO_A_FAVOR'])."'";
          $e_CADENA_XML .= "/>";

          $e_CADENA_XML .= "\r\n\t\t</nomina12:OtroPago>";
          //--------------------- Final  Nomina CompensacionSaldosAFavor---------------------//
        }
        else {
          $e_CADENA_XML .= "/>";
        }
      }

      if($m_DATOS_NOMINA['OTROSPAGOS']['DETALLES'])
      {
        //$e_CADENA_XML .= "\r\n\t\t</nomina12:OtroPago>";
        $e_CADENA_XML .= "\r\n\t\t\t</nomina12:OtrosPagos>";
      }
   }

    //--------------------- Inicio Nomina Incapacidades---------------------//
    if($m_DATOS_NOMINA['INCAPACIDADES']['DETALLES'])
    {
      $e_CADENA_XML .= "\r\n\t\t\t<nomina12:Incapacidades";
      //--------------------- Inicio Nomina Incapacidad---------------------//

      $e_CADENA_XML .= ">";
      foreach($m_DATOS_NOMINA['INCAPACIDADES']['DETALLES'] as $m_DETALLES)
      {
        $e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Incapacidad";
        //DiasIncapacidad
        $e_CADENA_XML.= " DiasIncapacidad='".$this->remplazar_caracteres_xml($m_DETALLES['DIAS_INCAPACIDAD'])."'";
        //TipoIncapacidad
        $e_CADENA_XML.= " TipoIncapacidad='".$this->remplazar_caracteres_xml($m_DETALLES['TIPO_INCAPACIDAD'])."'";
        $e_CADENA_XML.= " ImporteMonetario='".$this->remplazar_caracteres_xml($m_DETALLES['DESCUENTO'])."'";
        $e_CADENA_XML .= "/>";
      }

      if($m_DATOS_NOMINA['INCAPACIDADES']['DETALLES'])
      {
        $e_CADENA_XML .= "\r\n\t\t\t</nomina12:Incapacidades>";
      }
      else
      {
        $e_CADENA_XML .= "/>";
      }
      //--------------------- Final  Nomina Incapacidad---------------------//
      //$e_CADENA_XML .= "\r\n\t\t\t</nomina12:Incapacidades>";
      //--------------------- Final  Nomina Incapacidades---------------------//
    }
    else
    {
      //$e_CADENA_XML .= "\r\n\t\t\t\t<nomina12:Incapacidad />";
    }
    $e_CADENA_XML .= "\r\n\t\t</nomina12:Nomina>";
    //--------------------- Final  Nomina ---------------------//
    return $e_CADENA_XML;
  }

  private function generar_elementos_nomina( $e_SIGLA_STATUS )
  {
  	$m_CAMPOS = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"];
  	$e_ID_DOCUMENTO = $this->DOCUMENTO_PADRE->ID;
  	$m_DATOS_NOMINA['ENCABEZADO'] = $this->generar_elemento_nomina_encabezado($e_ID_DOCUMENTO, $m_CAMPOS);
  	$m_DATOS_NOMINA['PERCEPCIONES'] = $this->generar_elemento_nomina_percepciones($e_ID_DOCUMENTO, $m_CAMPOS);
    // Modificacion deducciones
    // Verificamos si tiene deducciones
    $e_EXISTE_DEDUCCIONES = $GLOBALS[SYSTEM][DBS][DB]->get_field('MY_QUERY','EXISTE_DEDC',"SELECT COUNT(D_RECN_DEDC_ID) AS EXISTE_DEDC FROM doc_recn_dedc WHERE D_RECN_DEDC_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'");
    if($e_EXISTE_DEDUCCIONES > 0  )
    {
      $m_DATOS_NOMINA['DEDUCCIONES'] = $this->generar_elemento_nomina_deducciones($e_ID_DOCUMENTO, $m_CAMPOS);
    }

  	$m_DATOS_NOMINA['INCAPACIDADES'] = $this->generar_elemento_nomina_incapacidades($e_ID_DOCUMENTO, $m_CAMPOS);
  	$m_DATOS_NOMINA['HORASEXTRAS'] = $this->generar_elemento_nomina_horasextra($e_ID_DOCUMENTO, $m_CAMPOS);
  	$m_DATOS_NOMINA['OTROSPAGOS'] = $this->generar_elemento_nomina_otrospagos($e_ID_DOCUMENTO, $m_CAMPOS);
  	$m_DATOS_NOMINA['SUBCONTRATACION'] = $this->generar_elemento_nomina_subcontratacion($e_ID_DOCUMENTO, $m_CAMPOS);

  	if($m_DATOS_NOMINA['ENCABEZADO'] === false || $m_DATOS_NOMINA['PERCEPCIONES'] === false || $m_DATOS_NOMINA['DEDUCCIONES'] === false || $m_DATOS_NOMINA['INCAPACIDADES'] === false || $m_DATOS_NOMINA['HORASEXTRAS'] === false || $m_DATOS_NOMINA['OTROSPAGOS'] === false || $m_DATOS_NOMINA['SUBCONTRATACION'] === false)
  		return false;

  	return $m_DATOS_NOMINA;
  }

  function generar_elemento_nomina_encabezado($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_ENCABEZADO  = "SELECT ENT_NOMBRE AS REGISTRO_PATRONAL, {$m_CAMPOS['NUMEROEMP']} AS  NUM_EMPLEADO, {$m_CAMPOS['CURPEMP']} AS CURP, \n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['NUMSEGURIDADSOCIALEMP']} AS SEGURIDAD_SOCIAL, {$m_CAMPOS['FECHAPAGO']} AS FECHA_PAGO, {$m_CAMPOS['FECHAINICIALPAGO']} AS FECHA_INICIAL_PAGO, \n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['FECHAFINALPAGO']} AS FECHA_FINAL_PAGO,{$m_CAMPOS['NUMDIASPAGADOS']} AS DIAS_PAGADOS, DEP_NOMBRE AS DEPARTAMENTO, \n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['TIPOREGIMENEMP']} AS CLABE, {$m_CAMPOS['BANCOEMP']} AS BANCO, {$m_CAMPOS['FECHAINICIORELLABORALEMP']} AS FECHA_INICIO_LABORAL,\n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['PUESTOEMP']} AS PUESTO, {$m_CAMPOS['TIPOCONTRATOEMP']} AS CONTRATO, {$m_CAMPOS['JORNADAEMP']} AS JORNADA,\n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['PERIODICIDADPAGOEMP']} AS PERIODICIDAD_PAGO, {$m_CAMPOS['SALARIOBASECOTAPOR']} AS SALARIO_BASE,{$m_CAMPOS['RIESGOPUESTOEMP']} AS RIESGO_PUESTO,\n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['SALARIODIARIOINTEGRADO']} AS SALARIO_DIARIO, {$m_CAMPOS['TIPONOMINA']} AS TIPO_NOMINA,\n";
  	$e_QUERY_ENCABEZADO .= "       IF({$m_CAMPOS['ORIGENRECURSO']}=0,'',{$m_CAMPOS['ORIGENRECURSO']}) AS ORIGEN_RECURSO, {$m_CAMPOS['RFCPATRONORIGEN']} AS RFC_PATRON_ORIGEN,\n";
  	$e_QUERY_ENCABEZADO .= "       IF({$m_CAMPOS['FECHAINICIORELLABORALEMP']}>0, FLOOR(DATEDIFF({$m_CAMPOS['FECHAFINALPAGO']}, {$m_CAMPOS['FECHAINICIORELLABORALEMP']})/7), NULL) AS ANTIGUEDAD,\n";
  	$e_QUERY_ENCABEZADO .= "       FLOOR(TIMESTAMPDIFF(DAY, {$m_CAMPOS['FECHAINICIORELLABORALEMP']}, {$m_CAMPOS['FECHAFINALPAGO']}) / 7) AS DIA,\n";
  	$e_QUERY_ENCABEZADO .= "       TRA_EMAIL AS EMAIL, {$m_CAMPOS['CURPEMP']} AS CURP, {$m_CAMPOS['CUENTABANCARIAEMP']} AS CUENTA, {$m_CAMPOS['CLAVEENTFEDEMP']} AS ESTADO,\n";
  	$e_QUERY_ENCABEZADO .= "       {$m_CAMPOS['SINDICALIZADOEMP']} AS SINDICALIZADO, {$m_CAMPOS['MONEDA']} AS MONEDA\n";
  	$e_QUERY_ENCABEZADO .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . " \n";
  	$e_QUERY_ENCABEZADO .= "INNER JOIN {$m_CAMPOS['CATALOGOEMP']} TRA_NUMERO_EMPLEADO={$m_CAMPOS['CLIENTE']} AND TRA_ACTIVO='1'\n";
  	$e_QUERY_ENCABEZADO .= "LEFT JOIN cat_entidades ON ENT_ID={$m_CAMPOS['REGISTROPATRONALEMP']}\n";
    $e_QUERY_ENCABEZADO .= "LEFT JOIN cat_departamentos ON DEP_ID={$m_CAMPOS['DEPARTAMENTOEMP']}\n";
    $e_QUERY_ENCABEZADO .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$e_ID_DOCUMENTO}'\n";

    $m_DATOS_ENCABEZADO = $GLOBALS['SYSTEM']['DBS']['DB']->get_array('MY_QUERY', $e_QUERY_ENCABEZADO);
    $m_REGIMEN = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_array('MY_QUERY', "SELECT TIPOREGIMEN, DESCRIPCION FROM cat_cfdi_tiporegimen WHERE ID = '{$m_DATOS_ENCABEZADO['CLABE']}'");
    $m_BANCO = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_array('MY_QUERY', "SELECT BANCO FROM cat_cfdi_banco WHERE ID = '{$m_DATOS_ENCABEZADO['BANCO']}'");
    unset($m_DATOS_ENCABEZADO['CLABE']);
    $m_DATOS_ENCABEZADO['TIPO_REGIMEN'] = $m_REGIMEN['TIPOREGIMEN'];
    $m_DATOS_ENCABEZADO['BANCO'] = $m_BANCO['BANCO'];

    $m_DATOS_ENCABEZADO['JORNADA'] = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOJORNADA', "SELECT TIPOJORNADA FROM cat_cfdi_tipojornada WHERE ID = {$m_DATOS_ENCABEZADO['JORNADA']}");

    $m_DATOS_ENCABEZADO['PERIODICIDAD_PAGO'] = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'PERIODICIDADPAGO', "SELECT PERIODICIDADPAGO FROM cat_cfdi_periodicidadpago WHERE ID = {$m_DATOS_ENCABEZADO['PERIODICIDAD_PAGO']}");

    $m_DATOS_ENCABEZADO['TIPO_NOMINA'] = $m_DATOS_ENCABEZADO['TIPO_NOMINA'] ? $m_DATOS_ENCABEZADO['TIPO_NOMINA'] : $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPONOMINA', "SELECT TIPONOMINA FROM cat_cfdi_tiponomina WHERE ID = {$m_DATOS_ENCABEZADO['TIPO_NOMINA']}");

    if($m_DATOS_ENCABEZADO['TIPO_NOMINA'] == 'E')
    	$m_DATOS_ENCABEZADO['PERIODICIDAD_PAGO'] = '99';

    $m_ENCABEZADO = array
    (
      'REGISTRO_PATRONAL'    => $m_DATOS_ENCABEZADO['REGISTRO_PATRONAL'],
      'CURP'                 => $m_DATOS_ENCABEZADO['CURP'],
      'SEGURIDAD_SOCIAL'     => $m_DATOS_ENCABEZADO['SEGURIDAD_SOCIAL'],
      'FECHA_INICIO_LABORAL' => $m_DATOS_ENCABEZADO['FECHA_INICIO_LABORAL'],
      'CONTRATO'             => $m_DATOS_ENCABEZADO['CONTRATO'],
      'SINDICALIZADO'        => $m_DATOS_ENCABEZADO['SINDICALIZADO'],
      'TIPO_REGIMEN'         => $m_DATOS_ENCABEZADO['TIPO_REGIMEN'],
      'NUM_EMPLEADO'         => $m_DATOS_ENCABEZADO['NUM_EMPLEADO'],
      'DEPARTAMENTO'         => $m_DATOS_ENCABEZADO['DEPARTAMENTO'],
      'PUESTO'               => $m_DATOS_ENCABEZADO['PUESTO'],
      'RIESGO_PUESTO'        => $m_DATOS_ENCABEZADO['RIESGO_PUESTO'],
      'PERIODICIDAD_PAGO'    => $m_DATOS_ENCABEZADO['PERIODICIDAD_PAGO'],
      'CUENTA'               => $m_DATOS_ENCABEZADO['CUENTA'],
      'SALARIO_BASE'         => $m_DATOS_ENCABEZADO['SALARIO_BASE'],
      'SALARIO_DIARIO'       => $m_DATOS_ENCABEZADO['SALARIO_DIARIO'],
      'CLAVEENTFED'          => $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'ESTADO', "SELECT ESTADO FROM cat_cfdi_estado WHERE ID = {$m_DATOS_ENCABEZADO['ESTADO']}"),
      'ANTIGUEDAD'           => $m_DATOS_ENCABEZADO['DIA'],
      'TIPO_NOMINA'          => $m_DATOS_ENCABEZADO['TIPO_NOMINA'],
    );

    $m_OPCIONALES = array
    (
      'VERSION'              => '1.2',
      'BANCO'                => $m_DATOS_ENCABEZADO['BANCO'],
      'FECHA_PAGO'           => $m_DATOS_ENCABEZADO['FECHA_PAGO'],
      'FECHA_INICIAL_PAGO'   => $m_DATOS_ENCABEZADO['FECHA_INICIAL_PAGO'],
      'FECHA_FINAL_PAGO'     => $m_DATOS_ENCABEZADO['FECHA_FINAL_PAGO'],
      'DIAS_PAGADOS'         => $m_DATOS_ENCABEZADO['DIAS_PAGADOS'],
      'CLABE'                => $m_DATOS_ENCABEZADO['CLABE'],
      'JORNADA'              => $m_DATOS_ENCABEZADO['JORNADA'],
      'EMAIL'                => $m_DATOS_ENCABEZADO['EMAIL'],
  	  'ORIGEN_RECURSO'       => $m_DATOS_ENCABEZADO['ORIGEN_RECURSO'],
  	  'RFC_PATRON_ORIGEN'    => $m_DATOS_ENCABEZADO['RFC_PATRON_ORIGEN'],
      'MONEDA'               => $m_DATOS_ENCABEZADO['MONEDA'],
    );
    if( !$this->validar_atributos( 'NOMINA', $m_ENCABEZADO ) )
    	return false;

    foreach($m_OPCIONALES as $e_INDICE => $e_CAMPO)
    {
    	$e_CAMPO = trim($e_CAMPO);
    	if(!empty($e_CAMPO))
    	{
    		$m_ENCABEZADO[$e_INDICE] = $e_CAMPO;
    	}
    }

    return $m_ENCABEZADO;
  }

  function generar_elemento_nomina_percepciones($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_PERCEPCIONES  = "SELECT {$m_CAMPOS['TIPOPERCEPCION']} AS TIPO_PERCEPCION, {$m_CAMPOS['CLAVEPERC']} AS CLAVE, {$m_CAMPOS['SALARIODIARIOINTEGRADO']} AS SALARIO_DIARIO,\n";
  	$e_QUERY_PERCEPCIONES .="        {$m_CAMPOS['CONCEPTOPERCEPCION']} AS CONCEPTO, {$m_CAMPOS['IMPORTEGRAVADOPERCEPCION']} AS IMPORTE_GRAVADO, {$m_CAMPOS['CLAVEPERCEPCION']} AS CLAVE_PERCEPCION,\n";
  	$e_QUERY_PERCEPCIONES .="        {$m_CAMPOS['IMPORTEEXENTOPERCEPCION']} AS IMPORTE_EXCENTO, {$m_CAMPOS['FECHAINICIORELLABORALEMP']} AS FECHA_INICIO_LABORAL,\n";
  	$e_QUERY_PERCEPCIONES .="        PERIOD_DIFF(date_format(now(), '%Y%m'),date_format({$m_CAMPOS['FECHAINICIORELLABORALEMP']}, '%Y%m')) AS MESES\n";
  	$e_QUERY_PERCEPCIONES .="FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "\n";
  	$e_QUERY_PERCEPCIONES .="INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_perc ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_PERC_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID\n";
  	$e_QUERY_PERCEPCIONES .="LEFT JOIN {$m_CAMPOS['CATALOGOEMP']} ON TRA_ID = {$m_CAMPOS['CLIENTE']}\n";
  	$e_QUERY_PERCEPCIONES .="WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$e_ID_DOCUMENTO}'\n";

  	if($r_PERCEPCIONES = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource("MY_QUERY", $e_QUERY_PERCEPCIONES))
  	{
  	  $e_TOTAL_INDEMNIZACION  = 0;
  	  $e_TOTAL_JUBILACION  = 0;
  	  $e_CONTADOR = 0;
  	  $e_TOTAL_INCAPACIDAD = 0;

  	  while($a_PERCEPCION = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_PERCEPCIONES))
  	  {
  	    $m_PERCEPCIONES['DETALLES'][] = array
        (
          'TIPO_PERCEPCION'  => $a_PERCEPCION['TIPO_PERCEPCION'],
          'CLAVE'            => $a_PERCEPCION['CLAVE'],
          'CLAVE_PERCEPCION' => $a_PERCEPCION['CLAVE_PERCEPCION'],
          'CONCEPTO'         => $a_PERCEPCION['CONCEPTO'],
          'IMPORTE_GRAVADO'  => $this->formatear_decimales_hacienda($a_PERCEPCION['IMPORTE_GRAVADO'],2),
          'IMPORTE_EXCENTO'  => $this->formatear_decimales_hacienda($a_PERCEPCION['IMPORTE_EXCENTO'],2)
        );
        //Atributo condicional para expresar el importe exento y gravado de las claves tipo percepci�n 022 Prima por Antig�edad, 023 Pagos por separaci�n y 025 Indemnizaciones.
  	    if( $a_PERCEPCION['TIPO_PERCEPCION'] == '022' || $a_PERCEPCION['TIPO_PERCEPCION'] == '023' || $a_PERCEPCION['TIPO_PERCEPCION'] == '025' )
  	    {
  	      $e_TOTAL_INDEMNIZACION = $e_TOTAL_INDEMNIZACION + $a_PERCEPCION['IMPORTE_GRAVADO'] + $a_PERCEPCION['IMPORTE_EXCENTO'] ;
  	    }
  	    elseif( $a_PERCEPCION['TIPO_PERCEPCION'] == '039' || $a_PERCEPCION['TIPO_PERCEPCION'] == '044' )
  	    {
  	      $e_TOTAL_JUBILACION = $e_TOTAL_JUBILACION + $a_PERCEPCION['IMPORTE_GRAVADO'] + $a_PERCEPCION['IMPORTE_EXCENTO'];
  	    }
  	    else{
  	      //Obtener TotalSueldos
  	      $m_PERCEPCIONES['TOTAL_SUELDOS'] += $this->formatear_decimales_hacienda($a_PERCEPCION['IMPORTE_GRAVADO'],2) + $this->formatear_decimales_hacienda($a_PERCEPCION['IMPORTE_EXCENTO'],2);
  	    }
  	    //Tipo de percepcion incapacidad
  	    if( $a_PERCEPCION['TIPO_PERCEPCION'] == '014' )
  	    {
  	      $e_TOTAL_INCAPACIDAD = $e_TOTAL_INCAPACIDAD + $a_PERCEPCION['IMPORTE_GRAVADO'] + $a_PERCEPCION['IMPORTE_EXCENTO'];
  	    }
  	    //Atributo condicional para expresar el importe exento y gravado de las claves tipo percepci�n 039 Jubilaciones, pensiones o haberes de retiro en una exhibici�n y 044 Jubilaciones, pensiones o haberes de retiro en parcialidades.
  	    $m_PERCEPCIONES['TOTAL_GRAVADO'] += $this->formatear_decimales_hacienda($a_PERCEPCION['IMPORTE_GRAVADO'],2);
  	    $m_PERCEPCIONES['TOTAL_EXCENTO'] += $this->formatear_decimales_hacienda($a_PERCEPCION['IMPORTE_EXCENTO'],2);

  	    //CALCULAR A�OS
  	    //OBTENER RESIDUO
  	    //OPEREACION DE REDONDEO
  	    $e_MESES = round($a_PERCEPCION['MESES'] / 12);

  	    if( !$this->validar_atributos( 'NOMINA', $m_PERCEPCIONES['DETALLES'][$e_CONTADOR] ) )
  	    	return false;

  	    $e_CONTADOR++;
  	  }
  	  $m_PERCEPCIONES['SALARIO_DIARIO']      = $a_PERCEPCION['SALARIO_DIARIO'];
  	  $m_PERCEPCIONES['TOTAL_ANNIO']         = $e_MESES;
  	  $m_PERCEPCIONES['TOTAL_INDEMNIZACION'] = $e_TOTAL_INDEMNIZACION ? $this->formatear_decimales_hacienda($e_TOTAL_INDEMNIZACION,2) : "";
  	  $m_PERCEPCIONES['TOTAL_JUBILACION']    = $e_TOTAL_JUBILACION ? $this->formatear_decimales_hacienda($e_TOTAL_JUBILACION,2) : "";
  	  $m_PERCEPCIONES['TOTAL_GRAVADO']       = $this->formatear_decimales_hacienda($m_PERCEPCIONES['TOTAL_GRAVADO'],2);
  	  $m_PERCEPCIONES['TOTAL_EXCENTO']       = $this->formatear_decimales_hacienda($m_PERCEPCIONES['TOTAL_EXCENTO'],2);
  	  $m_PERCEPCIONES['TOTAL_SUELDOS']       = $this->formatear_decimales_hacienda($m_PERCEPCIONES['TOTAL_SUELDOS'],2);
  	  $m_PERCEPCIONES['TOTAL_INCAPACIDAD']       = $this->formatear_decimales_hacienda($e_TOTAL_INCAPACIDAD,2);
    }


    return $m_PERCEPCIONES;
  }

  /**
   * Obtiene las percepciones del documento nomina para la generaci�n del cfdi
   *
   * @param int $e_ID_DOCUMENTO el id del documento
   * @return alfanumerico
   */
  function generar_elemento_nomina_deducciones($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_DEDUCCIONES  = "SELECT {$m_CAMPOS['TIPODEDUCCION']} AS TIPO_DEDUCCION,{$m_CAMPOS['CLAVEDED']} AS CLAVE, {$m_CAMPOS['CLAVEDEDUCCION']} AS CLAVE_DEDUCCION,\n";
  	$e_QUERY_DEDUCCIONES .= "       {$m_CAMPOS['CONCEPTODEDUCCION']} AS CONCEPTO,{$m_CAMPOS['IMPORTEGRAVADODEDUCCION']} AS IMPORTE_GRAVADO,{$m_CAMPOS['IMPORTEEXENTODEDUCCION']} AS IMPORTE_EXCENTO\n";
  	$e_QUERY_DEDUCCIONES .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "\n";
  	$e_QUERY_DEDUCCIONES .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_dedc ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_DEDC_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID\n";
  	$e_QUERY_DEDUCCIONES .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$e_ID_DOCUMENTO}'\n";

  	if($r_DEDUCCIONES = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource("MY_QUERY", $e_QUERY_DEDUCCIONES))
  	{
  	  $e_CONTADOR = 0;
  	  $e_TOTAL_INCAPACIDAD = 0;
  	  while($a_DEDUCCION = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_DEDUCCIONES))
  	  {
  	    $m_DEDUCCIONES['DETALLES'][] = array
  		(
  		  'TIPO_DEDUCCION'  => $a_DEDUCCION['TIPO_DEDUCCION'],
  		  'CLAVE'           => $a_DEDUCCION['CLAVE'],
  		  'CLAVE_DEDUCCION' => $a_DEDUCCION['CLAVE_DEDUCCION'],
  		  'CONCEPTO'        => $a_DEDUCCION['CONCEPTO'],
  		  'IMPORTE_GRAVADO' => $this->formatear_decimales_hacienda($a_DEDUCCION['IMPORTE_GRAVADO'],2),
  		  'IMPORTE_EXCENTO' => $this->formatear_decimales_hacienda($a_DEDUCCION['IMPORTE_EXCENTO'],2)
  		);

  	    if( !$this->validar_atributos( 'NOMINA', $m_DEDUCCIONES['DETALLES'][$e_CONTADOR] ) )
  	    	return false;

  	    $e_CONTADOR++;
  		$m_DEDUCCIONES['TOTAL_GRAVADO'] += $a_DEDUCCION['IMPORTE_GRAVADO'];
  		$m_DEDUCCIONES['TOTAL_EXCENTO'] += $a_DEDUCCION['IMPORTE_EXCENTO'];
  		if( $a_DEDUCCION['TIPO_DEDUCCION'] == '006' )
  		{
  			$e_TOTAL_INCAPACIDAD += $a_DEDUCCION['IMPORTE_GRAVADO'] + $a_DEDUCCION['IMPORTE_EXCENTO'];
  		}

  	  }
  	  $m_DEDUCCIONES['TOTAL_GRAVADO']     = $this->formatear_decimales_hacienda($m_DEDUCCIONES['TOTAL_GRAVADO'],2);
  	  $m_DEDUCCIONES['TOTAL_EXCENTO']     = $this->formatear_decimales_hacienda($m_DEDUCCIONES['TOTAL_EXCENTO'],2);
  	  $m_DEDUCCIONES['TOTAL_INCAPACIDAD'] = $this->formatear_decimales_hacienda($e_TOTAL_INCAPACIDAD,2);
    }
    return $m_DEDUCCIONES;
  }


 /**
   * Obtiene las percepciones del documento nomina para la generaci�n del cfdi
   *
   * @param int $e_ID_DOCUMENTO el id del documento
   * @return alfanumerico
   */
  function generar_elemento_nomina_incapacidades($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_INCAPACIDADES  = "SELECT  {$m_CAMPOS['DIASINCAPACIDAD']} AS DIAS_INCAPACIDAD,{$m_CAMPOS['TIPOINCAPACIDAD']} AS TIPO_INCAPACIDAD,{$m_CAMPOS['IMPORTEINCAPACIDAD']} AS DESCUENTO\n";
  	$e_QUERY_INCAPACIDADES .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "\n";
  	$e_QUERY_INCAPACIDADES .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_inc ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_INC_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID\n";
  	$e_QUERY_INCAPACIDADES .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$e_ID_DOCUMENTO}'\n";

  	if($r_INCAPACIDADES = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource("MY_QUERY", $e_QUERY_INCAPACIDADES))
  	{
  	  $e_CONTADOR = 0;
  	  while($a_INCAPACIDAD = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_INCAPACIDADES))
  	  {

        if($GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOINCAPACIDAD', "SELECT TIPOINCAPACIDAD FROM cat_cfdi_tipoincapacidad WHERE ID = {$a_INCAPACIDAD['TIPO_INCAPACIDAD']}"))
          $a_INCAPACIDAD['TIPO_INCAPACIDAD'] = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOINCAPACIDAD', "SELECT TIPOINCAPACIDAD FROM cat_cfdi_tipoincapacidad WHERE ID = {$a_INCAPACIDAD['TIPO_INCAPACIDAD']}");


  	  	$e_IMPORTEMONETRIO += $this->formatear_decimales_hacienda($a_INCAPACIDAD['DESCUENTO'],2);

  		$m_INCAPACIDADES['DETALLES'][] = array
  		(
  		  'DIAS_INCAPACIDAD'  => $a_INCAPACIDAD['DIAS_INCAPACIDAD'],
  		  'TIPO_INCAPACIDAD'  => $a_INCAPACIDAD['TIPO_INCAPACIDAD'],
  	      'DESCUENTO'         => $this->formatear_decimales_hacienda($a_INCAPACIDAD['DESCUENTO'],2),
  		);

  		if( !$this->validar_atributos( 'NOMINA', $m_INCAPACIDADES['DETALLES'][$e_CONTADOR] ) )
  			return false;

  		$e_CONTADOR++;
  	  }
  	  $m_INCAPACIDADES['TOTAL_IMP_MONETARIO'] = $e_IMPORTEMONETRIO;
  	  if( !$this->validar_atributos( 'NOMINA', $m_INCAPACIDADES['TOTAL_IMP_MONETARIO'] ) )
  	  	return false;

  	}
  	return $m_INCAPACIDADES;
  }


  /**
   * Obtiene las percepciones del documento nomina para la generaci�n del cfdi
   *
   * @param int $e_ID_DOCUMENTO el id del documento
   * @return alfanumerico
   */
  function generar_elemento_nomina_horasextra($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_HORASEXTRA  = "SELECT {$m_CAMPOS['DIASHORASEXTRA']} AS DIAS, {$m_CAMPOS['TIPOHORASEXTRA']} AS TIPO_HORAS,\n";
  	$e_QUERY_HORASEXTRA .= "       {$m_CAMPOS['HORASEXTRA']} AS HORAS_EXTRA,{$m_CAMPOS['IMPORTEPAGADOHORASEXTRA']} AS IMPORTE\n";
  	$e_QUERY_HORASEXTRA .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . " \n";
  	$e_QUERY_HORASEXTRA .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_hrex ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_HREX_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID\n";
  	$e_QUERY_HORASEXTRA .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$e_ID_DOCUMENTO}'\n";

  	if($r_HORASEXTRA = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource("MY_QUERY", $e_QUERY_HORASEXTRA))
  	{
  	  $e_CONTADOR = 0;
  	  while($a_HORASEXTRA = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_HORASEXTRA))
  	  {

        if($GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOHORAS', "SELECT TIPOHORAS FROM cat_cfdi_tipohoras WHERE ID = {$a_HORASEXTRA['TIPO_HORAS']}"))
  	  	  $a_HORASEXTRA['TIPO_HORAS'] = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOHORAS', "SELECT TIPOHORAS FROM cat_cfdi_tipohoras WHERE ID = {$a_HORASEXTRA['TIPO_HORAS']}");

  	    $m_HORASEXTRAS['DETALLES'][] = array
  		(
  		  'DIAS'        => ($a_HORASEXTRA['DIAS']),
  		  'TIPO_HORAS'  => $a_HORASEXTRA['TIPO_HORAS'],
  		  'HORAS_EXTRA' => $a_HORASEXTRA['HORAS_EXTRA'],
  		  'IMPORTE'     => $this->formatear_decimales_hacienda($a_HORASEXTRA['IMPORTE'],2),
  		);

  	    if( !$this->validar_atributos( 'NOMINA', $m_HORASEXTRAS['DETALLES'][$e_CONTADOR] ) )
  	      return false;

  	    $e_CONTADOR++;
  	  }
  	}
  	return $m_HORASEXTRAS;
  }

  /**
   * Obtiene los otros pagos del documento nomina para la generaci�n del cfdi
   *
   * @param int $e_ID_DOCUMENTO el id del documento
   * @return alfanumerico
   */
  function generar_elemento_nomina_otrospagos($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_OTROSPAGOS  = "SELECT  D_" . $m_CAMPOS['DOC_SIGLA'] . "_OPAG_ID AS ID, {$m_CAMPOS['CLAVEOP']} AS CLAVE, {$m_CAMPOS['CLAVEOTROPAGO']} AS CLAVE_OTROS_PAGOS,\n";
  	$e_QUERY_OTROSPAGOS .= "        {$m_CAMPOS['TIPOOTROPAGO']} AS TIPO_PAGO, {$m_CAMPOS['CONCEPTOOTROPAGO']} AS CONCEPTO, {$m_CAMPOS['IMPORTEOTROPAGO']} AS IMPORTE,\n";
  	$e_QUERY_OTROSPAGOS .= "        {$m_CAMPOS['SUBSIDIOOTROPAGO']} AS SUBSIDIO, {$m_CAMPOS['SALDOAFAVOROTROPAGO']} AS SALDO_A_FAVOR, {$m_CAMPOS['ANNIOOTROPAGO']} AS ANNIO\n";
  	$e_QUERY_OTROSPAGOS .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "\n";
  	$e_QUERY_OTROSPAGOS .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_opag ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_OPAG_ENCABEZADO = " . $m_CAMPOS['DOC_SIGLA'] . "_ID\n";
  	$e_QUERY_OTROSPAGOS .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO ='{$e_ID_DOCUMENTO}'        \n";

  	if($r_OTROSPAGOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource("MY_QUERY", $e_QUERY_OTROSPAGOS))
  	{
  	  $e_CONTADOR = 0;
  	  while($a_OTROSPAGOS = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_OTROSPAGOS))
  	  {
        if($GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOOTROPAGO', "SELECT TIPOOTROPAGO FROM cat_cfdi_tipootropago WHERE ID = {$a_OTROSPAGOS['CLAVE']}"))
          $a_OTROSPAGOS['CLAVE'] = $GLOBALS[SYSTEM][DBS][DB_SAT]->get_field('MY_QUERY', 'TIPOOTROPAGO', "SELECT TIPOOTROPAGO FROM cat_cfdi_tipootropago WHERE ID = {$a_OTROSPAGOS['CLAVE']}");

  	  	if($a_OTROSPAGOS['CLAVE'] != '001' && $a_OTROSPAGOS['CLAVE'] != '002' && $a_OTROSPAGOS['CLAVE'] != '003' && $a_OTROSPAGOS['CLAVE'] != '004' && $a_OTROSPAGOS['CLAVE'] != '005')
  	  		$a_OTROSPAGOS['CLAVE'] = '999';

  		$m_OTROSPAGOS['DETALLES'][] = array
  		(
  		  'ID'                      => ($a_OTROSPAGOS['ID']),
  		  'CLAVE_P'                 => ($a_OTROSPAGOS['CLAVE']),
  		  'CLAVE_OTROS_PAGOS'       => ($a_OTROSPAGOS['CLAVE_OTROS_PAGOS']),
  		  'TIPO_PAGO'               => $a_OTROSPAGOS['TIPO_PAGO'],
  		  'CONCEPTO'                => $a_OTROSPAGOS['CONCEPTO'],
  		  'IMPORTE'                 => $this->formatear_decimales_hacienda($a_OTROSPAGOS['IMPORTE'],2),
  		  'SUBSIDIO'                => $a_OTROSPAGOS['SUBSIDIO'] ? $a_OTROSPAGOS['SUBSIDIO'] : '0.00',
  		  'SALDO_A_FAVOR'           => $a_OTROSPAGOS['SALDO_A_FAVOR'] ? $a_OTROSPAGOS['SALDO_A_FAVOR'] : '0.00',
  		  'REMANENTE_SALDO_A_FAVOR' => '0.00',
  		  'ANNIO'                   => $a_OTROSPAGOS['ANNIO']
  		);

  		if( !$this->validar_atributos( 'NOMINA', $m_OTROSPAGOS['DETALLES'][$e_CONTADOR] ) )
  			return false;

  		$e_CONTADOR++;
  	    $m_OTROSPAGOS['TOTAL_IMPORTE'] += $this->formatear_decimales_hacienda($a_OTROSPAGOS['IMPORTE'],2);
  	  }
  	}
  	return $m_OTROSPAGOS;
  }


  /**
   * Obtiene las subcontrataciones del documento nomina para la generaci�n del cfdi
   *
   * @param int $e_ID_DOCUMENTO el id del documento
   * @return alfanumerico
   */
  function generar_elemento_nomina_subcontratacion($e_ID_DOCUMENTO, $m_CAMPOS)
  {
  	$e_QUERY_SUBCONTRATACION  = "SELECT  D_" . $m_CAMPOS['DOC_SIGLA'] . "_SCON_ID AS ID, D_" . $m_CAMPOS['DOC_SIGLA'] . "_SCON_IMPUESTOS AS IMPUESTOS, \n";
  	$e_QUERY_SUBCONTRATACION .= "        {$m_CAMPOS['RFCLABORASCON']} AS RFC_LABORAL, {$m_CAMPOS['PORCENTAJETIEMPOSCON']} AS PORCENTAJE_TIEMPO\n";
  	$e_QUERY_SUBCONTRATACION .= "FROM doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "\n";
  	$e_QUERY_SUBCONTRATACION .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_scon ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_SCON_ENCABEZADO = " . $m_CAMPOS['DOC_SIGLA'] . "_ID\n";
  	$e_QUERY_SUBCONTRATACION .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO = '{$e_ID_DOCUMENTO}'\n";

  	if($r_SUBCONTRATACION = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource("MY_QUERY", $e_QUERY_SUBCONTRATACION))
  	{
  	  while($a_SUBCONTRATACION = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_SUBCONTRATACION))
  	  {
  	    $m_SUBCONTRATACION['DETALLES'][] = array
  		(
  		  'ID'                => ($a_SUBCONTRATACION['ID']),
  		  'IMPUESTOS'         => ($a_SUBCONTRATACION['IMPUESTOS']),
  		  'RFC_LABORAL'       => $a_SUBCONTRATACION['RFC_LABORAL'],
  		  'PORCENTAJE_TIEMPO' => $a_SUBCONTRATACION['PORCENTAJE_TIEMPO']
  		);
  	  }
  	}
  	return $m_SUBCONTRATACION;
  }

  /**
   * la siguiente funcion  genera el elemento comprobante para el documento xml
   * @return multitype:string NULL unknown
   */
  private function generar_elemento_comprobante( $e_SIGLA_STATUS )
  {
    $m_METODO_Y_FORMA_PAGO = $this->calcular_forma_pago( $e_SIGLA_STATUS );
    $e_MONEDA              = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['MONEDA']}"];
    $e_TIPO_CAMBIO         = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['TIPOCAMBIO']}"];

    if($this->TIPO_DOCUMENTO == 'NOMINA')
      $e_TIPO_CAMBIO = 1.0000;
    //***campos requeridos
    $m_ELEMENTO = array
    (
      'VERSION'           => $this->VERSION,
      'FOLIO'             => intval($this->DOCUMENTO_PADRE->DATA['DOC_DOC_FOLIO']),
      'FECHA'             => str_replace(' ', 'T', $this->DOCUMENTO_PADRE->DATA['DOC_DOC_TS']),
      'METODOPAGO'        => $m_METODO_Y_FORMA_PAGO['METODO_PAGO'],
      'FORMAPAGO'         => $m_METODO_Y_FORMA_PAGO['FORMA_PAGO'],
      'NOCERTIFICADO'     => $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['NUMERO_CERTIFICADO'],
      'CERTIFICADO'       => $this->CSD['CADENA_CERTIFICADO'],
      'TIPODECOMPROBANTE' => $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['TIPO_COMPROBANTE'],
      'LUGAREXPEDICION'   => $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['LUGAR_EXPEDICION'],
      'MONEDA'            => Utils::catalogo_obtener_atributo('cat_monedas', 'MON_ABREVIATURA', 'MON_ID', $e_MONEDA),
      'TIPO_CAMBIO'       => $this->formatear_decimales_hacienda($e_TIPO_CAMBIO, 2),
    );

    if( !$this->validar_atributos( 'COMPROBANTE', $m_ELEMENTO ) )
      return false;

    //***campos opcionales
    $m_OPCIONALES = array
    (
      'SERIE' => Utils::catalogo_obtener_atributo('cat_series', 'SER_PREFIJO', 'SER_ID', $this->DOCUMENTO_PADRE->DATA['DOC_DOC_SERIE']),
    );

    foreach( $m_OPCIONALES as $e_INDICE => $e_CAMPO )
    {
      $e_CAMPO = trim( $e_CAMPO );
      if( !empty( $e_CAMPO ) )
      {
        $m_ELEMENTO[$e_INDICE] = $e_CAMPO;
      }
    }

    return $m_ELEMENTO;
  }

  private function generar_elemento_emisor()
  {
    $m_ELEMENTO = array
    (
      'RFC'           => $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['RFC'],
      'REGIMENFISCAL' => $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['CODIGO_REGIMEN_FISCAL'],
    );

    if( !$this->validar_atributos( 'EMISOR', $m_ELEMENTO ) )
      return false;

    $m_OPCIONALES = array
    (
      'NOMBRE'        => $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['RAZON_SOCIAL'],
      'CURP'        => $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['CURP'],
    );

    foreach( $m_OPCIONALES as $e_INDICE => $e_CAMPO )
    {
      $e_CAMPO = trim( $e_CAMPO );
      if( !empty( $e_CAMPO ) )
      {
        $m_ELEMENTO[$e_INDICE] = $e_CAMPO;
      }
    }

    return $m_ELEMENTO;
  }

  private function generar_elemento_receptor( $e_SIGLA_STATUS )
  {
    $e_TABLA                    = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_STA_PREFIJO              = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO'];
    $e_CAMPO_SOLICITAR_FACTURA  = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA}' AND COLUMN_NAME = '{$e_STA_PREFIJO}_SOLICITAR_FACTURA'" );

    if ($e_CAMPO_SOLICITAR_FACTURA && !$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO']}_SOLICITAR_FACTURA"] )
    {
      //cambiar los datos por los del cliente generico
      $m_ELEMENTO = array
      (
        'RFC'     => 'XAXX010101000',
        'NOMBRE'  => 'CLIENTE MOSTRADOR',
        'USOCFDI' => $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['USOCFDI']}"]
      );
    }
    else
    {
      if( $this->TIPO_DOCUMENTO == 'NOMINA' )
      {
      	$this->obtener_datos_receptor_nomina($this->DOCUMENTO_PADRE->ID);
      	//***campos requeridos
      	$m_ELEMENTO = array
      	(
      	  'RFC'     => $this->RECEPTOR['RFC'],
      	  'USOCFDI' => "P01"
      	);
      }
      else
      {
        $this->obtener_datos_receptor( $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CLIENTE']}"] );
        //***campos requeridos
        $m_ELEMENTO = array
        (
          'RFC'     => $this->RECEPTOR['RFC'],
          'USOCFDI' => $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['USOCFDI']}"],
        );
      }

      //***campos opcionales
      $m_OPCIONALES = array
      (
        'NOMBRE' => $this->RECEPTOR['RAZON_SOCIAL'],
        'RESIDENCIAFISCAL' => $m_ELEMENTO['RFC'] == 'XEXX010101000' ? $this->RECEPTOR['SIGLA_PAIS'] : '',
        'NUMREGIDTRIB'     => $m_ELEMENTO['RFC'] == 'XEXX010101000' ? $this->RECEPTOR['NUMREGIDTRIB'] : '',
      );

      if ( $m_ELEMENTO['RFC'] == 'XEXX010101000' )
      {
        $m_ELEMENTO['RESIDENCIAFISCAL'] = $this->RECEPTOR['SIGLA_PAIS'];
        $m_ELEMENTO['NUMREGIDTRIB']     = $this->RECEPTOR['NUMREGIDTRIB'];
      }
      else
      {
        $m_OPCIONALES['RESIDENCIAFISCAL'] = '';
        $m_OPCIONALES['NUMREGIDTRIB']     = '';
      }

      if( !$this->validar_atributos( 'RECEPTOR', $m_ELEMENTO ) )
        return false;

      foreach ($m_OPCIONALES as $e_INDICE => $e_VALOR)
      {
        $e_VALOR = trim( $e_VALOR );
        if (!empty( $e_VALOR ))
        {
          $m_ELEMENTO[$e_INDICE] = $e_VALOR;
        }
      }
    }

    return $m_ELEMENTO;
  }

  private function generar_elemento_conceptos( $e_SIGLA_STATUS )
  {
    $m_ELEMENTO                  = array();
    $e_MAXIMO_DECIMALES_SAT      = 6;
    $e_TOTAL_DIGITOS_SAT         = 10 + $e_MAXIMO_DECIMALES_SAT;
    $e_DECIMALES_IMPORTE         = $this->obtener_decimales_validos_documento( $e_SIGLA_STATUS );
    $e_DIGITOS_IMPORTE           = 10 + $e_DECIMALES_IMPORTE;
    $e_TABLA                     = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_TABLA_D                   = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA_D'];
    $e_STA_PREFIJO_D             = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO_D'];
    $e_CAMPO_CANTIDAD            = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CANTIDAD'];
    $e_CAMPO_PRECIO_IMPORTE      = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRECIO_IMPORTE'];
    $e_CAMPO_PRECIO_SIN_DSCTO    = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRECIO_SIN_DSCTO'];
    $e_CAMPO_PRODUCTO            = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['PRODUCTO'];
    $e_CAMPO_UNIDAD_EQUIVALENCIA = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['UNIDAD'];
    $e_DOC_SIGLA                 = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['DOC_SIGLA'];
    $e_STA_SIGLA                 = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_SIGLA'];
    $e_STA_ID                    = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_ID'];

    // revisamos si contiene el campo FACTURA_GLOBAL
    if( isset( $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['FACTURA_GLOBAL'] ) )
    {
      $e_FACTURA_GLOBAL = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['FACTURA_GLOBAL'];
      $e_CONSOLIDACION_TICKETS = $this->DOCUMENTO_PADRE->STATUS["$e_STA_SIGLA"]->DATA["$e_FACTURA_GLOBAL"];
    }
    else
      $e_CONSOLIDACION_TICKETS = 0;

    $e_CAMPO_SERIE               = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA_D}' AND COLUMN_NAME = '{$e_STA_PREFIJO_D}_SERIE'" );
    $e_CAMPO_SERIE1              = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA_D}' AND COLUMN_NAME = '{$e_STA_PREFIJO_D}_SERIE1'" );
    $e_CAMPO_SERIE2              = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA_D}' AND COLUMN_NAME = '{$e_STA_PREFIJO_D}_SERIE2'" );
    $e_CAMPO_DESCRIPCION         = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA_D}' AND COLUMN_NAME = '{$e_STA_PREFIJO_D}_DESCRIPCION'" );
    $e_QUERY_CONCEPTOS           = '';
    $e_QUERY_IMPTOS_DETALLE      = '';

    switch( $this->TIPO_DOCUMENTO )
    {
      case 'FACTURA':
      case 'NCC':
      case 'NDC':
        // verificamos si es consolidacion de comprobantes simpes
        if( $e_CONSOLIDACION_TICKETS == 1 )
        {
          $e_QUERY_CONCEPTOS  =  "SELECT DOC_CON_DET_CONSOLIDADO, DOC_CON_DOCUMENTO_DESTINO, GROUP_CONCAT(CAST({$e_STA_PREFIJO_D}_ID AS CHAR) SEPARATOR ',') AS {$e_STA_PREFIJO_D}_ID,\n";
          $e_QUERY_CONCEPTOS .= "'01010101' AS CLAVE_PROD_SERV_SAT,'VENTA' AS DESCRIPCION,'1.000000' AS CANTIDAD,SERIE1,DOC_CON_DET_CONSOLIDADO,\n";

          if ( $e_CAMPO_PRECIO_SIN_DSCTO )
          {
            $e_QUERY_CONCEPTOS .= "CAST( SUM( ( {$e_CAMPO_PRECIO_SIN_DSCTO} * {$e_CAMPO_CANTIDAD} ) ) AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS PRECIO_UNITARIO,\n";
            $e_QUERY_CONCEPTOS .= "CAST( SUM( ( {$e_CAMPO_PRECIO_SIN_DSCTO} - {$e_CAMPO_PRECIO_IMPORTE} ) * {$e_CAMPO_CANTIDAD} ) AS DECIMAL($e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT) ) AS DESCUENTO,\n";
            $e_QUERY_CONCEPTOS .= "CAST( SUM( ( {$e_CAMPO_PRECIO_SIN_DSCTO} * {$e_CAMPO_CANTIDAD} ) ) AS DECIMAL($e_DIGITOS_IMPORTE, $e_DECIMALES_IMPORTE ) ) AS IMPORTE_PARTIDA\n";
          }
          else
          {
            $e_QUERY_CONCEPTOS .= "CAST( SUM( ( {$e_CAMPO_PRECIO_IMPORTE} * {$e_CAMPO_CANTIDAD} ) ) AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS PRECIO_UNITARIO,\n";
            $e_QUERY_CONCEPTOS .= "CAST( '0.0' AS DECIMAL( $e_TOTAL_DIGITOS_SAT,  $e_MAXIMO_DECIMALES_SAT ) ) AS DESCUENTO,\n";
            $e_QUERY_CONCEPTOS .= "CAST( SUM( ( {$e_CAMPO_PRECIO_IMPORTE} * {$e_CAMPO_CANTIDAD} ) ) AS DECIMAL( $e_DIGITOS_IMPORTE, $e_DECIMALES_IMPORTE ) ) AS IMPORTE_PARTIDA\n";
          }

          $e_QUERY_CONCEPTOS .= "FROM (\n";
          $e_QUERY_CONCEPTOS .= "     SELECT DOC_CON_ID, DOC_CON_DOCUMENTO_DESTINO, CONCAT(SER_PREFIJO,DOC_DOC_FOLIO) AS SERIE1\n";
          $e_QUERY_CONCEPTOS .= "     FROM doc_documentos_consolidados\n";
          $e_QUERY_CONCEPTOS .= "     JOIN doc_documentos ON DOC_CON_DOCUMENTO_ORIGEN = DOC_DOC_ID\n";
          $e_QUERY_CONCEPTOS .= "     JOIN cat_series ON DOC_DOC_SERIE = SER_ID\n";
          $e_QUERY_CONCEPTOS .= "     JOIN {$e_TABLA} ON {$e_DOC_SIGLA}_ID = DOC_CON_ENCABEZADO_ORIGEN\n";
          $e_QUERY_CONCEPTOS .= "     WHERE DOC_CON_DOCUMENTO_DESTINO = '{$this->DOCUMENTO_PADRE->ID}'\n";
          $e_QUERY_CONCEPTOS .= "     ORDER BY DOC_DOC_FOLIO) AS encabezado\n";
          $e_QUERY_CONCEPTOS .= "JOIN (";
          $e_QUERY_CONCEPTOS .= "      SELECT\n";
          $e_QUERY_CONCEPTOS .= "      {$e_STA_PREFIJO_D}_ID, DOC_CON_DET_CONSOLIDADO,\n";

          if( $e_CAMPO_PRECIO_SIN_DSCTO )
            $e_QUERY_CONCEPTOS .= "     {$e_CAMPO_PRECIO_SIN_DSCTO},\n";

          $e_QUERY_CONCEPTOS .= "      {$e_CAMPO_PRECIO_IMPORTE},$e_CAMPO_CANTIDAD\n";
          $e_QUERY_CONCEPTOS .= "      FROM doc_documentos_consolidados_detalle\n";
          $e_QUERY_CONCEPTOS .= "      JOIN {$e_TABLA_D} ON DOC_CON_DET_ID_DETALLE = {$e_STA_PREFIJO_D}_ID\n";
          $e_QUERY_CONCEPTOS .= "      WHERE DOC_CON_DET_STATUS = '{$e_STA_ID}'\n";
          $e_QUERY_CONCEPTOS .= "      ) AS detalle\n";
          $e_QUERY_CONCEPTOS .= "ON DOC_CON_DET_CONSOLIDADO = DOC_CON_ID\n";
          $e_QUERY_CONCEPTOS .= "GROUP BY DOC_CON_DET_CONSOLIDADO\n";
          $e_QUERY_CONCEPTOS .= "ORDER BY SERIE1\n";
        }
        else
        {
          $e_QUERY_CONCEPTOS .= "   SELECT {$e_STA_PREFIJO_D}_ID, {$e_CAMPO_PRODUCTO} AS ID_PRODUCTO, productos.CAM_PER_CLAVE_PRODUCTO_SERVICIO AS CLAVE_PROD_SERV_SAT, productos.PRO_NOMBRE AS PRODUCTO,\n";
          $e_QUERY_CONCEPTOS .= "          productos.PRO_CODIGO AS CODIGO_PRODUCTO, CAST( {$e_CAMPO_CANTIDAD} AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS CANTIDAD,\n";

          if ( $e_CAMPO_PRECIO_SIN_DSCTO )
          {
            $e_QUERY_CONCEPTOS .= "          CAST( {$e_CAMPO_PRECIO_SIN_DSCTO} AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS PRECIO_UNITARIO, \n";
            $e_QUERY_CONCEPTOS .= "          CAST( ( ( {$e_CAMPO_PRECIO_SIN_DSCTO} - {$e_CAMPO_PRECIO_IMPORTE} ) * {$e_CAMPO_CANTIDAD} ) AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS DESCUENTO,\n";
            $e_QUERY_CONCEPTOS .= "          CAST( ( {$e_CAMPO_PRECIO_SIN_DSCTO} * {$e_CAMPO_CANTIDAD} ) AS DECIMAL( $e_DIGITOS_IMPORTE, $e_DECIMALES_IMPORTE ) ) AS IMPORTE_PARTIDA, \n";
          }
          else
          {
            $e_QUERY_CONCEPTOS .= "          CAST( {$e_CAMPO_PRECIO_IMPORTE} AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS PRECIO_UNITARIO, \n";
            $e_QUERY_CONCEPTOS .= "          CAST( '0.0' AS DECIMAL( $e_TOTAL_DIGITOS_SAT, $e_MAXIMO_DECIMALES_SAT ) ) AS DESCUENTO,\n";
            $e_QUERY_CONCEPTOS .= "          CAST( ( {$e_CAMPO_PRECIO_IMPORTE} * {$e_CAMPO_CANTIDAD} ) AS DECIMAL( $e_DIGITOS_IMPORTE, $e_DECIMALES_IMPORTE ) ) AS IMPORTE_PARTIDA, \n";
          }

          if ( ( $this->OBTENER_UNIDAD == 'EQUIVALENCIA' || $this->OBTENER_UNIDAD == 'DOCUMENTO' ) && ( $e_CAMPO_UNIDAD_EQUIVALENCIA != '' ) )
            $e_QUERY_CONCEPTOS .= "          {$e_CAMPO_UNIDAD_EQUIVALENCIA} AS ID_UNIDAD_EQUIVALENCIA,\n";
          else
            $e_QUERY_CONCEPTOS .= "          NULL AS ID_UNIDAD_EQUIVALENCIA,\n";

          if( $e_CAMPO_SERIE )
            $e_QUERY_CONCEPTOS .= "          {$e_CAMPO_SERIE} as SERIE1, {$e_CAMPO_SERIE} AS SERIE2,\n";
          elseif( $e_CAMPO_SERIE1 && $e_CAMPO_SERIE2 )
            $e_QUERY_CONCEPTOS .= "          {$e_CAMPO_SERIE1} as SERIE1, {$e_CAMPO_SERIE2} AS SERIE2,\n";
          elseif( $e_CAMPO_SERIE1 && !$e_CAMPO_SERIE2 )
            $e_QUERY_CONCEPTOS .= "          {$e_CAMPO_SERIE1} as SERIE1, {$e_CAMPO_SERIE1} AS SERIE2,\n";
          elseif ( !$e_CAMPO_SERIE1 && $e_CAMPO_SERIE2 )
            $e_QUERY_CONCEPTOS .= "          {$e_CAMPO_SERIE2} as SERIE1, {$e_CAMPO_SERIE2} AS SERIE2,\n";
          else
            $e_QUERY_CONCEPTOS .= "          '' as SERIE1, '' AS SERIE2,\n";

          if( $e_CAMPO_DESCRIPCION )
            $e_QUERY_CONCEPTOS .= "          REPLACE(REPLACE(trim({$e_CAMPO_DESCRIPCION}), '\r', ''), '\n', '') AS DESCRIPCION\n";
          else
            $e_QUERY_CONCEPTOS .= "          trim(productos.PRO_NOMBRE) AS DESCRIPCION\n";

          $e_QUERY_CONCEPTOS .= "     FROM {$e_TABLA}\n";
          $e_QUERY_CONCEPTOS .= "     JOIN {$e_TABLA_D} ON {$e_STA_PREFIJO_D}_ENCABEZADO = {$this->DOCUMENTO_PADRE->SIGLA}_ID\n";
          $e_QUERY_CONCEPTOS .= "     JOIN cat_productos productos ON productos.PRO_ID = {$e_CAMPO_PRODUCTO}\n";
          $e_QUERY_CONCEPTOS .= "    WHERE {$this->DOCUMENTO_PADRE->SIGLA}_DOCUMENTO = '{$this->DOCUMENTO_PADRE->ID}'\n";
          $e_QUERY_CONCEPTOS .= " ORDER BY {$e_STA_PREFIJO_D}_ID";
        }

        $e_QUERY_IMPTOS_DETALLE .= "SELECT DOC_IMP_TIPO, DOC_IMP_TASA, IMP_SIGLA,\n";
        $e_QUERY_IMPTOS_DETALLE .= " CAST( ( SUM( DOC_IMP_CANTIDAD * DOC_IMP_MONTO ) ) AS DECIMAL( $e_DIGITOS_IMPORTE, $e_DECIMALES_IMPORTE ) ) AS IMPUESTO\n";
        $e_QUERY_IMPTOS_DETALLE .= "  FROM {$e_TABLA_D}\n";
        $e_QUERY_IMPTOS_DETALLE .= "  JOIN doc_documentos_impuestos ON DOC_IMP_STATUS = '{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_ID']}'\n";
        $e_QUERY_IMPTOS_DETALLE .= "                               AND DOC_IMP_DOCUMENTO = '{$this->DOCUMENTO_PADRE->ID}'\n";
        $e_QUERY_IMPTOS_DETALLE .= "                               AND DOC_IMP_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'\n";
        $e_QUERY_IMPTOS_DETALLE .= "                               AND DOC_IMP_DETALLE = {$e_STA_PREFIJO_D}_ID\n";
        $e_QUERY_IMPTOS_DETALLE .= "  JOIN cat_impuestos ON DOC_IMP_IMPUESTO = IMP_ID\n";
        $e_QUERY_IMPTOS_DETALLE .= " WHERE DOC_IMP_DOCUMENTO = '{$this->DOCUMENTO_PADRE->ID}'\n";

        if ( $e_CONSOLIDACION_TICKETS == 1 )
          $e_QUERY_IMPTOS_DETALLE .= "   AND DOC_IMP_DETALLE IN (@PARAMETRO2)\n";
        else
          $e_QUERY_IMPTOS_DETALLE .= "   AND DOC_IMP_DETALLE = '@PARAMETRO2'\n";

        $e_QUERY_IMPTOS_DETALLE .= "GROUP BY DOC_IMP_TIPO, DOC_IMP_TASA, IMP_SIGLA\n";
        $e_QUERY_IMPTOS_DETALLE .= " ORDER BY DOC_IMP_TIPO DESC\n";

      break;

      case 'CDP':
        $e_QUERY_CONCEPTOS .= "SELECT '84111506' AS CLAVE_PROD_SERV_SAT, 'ACT' AS CLAVE_UNIDAD_SAT, 'Actividad' AS UNIDAD, 1 AS CANTIDAD, 'Pago' AS PRODUCTO, 0 as PRECIO_UNITARIO, 0 as IMPORTE_PARTIDA, 0 AS DESCUENTO";
        break;

      case 'NOMINA':
      	$m_CAMPOS = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"];

        $e_QUERY_CONCEPTOS .= "SELECT '84111505' AS CLAVE_PROD_SERV_SAT, 'ACT' AS CLAVE_UNIDAD_SAT, 'Actividad' AS UNIDAD, 1 AS CANTIDAD, 'Pago de n�mina' AS PRODUCTO, 600 as PRECIO_UNITARIO, 600 as IMPORTE_PARTIDA, 0 AS DESCUENTO";

        //calcular totales documentos
        $e_QUERY_TOTALES_CONCEPTO  = " SELECT  '84111505' AS CLAVE_PROD_SERV_SAT, 'ACT' AS CLAVE_UNIDAD_SAT, 'Actividad' AS UNIDAD, 1 AS CANTIDAD, 'Pago de n�mina' AS PRODUCTO,  '0' AS PRO_ID, '0' AS IMPUESTOS, ({$m_CAMPOS['IMPORTEGRAVADOPERCEPCION']} + {$m_CAMPOS['IMPORTEEXENTOPERCEPCION']}) AS PRECIO \n";
        $e_QUERY_TOTALES_CONCEPTO  .= "FROM {$e_TABLA}\n";
        $e_QUERY_TOTALES_CONCEPTO  .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_" . strtolower ($m_CAMPOS['STA_SIGLA_PERCEPCIONES']) . " ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_" . $m_CAMPOS['STA_SIGLA_PERCEPCIONES'] . "_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID \n";
        $e_QUERY_TOTALES_CONCEPTO  .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$this->DOCUMENTO_PADRE->ID}' \n";
        $e_QUERY_TOTALES_CONCEPTO  .= "UNION ALL \n";
        $e_QUERY_TOTALES_CONCEPTO  .= " SELECT  '84111505' AS CLAVE_PROD_SERV_SAT, 'ACT' AS CLAVE_UNIDAD_SAT, 'Actividad' AS UNIDAD, 1 AS CANTIDAD, 'Pago de n�mina' AS PRODUCTO, '0' AS PRO_ID, '0' AS IMPUESTOS, ({$m_CAMPOS['IMPORTEOTROPAGO']}) AS PRECIO \n";
        $e_QUERY_TOTALES_CONCEPTO  .= "FROM {$e_TABLA}\n";
        $e_QUERY_TOTALES_CONCEPTO  .= "INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_" . strtolower ($m_CAMPOS['STA_SIGLA_OTROSPAGOS']) . " ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_" . $m_CAMPOS['STA_SIGLA_OTROSPAGOS'] . "_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID \n";
        $e_QUERY_TOTALES_CONCEPTO  .= "WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$this->DOCUMENTO_PADRE->ID}'\n";

        $e_QUERY_DESCUENTO  ="SELECT {$m_CAMPOS['TIPODEDUCCION']} AS TIPO_DEDUCCION, SUM({$m_CAMPOS['IMPORTEGRAVADODEDUCCION']} + {$m_CAMPOS['IMPORTEEXENTODEDUCCION']}) AS DESCUENTO, \n";
        $e_QUERY_DESCUENTO .="       SUM({$m_CAMPOS['IMPORTEGRAVADODEDUCCION']}) AS IMPORTE_GRAVADO, SUM({$m_CAMPOS['IMPORTEEXENTODEDUCCION']}) AS IMPORTE_EXCENTO \n";
        $e_QUERY_DESCUENTO .="FROM {$e_TABLA} \n";
        $e_QUERY_DESCUENTO .="INNER JOIN doc_" . strtolower ($m_CAMPOS['DOC_SIGLA']) . "_" . strtolower ($m_CAMPOS['STA_SIGLA_DEDUCCIONES']) . " ON D_" . $m_CAMPOS['DOC_SIGLA'] . "_" . $m_CAMPOS['STA_SIGLA_DEDUCCIONES'] . "_ENCABEZADO=" . $m_CAMPOS['DOC_SIGLA'] . "_ID \n";
        $e_QUERY_DESCUENTO .="WHERE " . $m_CAMPOS['DOC_SIGLA'] . "_DOCUMENTO='{$this->DOCUMENTO_PADRE->ID}' \n";
        break;

      default:
        $e_QUERY_CONCEPTOS = 'SELECT 0';
    }

    if ( $e_CONSOLIDACION_TICKETS == 1 )
      $GLOBALS['SYSTEM']['DBS']['DB']->exec( 'SET group_concat_max_len = 10240;' );

    if($r_CONCEPTOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_CONCEPTOS ) )
    {
      //Inicializamos los totales de impuestos trasladados y retenidos
      $m_TOTALES_DOCUMENTO = array
      (
        'SUBTOTAL'  => 0.0,
        'DESCUENTO' => 0.0,
        'IMPUESTOS' => array
        (
          'TRASLADADOS' => array('GRAN_TOTAL' => 0.0),
          'RETENIDOS'   => array('GRAN_TOTAL' => 0.0),
        ),
      );
      while ( $a_CONCEPTOS = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_CONCEPTOS ) )
      {
        if ( trim($a_CONCEPTOS['DESCRIPCION']) )
          $e_DESCRIPCION = trim( $a_CONCEPTOS['DESCRIPCION'] );
        else
          $e_DESCRIPCION = trim( $a_CONCEPTOS['PRODUCTO'] );

        if( $this->TIPO_DOCUMENTO == 'CDP' || $this->TIPO_DOCUMENTO == 'NOMINA')
        {
          $m_DATOS_UNIDAD = array
          (
            'CLAVE_UNIDAD_SAT' => $a_CONCEPTOS['CLAVE_UNIDAD_SAT'],
          );
        }
        else
        {
          $m_PARAMS_UNIDAD = array
          (
            'ID_PRODUCTO'            => $a_CONCEPTOS['ID_PRODUCTO'],
            'ID_UNIDAD_EQUIVALENCIA' => $a_CONCEPTOS['ID_UNIDAD_EQUIVALENCIA']
          );
          if( $e_CONSOLIDACION_TICKETS == 0)
            $m_DATOS_UNIDAD  = $this->obtener_unidad( $m_PARAMS_UNIDAD );
          else
            $m_DATOS_UNIDAD = array('CLAVE_UNIDAD_SAT' => 'ACT', 'UNIDAD' => 'Actividad');
        }

        if ( $this->TIPO_DOCUMENTO == 'NOMINA' )
        {
          //se calculan los totales
          if($r_DATOS_TOTALES = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource('MY_QUERY', $e_QUERY_TOTALES_CONCEPTO))
          {
            while($a_DATOS_TOTALES = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_DATOS_TOTALES))
        	{
        	  $e_SUBTOTAL += $a_DATOS_TOTALES['CANTIDAD'] * $a_DATOS_TOTALES['PRECIO'];
        	  $e_IMPUESTOS += $a_DATOS_TOTALES['IMPUESTOS'];
        	  $e_DESCUENTO += $a_DATOS_TOTALES['CANTIDAD'] * $a_DATOS_TOTALES['DESCUENTO'];
        	}
        	$e_TOTAL = $e_SUBTOTAL - $e_DESCUENTO + $e_IMPUESTOS;

        	$a_CONCEPTOS['PRECIO_UNITARIO'] = $e_TOTAL;
        	$a_CONCEPTOS['IMPORTE_PARTIDA'] = $e_TOTAL;
          }

          if($r_DESCUENTO = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource('MY_QUERY', $e_QUERY_DESCUENTO))
          {
            if($a_DESCUENTO = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_DESCUENTO))
        	{
        	  $e_DESCUENTO = $a_DESCUENTO['DESCUENTO'];
        	  $a_CONCEPTOS['DESCUENTO'] = $a_DESCUENTO['DESCUENTO'];
        	}
          }
       }

       $m_PARTIDA = array
       (
         'CLAVEPRODSERV'   => $a_CONCEPTOS['CLAVE_PROD_SERV_SAT'],
         'CLAVEUNIDAD'     => $m_DATOS_UNIDAD['CLAVE_UNIDAD_SAT'],
         'CANTIDAD'        => $a_CONCEPTOS['CANTIDAD'],
         'DESCRIPCION'     => $e_DESCRIPCION,
         'VALORUNITARIO'   => $a_CONCEPTOS['PRECIO_UNITARIO'],
         'IMPORTE'         => $a_CONCEPTOS['IMPORTE_PARTIDA'],
       );

        $m_TOTALES_DOCUMENTO['SUBTOTAL'] += $a_CONCEPTOS['IMPORTE_PARTIDA'];

        if ( $this->TIPO_DOCUMENTO != 'CDP' )
          if ( !$this->validar_atributos( 'CONCEPTO', $m_PARTIDA ) )
            return false;

        //***Campos opcionales
        if ($a_CONCEPTOS['DESCUENTO'] > 0.0)
        {
          $m_TOTALES_DOCUMENTO['DESCUENTO'] += $a_CONCEPTOS['DESCUENTO'];
          $m_PARTIDA['DESCUENTO']            = $a_CONCEPTOS['DESCUENTO'];
        }

        //CAMPOS ADICONALES ADDENDA Y ADUANAS
        $m_PARTIDA['ID_PRODUCTO'] = $a_CONCEPTOS['ID_PRODUCTO'];
        // obtener numero de pedimento
         if ( $this->RECEPTOR['RFC'] != 'XEXX010101000' && $this->RECEPTOR['SIGLA_PAIS'] == "MEX" )
         {
           $m_PARTIDA['INFORMACIONADUANERA'] = $this->obtener_numero_pedimento($this->DOCUMENTO_PADRE->ID, $a_CONCEPTOS["{$e_STA_PREFIJO_D}_ID"]);
          if(!is_array($m_PARTIDA['INFORMACIONADUANERA']))
            unset($m_PARTIDA['INFORMACIONADUANERA']);
         }
        //FIN CAMPOS NUEVOS

        if ( $a_CONCEPTOS['SERIE1'] || $a_CONCEPTOS['SERIE2'] )
        {
          if ( $a_CONCEPTOS['SERIE1'] && $a_CONCEPTOS['SERIE2'] && ( $a_CONCEPTOS['SERIE1'] !== $a_CONCEPTOS['SERIE2'] ) )
            $m_PARTIDA['NOIDENTIFICACION'] = $a_CONCEPTOS['SERIE1'] . ' - ' . $a_CONCEPTOS['SERIE2'];
          elseif ($a_CONCEPTOS['SERIE1'])
            $m_PARTIDA['NOIDENTIFICACION'] = $a_CONCEPTOS['SERIE1'];
          else
            $m_PARTIDA['NOIDENTIFICACION'] = $a_CONCEPTOS['SERIE2'];
        }

        if ( $this->TIPO_DOCUMENTO != 'CDP' && $this->TIPO_DOCUMENTO != 'NOMINA' )
        {
          if($r_IMPUESTOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_IMPTOS_DETALLE, $a_CONCEPTOS["{$e_STA_PREFIJO_D}_ID"] ) )
          {
            $m_IMP = array();
            while( $a_IMPUESTO = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_IMPUESTOS ) )
            {
              $e_CLAVE_SAT   = $this->CONF_IMPUESTOS["{$a_IMPUESTO['IMP_SIGLA']}"]['CODIGO_SAT'];

              if($a_IMPUESTO['DOC_IMP_TIPO'] == 'T')
              {
                if( $this->CONF_IMPUESTOS["{$a_IMPUESTO['IMP_SIGLA']}"]['APLICA_TRASLADO'] )
                {
                  $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']['GRAN_TOTAL']                                      += $a_IMPUESTO['IMPUESTO'];
                  $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']["{$e_CLAVE_SAT}"]["{$a_IMPUESTO['DOC_IMP_TASA']}"] += $a_IMPUESTO['IMPUESTO'];
                }
              }
              else
              {
                if( $this->CONF_IMPUESTOS["{$a_IMPUESTO['IMP_SIGLA']}"]['APLICA_RETENCION'] )
                {
                  $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']['GRAN_TOTAL']                                      += $a_IMPUESTO['IMPUESTO'];
                  $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']["{$e_CLAVE_SAT}"]["{$a_IMPUESTO['DOC_IMP_TASA']}"] += $a_IMPUESTO['IMPUESTO'];
                }
              }

              $a_IMP = array
              (
                'BASE'      => $this->formatear_decimales_hacienda( ( $a_CONCEPTOS['IMPORTE_PARTIDA'] - $a_CONCEPTOS['DESCUENTO'] ) , $e_DECIMALES_IMPORTE ),
                'TIPO'      => $a_IMPUESTO['DOC_IMP_TIPO'],
                'IMPORTE'   => $a_IMPUESTO['IMPUESTO'],
                'TASA'      => $a_IMPUESTO['DOC_IMP_TASA'],
                'CLAVE_IMP' => $e_CLAVE_SAT,
              );

              if( !$this->validar_atributos( 'IMPUESTOS_UNCONCEPTO', array_merge( $a_IMP, array('DESCRIPCION' => $m_PARTIDA['DESCRIPCION'] ) )  ) )
                return false;

              $m_IMP[] = $a_IMP;
            }
          }
        }

        $m_PARTIDA['IMPUESTOS'] = $m_IMP;
        unset( $m_IMP );
        $m_ELEMENTO[] = $m_PARTIDA;
      }
    }
    $m_ELEMENTO['TOTALES_DOCUMENTO'] = $m_TOTALES_DOCUMENTO;
    return $m_ELEMENTO;
  }

  private function generar_elemento_pagos()
  {
    $e_CAMPO_RFC_BANCO_ORDENANTE  = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['RFC_EMISOR_BANCO_ORDENANTE'];
    $e_CAMPO_CUENTA_ORDENANTE     = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CUENTA_ORDENANTE'];
    $e_CAMPO_NUMERO_OPERACION     = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['NUMERO_OPERACION'];
    $e_CAMPO_CUENTA_BENEFICIARIO  = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CUENTA_BENEFICIARIO'];
    $e_SIGLA_DOC_CDP              = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['DOC_SIGLA'];
    $e_SIGLA_STA_CDP              = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['STA_SIGLA'];
    $e_DECIMALES_IMPORTE          = $this->obtener_decimales_validos_documento($e_SIGLA_STA_CDP);
    $e_DIGITOS_IMPORTE            = 10 + $e_DECIMALES_IMPORTE;

    $m_INFO_PAGOS = array();

    // Ingreso para Recibo de Cobranza
    $e_QUERY_INGRESO  = "SELECT doc_origen.DOC_DOC_ID AS DOC_ORIGEN, CONCAT(ser_origen.SER_PREFIJO,doc_origen.DOC_DOC_FOLIO) AS FOLIO_ORIGEN,\n";
    $e_QUERY_INGRESO .= "		   ing_origen.ING_TS AS TS_ORIGEN, ing_origen.ING_CAP_FECHA AS FECHA_ORIGEN,\n";
    $e_QUERY_INGRESO .= "			 ing_destino.ING_CAP_REFERENCIA AS NUMERO_OPERACION, ing_destino.ING_CAP_CUENTA AS CUENTA_BENEFICIARIO,\n";
    $e_QUERY_INGRESO .= "      ing_destino.ING_CAP_BANCO_ORDENANTE AS RFC_BANCO_ORDENANTE, ing_destino.ING_CAP_CUENTA_ORDENANTE AS NUM_CUENTA_ORDENANTE,\n";
    $e_QUERY_INGRESO .= "      LIS_NOMBRE AS BANCO_BENEFICIARIO, DOC_CON_DOCUMENTO_ORIGEN, DOC_CON_DOCUMENTO_DESTINO, ser_destino.SER_PREFIJO AS SERIE,\n";
    $e_QUERY_INGRESO .= "      consolidado.DOC_DOC_FOLIO AS FOLIO, ing_destino.ING_CAP_FECHA AS FECHA, CAM_PER_CODIGO AS FDP, MON_ABREVIATURA AS MONEDA,\n";
    $e_QUERY_INGRESO .= "      CAST( ing_destino.ING_CAP_MONTO AS DECIMAL( 12, 2 ) ) AS MONTO, ing_destino.ING_CAP_REFERENCIA AS REFERENCIA, CUE_NOMBRE,\n";
    $e_QUERY_INGRESO .= "      IF(ing_destino.ING_CAP_TIPO_CAMBIO = NULL OR ing_destino.ING_CAP_TIPO_CAMBIO = 0, '', ing_destino.ING_CAP_TIPO_CAMBIO) AS TIPO_CAMBIO,\n";
    $e_QUERY_INGRESO .= "      CUE_CUENTA AS CTA_BENEFICIARIO, CUE_CLABE AS CTA_BENEFICIARIO_CLABE, CAM_PER_CODIGO AS TIPO_CAD_PAGO\n";
    $e_QUERY_INGRESO .= "FROM doc_documentos as consolidado\n";
    $e_QUERY_INGRESO .= "JOIN cat_series AS ser_destino ON ser_destino.SER_ID = consolidado.DOC_DOC_SERIE\n";
    $e_QUERY_INGRESO .= "JOIN doc_ing AS ing_destino ON ing_destino.ING_DOCUMENTO = consolidado.DOC_DOC_ID\n";
    $e_QUERY_INGRESO .= "JOIN cat_clientes ON CLI_ID = ing_destino.ING_CAP_CLIENTE\n";
    $e_QUERY_INGRESO .= "JOIN doc_documentos_consolidados ON DOC_CON_DOCUMENTO_DESTINO = consolidado.DOC_DOC_ID\n";
    $e_QUERY_INGRESO .= "JOIN dat_cxc_movimientos AS cargos ON cargos.CXC_MOV_DOCUMENTO = DOC_CON_DOCUMENTO_DESTINO\n";
    $e_QUERY_INGRESO .= "JOIN cat_formaspago ON FDP_ID = ing_destino.ING_CAP_FORMA_PAGO\n";
    $e_QUERY_INGRESO .= "JOIN cat_monedas ON MON_ID = ing_destino.ING_CAP_MONEDA\n";
    $e_QUERY_INGRESO .= "JOIN cat_cuentas ON CUE_ID = ing_destino.ING_CAP_CUENTA\n";
    $e_QUERY_INGRESO .= "LEFT JOIN sys_listas ON LIS_ID = CUE_BANCO\n";
    $e_QUERY_INGRESO .= "JOIN doc_documentos AS doc_origen ON DOC_CON_DOCUMENTO_ORIGEN = doc_origen.DOC_DOC_ID\n";
    $e_QUERY_INGRESO .= "JOIN cat_series AS ser_origen ON doc_origen.DOC_DOC_SERIE = ser_origen.SER_ID\n";
    $e_QUERY_INGRESO .= "JOIN doc_ing AS ing_origen ON doc_origen.DOC_DOC_ID = ing_origen.ING_DOCUMENTO\n";
    $e_QUERY_INGRESO .= "WHERE consolidado.DOC_DOC_ID = '{$this->DOCUMENTO_PADRE->ID}'";
    file_put_contents("QUERY_INGRESO",$e_QUERY_INGRESO);

    //Informacion de cargos
    $e_QUERY_DOCUMENTO  = "SELECT cargo.CXC_MOV_MONTO AS CARGO, abono.CXC_MOV_MONTO AS ABONO, abono.CXC_MOV_MONTO_DISPONIBLE AS DISPONIBLE,\n";
    $e_QUERY_DOCUMENTO .= "       CAST(CXC_MOV_DET_MONTO AS DECIMAL( $e_DIGITOS_IMPORTE, $e_DECIMALES_IMPORTE  )) AS CARGO_ING, \n";
    $e_QUERY_DOCUMENTO .= "       DOC_DOC_FOLIO, SER_PREFIJO, DOC_SIGLA, STA_SIGLA, DOC_DOC_ID,\n";
    $e_QUERY_DOCUMENTO .= "       cargo.CXC_MOV_DOCUMENTO AS INGRESO, CXC_MOV_DET_ID, CXC_MOV_DET_TS AS FECHA,CXC_MOV_DET_ABONO\n";
    $e_QUERY_DOCUMENTO .= "  FROM dat_cxc_movimientos AS cargo\n";
    $e_QUERY_DOCUMENTO .= "  JOIN dat_cxc_movimientos_detalles ON cargo.CXC_MOV_ID = CXC_MOV_DET_CARGO\n";
    $e_QUERY_DOCUMENTO .= "  JOIN dat_cxc_movimientos AS abono ON abono.CXC_MOV_ID = CXC_MOV_DET_ABONO\n";
    $e_QUERY_DOCUMENTO .= "  JOIN doc_documentos ON DOC_DOC_ID = abono.CXC_MOV_DOCUMENTO\n";
    $e_QUERY_DOCUMENTO .= "  JOIN cat_series ON SER_ID = DOC_DOC_SERIE\n";
    $e_QUERY_DOCUMENTO .= "  JOIN sys_documentos ON DOC_ID = DOC_DOC_DOCUMENTO\n";
    $e_QUERY_DOCUMENTO .= "  JOIN sys_flujos ON FLU_DOCUMENTO = DOC_ID AND FLU_ACTIVO='1' AND FLU_ORDEN='1'\n";
    $e_QUERY_DOCUMENTO .= "  JOIN sys_status ON STA_FLUJO = FLU_ID AND STA_ACTIVO='1' AND STA_ORDEN='1'\n";
    $e_QUERY_DOCUMENTO .= " WHERE cargo.CXC_MOV_DOCUMENTO='@PARAMETRO2' ORDER BY FECHA ASC";
    file_put_contents("QUERY_DOCUMENTO",$e_QUERY_DOCUMENTO);
    $e_INDEX = 0;
    if ( $r_INGRESO = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_INGRESO ) )
    {
      while( $a_INGRESO = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_INGRESO ) )
      {
      	$e_RFC_BANCO_ORDENANTE    = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('QUERY_CFDI_BANCO', 'RFC', "AND BANCO = '{$a_INGRESO['RFC_BANCO_ORDENANTE']}'" );
      	$e_RFC_BANCO_BENEFICIARIO = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('QUERY_CFDI_BANCO', 'RFC', "AND DESCRIPCION = '{$a_INGRESO['BANCO_BENEFICIARIO']}'" );

        $m_INFO_PAGOS[$e_INDEX] = array
        (
          'MONEDA'                  => $a_INGRESO['MONEDA'],
          'IMPORTE'                 => $a_INGRESO['MONTO'],
          'RFC_BANCO_ORDENANTE'     => $e_RFC_BANCO_ORDENANTE,
          'NOMBRE_BANCO_ORDENANTE'  => $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('QUERY_CFDI_BANCO', 'DESCRIPCION', "AND BANCO = '{$a_INGRESO['RFC_BANCO_ORDENANTE']}'"),
          'RFC_BANCO_BENEFICIARIO'  => $e_RFC_BANCO_BENEFICIARIO,
          'CUENTA_ORDENANTE'        => $a_INGRESO['NUM_CUENTA_ORDENANTE'],
          'NUMERO_OPERACION'        => $a_INGRESO['NUMERO_OPERACION'],
          'CTA_BENEFICIARIO_CLABE'  => $a_INGRESO['CTA_BENEFICIARIO_CLABE'],
          'CTA_BENEFICIARIO'        => $a_INGRESO['CTA_BENEFICIARIO'],
          'FECHA_TS_ORIGEN'         => $a_INGRESO['TS_ORIGEN'],
          'FECHA_ORIGEN'            => $a_INGRESO['FECHA_ORIGEN'],
          'FOLIO_ORIGEN'            => $a_INGRESO['FOLIO_ORIGEN']
        );

        if( $r_DOCUMENTO = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_DOCUMENTO, $a_INGRESO['DOC_CON_DOCUMENTO_DESTINO'] ) )
        {
          while( $m_DOCUMENTO = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_DOCUMENTO ) )
          {
            $e_SIGLA_documento = strtolower( $m_DOCUMENTO['DOC_SIGLA'] );
            $m_INFO_DOCUMENTO  = $GLOBALS['SYSTEM']['DBS']['DB']->get_array( 'MY_QUERY', "SELECT * FROM doc_{$e_SIGLA_documento} WHERE {$m_DOCUMENTO['DOC_SIGLA']}_DOCUMENTO = '{$m_DOCUMENTO['DOC_DOC_ID']}'" );
            $e_UUID            = '';
            if( preg_match( '#UUID=["\']([A-Fa-f0-9\-]{36})["\']#', $m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_{$m_DOCUMENTO['STA_SIGLA']}_UUID"], $m_PATTERNS ) )
              $e_UUID = $m_PATTERNS[1];

            //Obtener el numero de pagos que se han aplicado a la venta
            $e_CXC_MOV     = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'CXC_MOV_ID', "SELECT CAST(GROUP_CONCAT(CXC_MOV_ID) AS CHAR) AS CXC_MOV_ID FROM dat_cxc_movimientos WHERE CXC_MOV_DOCUMENTO = '{$m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_DOCUMENTO"]}' AND CXC_MOV_TIPO = 'A'");
            $e_QUERY_CXC   = "SELECT abono.CXC_MOV_ID,cargo.CXC_MOV_MONTO AS MONTO_PARCIALIDAD, cargo.CXC_MOV_MONTO AS CARGO, abono.CXC_MOV_MONTO AS ABONO,\n";
            $e_QUERY_CXC  .= "       abono.CXC_MOV_MONTO_DISPONIBLE AS DISPONIBLE, CXC_MOV_DET_MONTO AS CARGO_ING, DOC_DOC_FOLIO,\n";
            $e_QUERY_CXC  .= "       CXC_MOV_DET_TS AS FECHA, cargo.CXC_MOV_DOCUMENTO AS INGRESO, CXC_MOV_DET_ID, abono.CXC_MOV_DOCUMENTO AS VENTA\n";
            $e_QUERY_CXC  .= "  FROM doc_documentos\n";
            $e_QUERY_CXC  .= "  JOIN dat_cxc_movimientos AS cargo ON cargo.CXC_MOV_DOCUMENTO = DOC_DOC_ID\n";
            $e_QUERY_CXC  .= "  JOIN dat_cxc_movimientos_detalles ON cargo.CXC_MOV_ID = CXC_MOV_DET_CARGO\n";
            $e_QUERY_CXC  .= "  JOIN dat_cxc_movimientos AS abono ON abono.CXC_MOV_ID = CXC_MOV_DET_ABONO\n";
            $e_QUERY_CXC  .= "  JOIN doc_ing ON ING_ID = cargo.CXC_MOV_ENCABEZADO\n";
            $e_QUERY_CXC  .= "  JOIN cat_series ON SER_ID = DOC_DOC_SERIE\n";
            $e_QUERY_CXC  .= " WHERE CXC_MOV_DET_ABONO IN ({$e_CXC_MOV}) ORDER BY CXC_MOV_ID, FECHA ASC";
            file_put_contents("QUERY_CXC",$e_QUERY_CXC."\n\n",FILE_APPEND);
            $e_PARCIALIDAD = 1;
            $e_ABONOS_CXC  = 0;

            if( $r_CXC = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_CXC ) )
            {
              $e_CARGO = 0;
              while( $a_CXC = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_CXC ) )
              {
                if ($e_ABONO_ID_ANTERIOR != $a_CXC['CXC_MOV_ID'])
                {
                    $e_CARGO = 0;
                }
                $e_ABONOS_CXC                                                      += $a_CXC['MONTO_PARCIALIDAD'];
                $a_CXC['PARCIALIDAD']                                               = $e_PARCIALIDAD;
                $e_CARGO                                                            = $e_CARGO + $a_CXC['CARGO_ING'];
                $a_CXC['SALDO_PENDIENTE']                                           = $a_CXC['ABONO'] - $e_CARGO;
                $m_PARCIALIDAD_AUX_VENTA[$a_CXC['VENTA']][$a_CXC['CXC_MOV_DET_ID']] = $a_CXC;
                $e_PARCIALIDAD++;
                $e_ABONO_ID_ANTERIOR = $a_CXC['CXC_MOV_ID'];
              }
            }

            if( $m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_{$m_DOCUMENTO['STA_SIGLA']}_MONEDA"] )
              $e_MONEDA = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'MON_ABREVIATURA', "SELECT MON_ABREVIATURA FROM cat_monedas WHERE MON_ID = '{$m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_{$m_DOCUMENTO['STA_SIGLA']}_MONEDA"]}'");
            else
              $e_MONEDA = 'MXN';

            //Obtener clave del impuesto del documento de venta
            $e_SIGLA_IMPUESTO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'IMP_SIGLA', "SELECT IMP_SIGLA FROM cat_impuestos JOIN doc_documentos_impuestos ON DOC_IMP_IMPUESTO = IMP_ID JOIN doc_{$e_SIGLA_documento} ON {$m_DOCUMENTO['DOC_SIGLA']}_DOCUMENTO = DOC_IMP_DOCUMENTO WHERE {$m_DOCUMENTO['DOC_SIGLA']}_DOCUMENTO = '{$m_INFO_DOCUMENTO['VEN_DOCUMENTO']}'" );

            //Numero de parcialidades
            $e_PARCIALIDAD_AUX = $m_PARCIALIDAD_AUX_VENTA[$m_DOCUMENTO['DOC_DOC_ID']][$m_DOCUMENTO['CXC_MOV_DET_ID']]['PARCIALIDAD'];
            if( $e_PARCIALIDAD_AUX == 1 )
              $e_ABONOS_CXC = $m_PARCIALIDAD_AUX_VENTA[$m_DOCUMENTO['DOC_DOC_ID']][$m_DOCUMENTO['CXC_MOV_DET_ID']]['ABONO'];
            else
              $e_ABONOS_CXC = $m_PARCIALIDAD_AUX_VENTA[$m_DOCUMENTO['DOC_DOC_ID']][$m_DOCUMENTO['CXC_MOV_DET_ID']]['SALDO_PENDIENTE'] + $m_DOCUMENTO['CARGO_ING'];


            //Restamos los documentos de abono diferentes a ingresos
            $e_QUERY_DIF_MONTO = "SELECT abono.CXC_MOV_ID, 
                                        abono.CXC_MOV_TIPO,
                                        abono.CXC_MOV_CLIENTE,
                                        SUM(CXC_MOV_DET_MONTO) AS MONTO_NO_INGRESO,
                                        CXC_MOV_DET_CARGO,
                                        cargo.CXC_MOV_DOCUMENTO,
                                        DOC_DOC_ID
                                FROM
                                dat_cxc_movimientos AS abono
                                INNER JOIN dat_cxc_movimientos_detalles ON abono.CXC_MOV_ID = CXC_MOV_DET_ABONO
                                INNER JOIN dat_cxc_movimientos AS cargo ON CXC_MOV_DET_CARGO = cargo.CXC_MOV_ID
                                INNER JOIN doc_documentos ON cargo.CXC_MOV_DOCUMENTO = DOC_DOC_ID
                                WHERE abono.CXC_MOV_DOCUMENTO = '{$m_DOCUMENTO['DOC_DOC_ID']}'
                                AND DOC_DOC_DOCUMENTO != '16'";
            $e_MONTO_DIFERENTE_INGRESOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY','MONTO_NO_INGRESO',$e_QUERY_DIF_MONTO);
            $e_ABONOS_CXC -= $e_MONTO_DIFERENTE_INGRESOS;
            $e_IMPSALDOINSOLUTO  = ($e_ABONOS_CXC - $m_DOCUMENTO['CARGO_ING']);
            //$e_IMPSALDOINSOLUTO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY','CXC_MOV_MONTO_DISPONIBLE',"SELECT CXC_MOV_MONTO_DISPONIBLE FROM dat_cxc_movimientos WHERE CXC_MOV_DOCUMENTO = '{$m_DOCUMENTO['DOC_DOC_ID']}'");

            $e_DECIMALES_FACTURA = $this->obtener_decimales_validos_documento($e_SIGLA_STA_CDP);

            $m_DOC_REL[] = array
            (
              'IDDCOUMENTO'      => $e_UUID,
              'SERIE'            => $m_DOCUMENTO['SER_PREFIJO'],
              'FOLIO'            => $m_DOCUMENTO['DOC_DOC_FOLIO'],
              'MONEDA_DR'        => $e_MONEDA,
              'TIPO_CAMBIO'      => ($m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_{$m_DOCUMENTO['STA_SIGLA']}_TIPO_CAMBIO"] > 0.0001) ? $m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_{$m_DOCUMENTO['STA_SIGLA']}_TIPO_CAMBIO"] : "1",
              'METODO_PAGO_DR'   => $m_INFO_DOCUMENTO["{$m_DOCUMENTO['DOC_SIGLA']}_{$m_DOCUMENTO['STA_SIGLA']}_METODO_PAGO_V33"],
              'IMPSALDOANT'      => $this->formatear_decimales_hacienda($e_ABONOS_CXC,$e_DECIMALES_FACTURA),
              'IMPSALDOINSOLUTO' => $this->formatear_decimales_hacienda($e_IMPSALDOINSOLUTO, $e_DECIMALES_FACTURA),
              'IMPORTE_PAGADO'   => $m_DOCUMENTO['CARGO_ING'],
              'CLAVE_IMPUESTO'   => $this->CONF_IMPUESTOS["{$e_SIGLA_IMPUESTO}"]['CODIGO_SAT'],
              'PARCIALIDAD'      => $e_PARCIALIDAD_AUX
            );
          }// while $m_DOCUMENTO
          $m_INFO_PAGOS[$e_INDEX]['DOCTOS_REL'] = $m_DOC_REL;
        }//if Resource
        $e_INDEX++;
      }//while $a_INGRESO
    }//fin If Ingreso
    return $m_INFO_PAGOS;
  }// fin Funcion

  private function generar_elemento_documentos_relacionados( $e_SIGLA_STATUS )
  {
    $m_DOCUMENTOS_RELACIONADOS = array();
    $e_CAMPO_RELACION          = $this->CONF_DOCUMENTO[$this->TIPO_DOCUMENTO][RELACIONCFDI];

    $e_QUERY_DOCUMENTO  = "SELECT DOC_SIGLA, STA_SIGLA, DOC_REL_DOCUMENTO_PADRE, DOC_REL_ENCABEZADO_PADRE, DOC_REL_RELACION\r\n";
    $e_QUERY_DOCUMENTO .= "  FROM doc_documentos_relacionados\r\n";
    $e_QUERY_DOCUMENTO .= "  JOIN doc_documentos ON DOC_DOC_ID = DOC_REL_DOCUMENTO_PADRE\r\n";
    $e_QUERY_DOCUMENTO .= "  JOIN sys_documentos ON DOC_ID = DOC_DOC_DOCUMENTO\r\n";
    $e_QUERY_DOCUMENTO .= "  JOIN sys_flujos ON FLU_DOCUMENTO = DOC_ID AND FLU_ACTIVO='1' AND FLU_ORDEN='1'\r\n";
    $e_QUERY_DOCUMENTO .= "  JOIN sys_status ON STA_FLUJO = FLU_ID AND STA_ACTIVO='1' AND STA_ORDEN='1'\r\n";
    $e_QUERY_DOCUMENTO .= " WHERE DOC_REL_DOCUMENTO = '{$this->DOCUMENTO_PADRE->ID}'\r\n";
    $e_QUERY_DOCUMENTO .= "   AND DOC_REL_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'\r\n";
    $e_QUERY_DOCUMENTO .= "   AND DOC_REL_DETALLE = 0 AND DOC_REL_DETALLE_PADRE = 0 AND DOC_REL_MONTO = 0\r\n";
    $e_QUERY_DOCUMENTO .= " GROUP BY DOC_REL_DOCUMENTO_PADRE";

    if( $r_DOCUMENTOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource('MY_QUERY', $e_QUERY_DOCUMENTO ) )
    {
      while( $a_DOCUMENTOS = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_DOCUMENTOS ) )
      {
        //Nombre del documento
        $e_DOCUMENTO = 'doc_' . strtolower( $a_DOCUMENTOS['DOC_SIGLA'] );

        //Obtenemos el xml del uuid
        $e_UUID = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', "UUID", "SELECT {$a_DOCUMENTOS['DOC_SIGLA']}_{$a_DOCUMENTOS['STA_SIGLA']}_UUID AS UUID FROM {$e_DOCUMENTO} WHERE {$a_DOCUMENTOS['DOC_SIGLA']}_DOCUMENTO = '{$a_DOCUMENTOS['DOC_REL_DOCUMENTO_PADRE']}'" );

        //Obtenemos solo el uuid
        if( preg_match( '#UUID=["\']([A-Fa-f0-9\-]{36})["\']#', $e_UUID, $m_PATTERNS ) )
          $e_UUID = $m_PATTERNS[1];

        //obtener el dato directamente del objeto cfdi->DOCUMENTO_PADRE->STATUS......
        $e_RELACION_CFDI = $this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->DATA[$e_CAMPO_RELACION];
        $m_DOCUMENTOS_RELACIONADOS['TIPO_RELACION'] = $e_RELACION_CFDI;

        //Arreglo con los uuid relacionados
        $m_DOCUMENTOS_RELACIONADOS['UUID_RELACIONADOS'][] = $e_UUID;
      }
    }
    return $m_DOCUMENTOS_RELACIONADOS;

  }

  private function generar_nodo_complemento_ine( $e_SIGLA_STATUS )
  {
    $e_TABLA                    = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_STA_PREFIJO              = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO'];
    $e_CAMPO_USAR_NODO_INE_CFDI = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA}' AND COLUMN_NAME = '{$e_STA_PREFIJO}_USAR_NODO_INE_CFDI'" );

    if ( $e_CAMPO_USAR_NODO_INE_CFDI && $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_STA_PREFIJO}_USAR_NODO_INE_CFDI"] )
    {
      $e_TIPO_PROCESO   = str_replace ('ampana', 'ampa�a', $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_STA_PREFIJO}_NODO_INE_TIPOPROCESO"] );
      $e_TIPO_COMITE    = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_STA_PREFIJO}_NODO_INE_TIPOCOMITE"];
      $e_AMBITO         = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_STA_PREFIJO}_NODO_INE_AMBITO"];
      $e_CLAVE_ENTIDAD  = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_STA_PREFIJO}_NODO_INE_CVEENTIDAD"];
      $e_IDCONTABILIDAD = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_STA_PREFIJO}_NODO_INE_IDCONTABILIDAD"];

      $e_NODO_INE  = "\r\n\t\t<ine:INE xmlns:ine='http://www.sat.gob.mx/ine' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.sat.gob.mx/ine http://www.sat.gob.mx/sitio_internet/cfd/ine/ine11.xsd' Version='1.1' TipoProceso='{$e_TIPO_PROCESO}'>";
      $e_NODO_INE .= "\r\n\t\t\t<ine:Entidad ClaveEntidad='{$e_CLAVE_ENTIDAD}' Ambito='{$e_AMBITO}'></ine:Entidad>";
      $e_NODO_INE .= "\r\n\t\t</ine:INE>";

      return $e_NODO_INE;
    }
    else
      return false;
  }

  private function validar_datos_receptor($e_SIGLA_STATUS)
  {
    $e_VALIDACION              = true;
    $e_TABLA                   = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['TABLA'];
    $e_STA_PREFIJO             = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO'];
    $e_CAMPO_SOLICITAR_FACTURA = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'COLUMN_NAME', "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}' AND TABLE_NAME = '{$e_TABLA}' AND COLUMN_NAME = '{$e_STA_PREFIJO}_SOLICITAR_FACTURA'" );

    if ( !$e_CAMPO_SOLICITAR_FACTURA )
      $e_VALIDACION = $this->validar_receptor_fiscal($e_SIGLA_STATUS);
    else
      if ( $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->CONF['PREFIJO']}_SOLICITAR_FACTURA"] )
        $e_VALIDACION = $this->validar_receptor_fiscal($e_SIGLA_STATUS);
    //Validar que el documento tengo el dato USOCFDI
    if( !$this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['USOCFDI']}"] && $this->TIPO_DOCUMENTO != 'NOMINA')
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del: <b>Uso del CFDi</b> es obligatorio, no puede guardar el documento sin este valor.";
      $e_VALIDACION = false;
    }

    return $e_VALIDACION;
  }

  private function validar_receptor_fiscal( $e_SIGLA_STATUS )
  {
    $e_VALIDACION = true;
    $e_ID_CLIENTE = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['CLIENTE']}"];
    $this->obtener_datos_receptor( $e_ID_CLIENTE );

    if( !$this->valida_rfc( $this->RECEPTOR['RFC'] ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El RFC del cliente/receptor: <b>{$this->RECEPTOR['RFC']}</b> no es v&aacute;lido, favor de corregir antes de continuar.";
      $e_VALIDACION = false;
    }

    if( !$this->RECEPTOR['SIGLA_PAIS'] )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El sigla/abreviatura para el Pa&iacute;s del cliente/receptor: <b>{$this->RECEPTOR['NOMBRE_PAIS']}: {$this->RECEPTOR['SIGLA_PAIS']}</b> no es v&aacute;lida, favor de corregir antes de continuar.";
      $e_VALIDACION = false;
    }
    return $e_VALIDACION;
  }

  private function calcular_forma_pago( $e_SIGLA_STATUS )
  {
    $e_CAMPO_FORMAPAGO   = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['FORMAPAGO'];
    $e_VALOR_FORMAPAGO   = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_CAMPO_FORMAPAGO}"];

    if ( is_numeric ( $e_VALOR_FORMAPAGO ) )
      $e_VALOR_FORMAPAGO = 'a:1:{i:' . "{$e_VALOR_FORMAPAGO}" . ';s:22:"NO_IMPORTA_ESTE_NOMBRE";}';

    $e_CAMPO_METODOPAGO  = $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['METODOPAGO'];
    $e_VALOR_METODOPAGO  = $this->DOCUMENTO_PADRE->STATUS["{$e_SIGLA_STATUS}"]->DATA["{$e_CAMPO_METODOPAGO}"];


    if( $this->TIPO_DOCUMENTO == 'FACTURA' )
    {
      if( $this->CONF_DOCUMENTO["{$this->TIPO_DOCUMENTO}"]['OBTENER_FDP_TES'] == 'SI' )
      {
        $e_QUERY_FORMAS_PAGO = "SELECT {$e_CAMPO_FORMAPAGO} AS FORMA_PAGO, SUM(D_VEN_TES_MONTO) AS MONTO FROM doc_ven_tes WHERE D_VEN_TES_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}' GROUP BY {$e_CAMPO_FORMAPAGO}";
        if( $r_FORMAS_PAGO = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource( 'MY_QUERY', $e_QUERY_FORMAS_PAGO ) )
        {
          $e_IMPORTE_FDP_SELECCIONADA = 0.00;
          $e_FDP_SELECCIONADA         = 0;
          while( $a_FORMA_PAGO = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array( $r_FORMAS_PAGO ) )
          {
            $e_IMPORTE_FDP_SELECCIONADA = ($a_FORMA_PAGO['MONTO']  > $e_IMPORTE_FDP_SELECCIONADA ? $a_FORMA_PAGO['MONTO'] : $e_IMPORTE_FDP_SELECCIONADA);
            $e_FDP_SELECCIONADA         = ($a_FORMA_PAGO['MONTO'] == $e_IMPORTE_FDP_SELECCIONADA ? $a_FORMA_PAGO['FORMA_PAGO'] : $e_FDP_SELECCIONADA);
          }

          $m_VALORES_RETORNO = array
          (
            'FORMA_PAGO'  => $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'CAM_PER_CODIGO', "SELECT CAM_PER_CODIGO FROM cat_formaspago WHERE FDP_ID = '{$e_FDP_SELECCIONADA}'"),
            'METODO_PAGO' => $e_VALOR_METODOPAGO
          );
        }
        else
          $m_VALORES_RETORNO = array('FORMA_PAGO' => '99', 'METODO_PAGO' => 'PPD');
      }
      else
          $m_VALORES_RETORNO = array();
    }
    else
      $m_VALORES_RETORNO = array();


    if($this->TIPO_DOCUMENTO == 'NOMINA')
    {
      $m_VALORES_RETORNO = array('FORMA_PAGO' => '99', 'METODO_PAGO' => 'PUE');
    }

    $m_FORMAS_PAGO = unserialize( $e_VALOR_FORMAPAGO );

    if( !count( $m_VALORES_RETORNO ) )
    {
      if ( count( $m_FORMAS_PAGO ) )
      {
        $m_VALORES_RETORNO = array
        (
          'FORMA_PAGO'  => $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'CAM_PER_CODIGO', "SELECT CAM_PER_CODIGO FROM cat_formaspago WHERE FDP_ID = '" . key( $m_FORMAS_PAGO ) . "'" ),
          'METODO_PAGO' => $e_VALOR_METODOPAGO,
        );
      }
      else
        $m_VALORES_RETORNO = array('FORMA_PAGO' => '99', 'METODO_PAGO' => 'PPD');
    }

    return $m_VALORES_RETORNO;
  }

  private function valida_rfc( $e_RFC )
  {
    $e_RFC = $this->remplazar_caracteres_xml( $e_RFC, true );
    $e_PATTERN = '/^([A-Z\x26]{3,4})\s{0,1}([\d]{2})(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])\s{0,1}([A-Z\d]{2}[A\d])$/';
    return ( preg_match( $e_PATTERN, $e_RFC ) === 1 );
  }

  private function validar_fecha_formato_sat( $e_FECHA )
  {
    $o_DATE = DateTime::createFromFormat( 'Y-m-d\TH:i:s', $e_FECHA, new DateTimeZone('America/Mexico_City' ) );
    return $o_DATE && $o_DATE->format( 'Y-m-d\TH:i:s' ) == $e_FECHA;
  }

  private function valida_codigo_postal( $e_CODIGPOSTAL )
  {
    return ( preg_match('/^[0-9]{5}$/', $e_CODIGPOSTAL ) === 1 );
  }

  private function validar_certificado()
  {
    $e_VALIDACION   = true;
    //validar longitud del numero de certificado registrado
    if( !preg_match( "/^[0-9]{20}$/", $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['NUMERO_CERTIFICADO'] ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El <b>n&uacute;mero de certificado</b> no es v&aacute;lido, deben ser 20 d&iacute;gitos.";
      $e_VALIDACION = false;
    }

    if( $e_VALIDACION )
    {
      $e_SALIDA_SHELL = shell_exec( 'openssl x509 -in ' . "{$this->PATH_CERTIFICADOS}/{$this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['CERTIFICADO_ELECTRONICO']}" . ' -noout -serial 2>&1' );
      if ( strpos( $e_SALIDA_SHELL, 'serial=' ) !== false )
      {
        $e_SALIDA_SHELL = str_replace( 'serial=', '', $e_SALIDA_SHELL );
        $e_NO_CERTIFICADO = '';
        for ( $i = 0; $i < strlen( $e_SALIDA_SHELL ); $i++ )
          if ($i % 2 != 0)
            $e_NO_CERTIFICADO .= $e_SALIDA_SHELL[$i];
      }

      if ( $e_NO_CERTIFICADO !== $this->EMISOR["{$this->ID_SUCURSAL_ACTIVA}"]['NUMERO_CERTIFICADO'] )
      {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = 'El n&uacute;mero de certificado capturado no corresponde al n&uacute;mero de certificado del archivo.';
          $e_VALIDACION = false;
      }

      $e_TIME_STAMP = time();
      if( $this->CSD['validFrom_time_t'] > $e_TIME_STAMP || $e_TIME_STAMP > $this->CSD['validTo_time_t'] )
      {
        $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = 'La vigencia del certificado ya no es v&aacute;lida, el per&iacute;odo de vigencia es: <b>' . date('d/m/Y H:i:s', $this->CSD['validFrom_time_t']) . '</b> al <b>' . date('d/m/Y H:i:s', $this->CSD['validTo_time_t']) . '</b> y la fecha de hoy es:<b>' . date('d/m/Y H:i:s') . '</b>' ;
        $e_VALIDACION = false;
      }
    }

    return $e_VALIDACION;
  }

  private function obtener_datos_una_serie($e_SER_ID)
  {
    $m_DATOS_UNA_SERIE = $GLOBALS['SYSTEM']['DBS']['DB']->get_array( 'QUERY_UNASERIE', $e_SER_ID );

    return array
    (
      'NOMBRE'         => $m_DATOS_UNA_SERIE['SER_NOMBRE'],
      'PREFIJO'        => $m_DATOS_UNA_SERIE['SER_PREFIJO'],
      'FOLIO_LONGITUD' => $m_DATOS_UNA_SERIE['SER_FOLIO_LONGITUD'],
    );
  }

  private function obtener_data_all_nodos_xml( $DATA )
  {
    $m_DATA = array();

    if( !$DATA )
      return $m_DATA;

    $XML_DATA = '<?xml version="1.0" encoding="ISO-8859-1"?>' . $DATA;
    $DATA_XML = new DOMDocument();
    $DATA_XML->loadXML($XML_DATA);
    $o_XML = $DATA_XML->documentElement;

    if( $o_XML->hasChildNodes() )
    {
      $ITEMS = $o_XML->childNodes;

      for($i=0;$i<$ITEMS->length;$i++)
      {
        $ITEM = $ITEMS->item($i);

        if($ITEM->nodeType == 1)
          $m_DATA[$ITEM->nodeName] = $ITEM->nodeValue;
      }
    }

    return $m_DATA;
  }

  public function obtener_attributes_nodoxml( $e_MINI_XML )
  {
    $m_CONFIGURACION = array();

    if( !$e_MINI_XML )
      return $e_MINI_XML;

    $XML_STRING = '<?xml version="1.0" encoding="ISO-8859-1"?>' . $e_MINI_XML;
    $CONFIG_XML = new DOMDocument();
    $CONFIG_XML->loadXML($XML_STRING);
    $o_CONFIGURACION = $CONFIG_XML->documentElement;
    $CONFIGURACION = $o_CONFIGURACION->getElementsByTagName('VALUES');

    if( $CONFIGURACION->item( 0 ) )
    {
      $CONFIGURACION   = $CONFIGURACION->item( 0 );
      $m_CONFIGURACION = Tool::get_attributes( $CONFIGURACION );
    }

    return $m_CONFIGURACION;
  }

  private function obtener_configuracion_impuestos()
  {
    if( count( $this->CONF_IMPUESTOS ) )
        return;

    $r_IMPUESTOS_SAT = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_resource( 'QUERY_CFDI_IMPUESTO', "AND ACTIVO = '1' ORDER BY ID" );
    if( $r_IMPUESTOS_SAT )
    {
      while ( $a_UNIMPUESTO_SAT = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->fetch_array( $r_IMPUESTOS_SAT ) )
      {
        $this->CONF_IMPUESTOS["{$a_UNIMPUESTO_SAT['DESCRIPCION']}"] = array
        (
          'CODIGO_SAT'       => $a_UNIMPUESTO_SAT['IMPUESTO'],
          'APLICA_RETENCION' => ($a_UNIMPUESTO_SAT['RETENCION'] == 'S'),
          'APLICA_TRASLADO'  => ($a_UNIMPUESTO_SAT['TRASLADO'] == 'S'),
          'FACTORES'         => array(),
        );

        $r_FACTORES_UNIMPUESTO = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_resource('MY_QUERY', "SELECT * FROM cat_cfdi_tasaocuota WHERE IMPUESTO = '{$a_UNIMPUESTO_SAT['DESCRIPCION']}' AND ACTIVO = '1'");
        if( $r_FACTORES_UNIMPUESTO )
        {
          while ( $a_UNFACTOR_UNIMPUESTO = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->fetch_array( $r_FACTORES_UNIMPUESTO ) )
          {
            $this->CONF_IMPUESTOS["{$a_UNIMPUESTO_SAT['DESCRIPCION']}"]['FACTORES'][] = array
            (
              'FACTOR'           => $a_UNFACTOR_UNIMPUESTO['FACTOR'],
              'TIPO'             => $a_UNFACTOR_UNIMPUESTO['RANGO_FIJO'],
              'VALOR_MINIMO'     => $a_UNFACTOR_UNIMPUESTO['TASA_CUOTA_VALOR_MINIMO'],
              'VALOR_MAXIMO'     => $a_UNFACTOR_UNIMPUESTO['TASA_CUOTA_VALOR_MAXIMOO'],
              'APLICA_RETENCION' => ($a_UNFACTOR_UNIMPUESTO['RETENCION'] == 'S'),
              'APLICA_TRASLADO'  => ($a_UNFACTOR_UNIMPUESTO['TRASLADO'] == 'S'),
            );
          }
        }
      }
    }
  }

  private function validar_atributos( $e_TIPO_NODO, $m_ELEMENTO )
  {
    $e_VALIDACION = true;

    if( $e_TIPO_NODO === 'CONCEPTO' || $e_TIPO_NODO === 'IMPUESTOS_UNCONCEPTO' )
      $e_DESCRIPCION = $m_ELEMENTO['DESCRIPCION'];

    foreach( $m_ELEMENTO as $e_ATRIBUTO => $e_VALOR )
    {
      if( $e_TIPO_NODO === 'CONCEPTO' || $e_TIPO_NODO === 'IMPUESTOS_UNCONCEPTO' )
        $e_DESC_ATRIBUTO = "<b>{$e_ATRIBUTO}</b>, del producto <b>{$e_DESCRIPCION}</b>,";
      else
        $e_DESC_ATRIBUTO = "<b>{$e_ATRIBUTO}</b>";

      $e_VALOR = trim( $e_VALOR );
      if ( $e_TIPO_NODO == "IMPUESTOS_UNCONCEPTO" && $m_ELEMENTO['TIPO'] == 'T' )
      {
        if ( $e_ATRIBUTO == 'TASA' || $e_ATRIBUTO == 'IMPORTE' )
        {
          if ( !is_numeric( $e_VALOR ) )
          {
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del atributo {$e_DESC_ATRIBUTO} es obligatorio, no se puede generar el XML (1)";
            $e_VALIDACION = false;
          }
        }
      }
      elseif( $e_ATRIBUTO == 'METODOPAGO' || $e_ATRIBUTO == 'FORMAPAGO' )
      {
        if ( $this->TIPO_DOCUMENTO == 'CDP' )
          $e_VALIDACION = true;
        elseif( !$e_VALOR )
        {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del atributo {$e_DESC_ATRIBUTO} es obligatorio, no se puede generar el XML (2)";
          $e_VALIDACION = false;
        }
      }
      elseif( !$e_VALOR )
      {
        $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del atributo {$e_DESC_ATRIBUTO} es obligatorio, no se puede generar el XML (3)";
        $e_VALIDACION = false;
      }
    }

    if( $e_VALIDACION )
      $e_VALIDACION = $this->verificar_valores_atributos($e_TIPO_NODO, $m_ELEMENTO);

    return $e_VALIDACION;
  }

  private function verificar_valores_atributos( $e_TIPO_NODO, $m_ELEMENTO )
  {
    $e_MENSAJES_ERRORES = count( $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'] );
    switch( $e_TIPO_NODO )
    {
      case 'COMPROBANTE':
        //Validad Fecha formato SAT
        if ( !$this->validar_fecha_formato_sat( $m_ELEMENTO['FECHA'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La fecha <b>{$m_ELEMENTO['FECHA']}</b> tiene un formato de fecha inv&aacute;lido.";

        //Vaildar METODO de PAGO
        if ( $this->TIPO_DOCUMENTO != 'CDP' )
          if ( !$this->validar_valor_uncatalogo_sat( 'metodopago', 'METODOPAGO', $m_ELEMENTO['METODOPAGO'] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el M&eacute;todo de Pago <b>{$m_ELEMENTO['METODOPAGO']}</b> en los cat&aacute;logos del SAT";

        //Vaildar FORMA de PAGO
        if ( $this->TIPO_DOCUMENTO != 'CDP' )
          if ( !$this->validar_valor_uncatalogo_sat( 'formapago', 'FORMAPAGO', $m_ELEMENTO['FORMAPAGO'] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la Forma de Pago <b>{$m_ELEMENTO['FORMAPAGO']}</b> en los cat&aacute;logos del SAT";

        //Vaildar TIPO DE COMPROBANTE
        if ( !$this->validar_valor_uncatalogo_sat( 'tipocomprobante', 'TIPOCOMPROBANTE', $m_ELEMENTO['TIPODECOMPROBANTE'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el Tipo de Comprobante <b>{$m_ELEMENTO['TIPODECOMPROBANTE']}</b> en los cat&aacute;logos del SAT";

        //Vaildar CODIGO POSTAL del Lugar de Expedicion
        if ( !$this->validar_valor_uncatalogo_sat( 'codigopostal', 'CODIGO_POSTAL', $m_ELEMENTO['LUGAREXPEDICION'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el C&oacute;digo Postal <b>{$m_ELEMENTO['LUGAREXPEDICION']}</b> (Lugar de Expedici&oacute;n) en los cat&aacute;logos del SAT";

        //Vaildar MONEDA
        if ( !$this->validar_valor_uncatalogo_sat( 'moneda', 'MONEDA', $m_ELEMENTO['MONEDA'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la Moneda <b>{$m_ELEMENTO['MONEDA']}</b> en los cat&aacute;logos del SAT";

        //Vaildar TIPO DE CAMBIO
        if ( $m_ELEMENTO['MONEDA'] == 'MXN' )
        {
          if ( floatval( $m_ELEMENTO['TIPO_CAMBIO'] ) != 1.0 )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del Tipo de Cambio para la Moneda <b>{$m_ELEMENTO['MONEDA']}</b> no puede ser diferente de \$1.00";
        }
        elseif ( $m_ELEMENTO['MONEDA'] != 'XXX' )
        {
          if ( !(floatval( $m_ELEMENTO['TIPO_CAMBIO'] ) > 0.0 ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del Tipo de Cambio para la Moneda <b>{$m_ELEMENTO['MONEDA']}</b> debe ser mayor \$0.00";
        }

        break;

      case 'EMISOR':
        //Vaildar RFC
        if ( !$this->valida_rfc( $m_ELEMENTO['RFC'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El RFC del Emisor <b>{$m_ELEMENTO['RFC']}</b> no es v&aacute;lido";

        //Valida REGIMENFISCAL
        if ( !$this->validar_valor_uncatalogo_sat( 'regimenfiscal', 'REGIMENFISCAL', $m_ELEMENTO['REGIMENFISCAL'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el R&eacute;gimen Fiscal <b>{$m_ELEMENTO['REGIMENFISCAL']}</b> en los cat&aacute;logos del SAT";

        break;

      case 'RECEPTOR':
        //Validar RFC
        if ( !$this->valida_rfc( $m_ELEMENTO['RFC'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El RFC del Receptor <b>{$m_ELEMENTO['RFC']}</b> no es v&aacute;lido";

        //Vaildar USO DEL CFDI
        if ( !$this->validar_valor_uncatalogo_sat( 'usocfdi', 'USOCFDI', $m_ELEMENTO['USOCFDI'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el Uso del CFDi <b>{$m_ELEMENTO['USOCFDI']}</b> en los cat&aacute;logos del SAT";

        //Vaildar DATOS de Receptores Extranjeros
        if ( $m_ELEMENTO['RFC'] == 'XEXX010101000' )
        {
          //Vaildar Residencia fiscal
          if( !$this->RECEPTOR['SIGLA_PAIS'] )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La Sigla del Pa&iacute;s de <b>Residencia Fiscal</b> es un dato obligatorio para receptores extranjeros";
          elseif ( $this->RECEPTOR['SIGLA_PAIS'] == 'MEX')
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La Sigla del Pa&iacute;s de <b>Residencia Fiscal</b> no puede ser <b>MEX</b> para receptores extranjeros";
          elseif ( !$this->validar_valor_uncatalogo_sat( 'pais', 'PAIS', $this->RECEPTOR['SIGLA_PAIS'] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la Sigla del Pa&iacute;s <b>{$m_ELEMENTO['RESIDENCIAFISCAL']}</b> (Residencia Fiscal del receptor) en los cat&aacute;logos del SAT";

          //Vaildar Residencia NUMREGIDTRIB
          if( !$this->RECEPTOR['NUMREGIDTRIB'] )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El <b>N&uacute;mero de Registro de Identidad Tibutaria (NUMREGIDTRIB)</b> es un dato obligatorio para receptores extranjeros";
        }

        break;

      case 'CONCEPTO':
        //Vaildar Clave del Producto o Servicio
        if ( !$this->validar_valor_uncatalogo_sat( 'claveprodserv', 'CLAVEPRODSERV', $m_ELEMENTO['CLAVEPRODSERV'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => No se encontr&oacute; la Clave de Producto/Servicio <b>{$m_ELEMENTO['CLAVEPRODSERV']}</b> en los cat&aacute;logos del SAT,";

        //Vaildar Clave Unidad SAT
        if ( !$this->validar_valor_uncatalogo_sat( 'claveunidad', 'CLAVEUNIDAD', $m_ELEMENTO['CLAVEUNIDAD'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => No se encontr&oacute; la Clave de Unidad <b>{$m_ELEMENTO['CLAVEUNIDAD']}</b> en los cat&aacute;logos del SAT,";

        //Vaildar Cantidad mayor a 0.00
        if( !(floatval($m_ELEMENTO['CANTIDAD'] ) > 0.0 ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => El valor para el atributo <b>CANTIDAD</b> debe ser mayor 0.00";

        //Vaildar VALOR UNITARIO mayor a 0.00
        if( !(floatval($m_ELEMENTO['VALORUNITARIO'] ) > 0.0 ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => El valor para el atributo <b>Valor Unitario</b> debe ser mayor \$0.00";

        //Vaildar IMPORTE PARTIDA mayor a 0.00
        if( !(floatval($m_ELEMENTO['IMPORTE'] ) > 0.0 ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => El valor para el atributo <b>Valor Unitario</b> debe ser mayor \$0.00";

        break;

      case 'IMPUESTOS_UNCONCEPTO':
        //Vaildar TIPO DE  IMPUESTOS
        if ( $m_ELEMENTO['TIPO'] != 'T' && $m_ELEMENTO['TIPO'] != 'R' )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => El tipo de impuestos solo puede ser (<b>T</b>)rasladado o (<b>R</b>)etenido";

        //Vaildar Clave del Impuesto
        if ( !$this->validar_valor_uncatalogo_sat( 'impuesto', 'IMPUESTO', $m_ELEMENTO['CLAVE_IMP'] ) )
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Producto: <b>{$m_ELEMENTO['DESCRIPCION']}</b> => No se encontr&oacute; la Clave de Impuesto<b>{$m_ELEMENTO['CLAVE_IMP']}</b> en los cat&aacute;logos del SAT,";

        break;

      case 'NOMINA':
        //Validar tipo de percepcion
        if( $m_ELEMENTO['TIPO_PERCEPCION'] )
        {
          if ( !$this->validar_valor_uncatalogo_sat( 'tipopercepcion', 'TIPOPERCEPCION', $m_ELEMENTO['TIPO_PERCEPCION'] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el tipo de percepci&oacute;n <b>{$m_ELEMENTO['TIPO_PERCEPCION']}</b> en los cat&aacute;logos del SAT";
        }

        //validar Clave de percepcion
        if( $m_ELEMENTO['CLAVE_PERCEPCION'] )
        {
          if ( strlen($m_ELEMENTO['CLAVE_PERCEPCION']) < 3 || strlen($m_ELEMENTO['CLAVE_PERCEPCION']) > 15 )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La clave de percepci&oacute;n <b>{$m_ELEMENTO['CLAVE_PERCEPCION']}</b> debe conformarse de 3 a 15 caracteres";
        }

        //Validar tipo de deduccion
        if( $m_ELEMENTO['TIPO_DEDUCCION'] )
        {
          if ( !$this->validar_valor_uncatalogo_sat( 'tipodeduccion', 'TIPODEDUCCION', $m_ELEMENTO['TIPO_DEDUCCION'] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el tipo de deducci&oacute;n <b>{$m_ELEMENTO['TIPO_DEDUCCION']}</b> en los cat&aacute;logos del SAT";
        }

        //validar Clave de deduccion
        if( $m_ELEMENTO['CLAVE_DEDUCCION'] )
        {
          if (strlen( $m_ELEMENTO['CLAVE_DEDUCCION'] ) < 3 || strlen( $m_ELEMENTO['CLAVE_DEDUCCION'] ) > 15 )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La clave de deducci&oacute;n <b>{$m_ELEMENTO['CLAVE_DEDUCCION']}</b> debe conformarse de 3 a 15 caracteres";
        }

        //Validar tipo de incapacidad
        if( $m_ELEMENTO['TIPO_INCAPACIDAD'] )
        {
          if ( !$this->validar_valor_uncatalogo_sat( 'tipoincapacidad', 'TIPOINCAPACIDAD', $m_ELEMENTO['TIPO_INCAPACIDAD'] ) )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el tipo de incapacidad <b>{$m_ELEMENTO['TIPO_INCAPACIDAD']}</b> en los cat&aacute;logos del SAT";
        }

        //Validar tipo de horas extras
        if( $m_ELEMENTO['TIPO_HORAS'] )
        {
          if ( !$this->validar_valor_uncatalogo_sat( 'tipoincapacidad', 'TIPOINCAPACIDAD', $m_ELEMENTO['TIPO_HORAS'] ) )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el tipo de horas <b>{$m_ELEMENTO['TIPO_HORAS']}</b> en los cat&aacute;logos del SAT";
        }

        //Validar tipo de otro pagos
        if( $m_ELEMENTO['CLAVE_P'] )
        {
          if ( !$this->validar_valor_uncatalogo_sat( 'tipootropago', 'TIPOOTROPAGO', $m_ELEMENTO['CLAVE_P'] ) )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el tipo de otro pago <b>{$m_ELEMENTO['CLAVE_P']}</b> en los cat&aacute;logos del SAT";
        }

        if( $m_ELEMENTO['ANNIO'] && $m_ELEMENTO['CLAVE_P'] == '004' ){
          if( $m_ELEMENTO['ANNIO'] == date("Y") && date("n") != 12 )
          {
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del a&ntilde;o debe ser igual al a&ntilde;o inmediato anterior o igual al a&ntilde;o en curso siempre que el per�odo de pago sea diciembre";
          }
          elseif( $m_ELEMENTO['ANNIO'] != (date("Y")-1) )
          {
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor del a&ntilde;o debe ser igual al a&ntilde;o inmediato anterior o igual al a&ntilde;o en curso siempre que el per�odo de pago sea diciembre";
          }
        }


        //Validar SaldoAFavor >= RemanenteSalFav
        if( $m_ELEMENTO['REMANENTE_SALDO_A_FAVOR'] && $m_ELEMENTO['SALDO_A_FAVOR'] && $m_ELEMENTO['CLAVE_P'] == '004' )
        {
          if ( $m_ELEMENTO['SALDO_A_FAVOR'] < $m_ELEMENTO['REMANENTE_SALDO_A_FAVOR'] )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor SaldoAFavor debe ser mayor o igual que el valor de RemantenteSalFav";
        }

        //Validar SubsidioCausado
        if( $m_ELEMENTO['IMPORTE'] && $m_ELEMENTO['CLAVE_P'] == '002' )
        {
          if ( $m_ELEMENTO['SUBSIDIO'] < $m_ELEMENTO['IMPORTE'] )
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El valor SubsidioCausado debe ser mayor o igual que el valor del Importe de la informaci&oacute;n de OtroPago";
        }

        //Validar Periodicidad
        if( $m_ELEMENTO['PERIODICIDAD_PAGO'] == 99 && $m_ELEMENTO['TIPO_NOMINA'] == 'O' )
        {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La Clave de n�mina no puede ser <b>Ordinario</b> cuando la periodicidad de pago es <b>Otra Periodicidad</b>";
        }

        //Validar Periodicidad
        if( $m_ELEMENTO['PERIODICIDAD_PAGO'] != 99 && $m_ELEMENTO['TIPO_NOMINA'] == 'E' )
        {
        	$GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "La Clave de n�mina no puede ser <b>Extraordinaria</b> cuando la periodicidad de pago es distinto de <b>Otra Periodicidad</b>";
        }

        break;
    }

    $e_NUEVOS_ERRORES = count($GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR']);

    return ( $e_NUEVOS_ERRORES == $e_MENSAJES_ERRORES );
  }

  private function add_atributo_sello_comprobante( $e_CADENA_XML )
  {
    //Cadena original del comprobante
    $e_CADENA     = $this->obtener_cadena_original_cfdi( $e_CADENA_XML );
    $e_CERT_KEY   = file_get_contents("{$this->PATH_CERTIFICADOS}/{$this->EMISOR[$this->ID_SUCURSAL_ACTIVA]['LLAVE_CERTIFICADO_ELECTRONICO']}");;
    $r_KEY        = openssl_pkey_get_private( $e_CERT_KEY );
    $e_CRYPTTEXT  = '';
    openssl_sign( $e_CADENA, $e_CRYPTTEXT, $r_KEY, 'SHA256' );

    //Cadena con el sello
    $e_SELLO              = base64_encode($e_CRYPTTEXT);
    $e_CADENA_XML_SELLADA = $this->agregar_atributo_sello_comprobante_cadena_xml($e_CADENA_XML, $e_SELLO);

    return $e_CADENA_XML_SELLADA;
  }

  private function obtener_cadena_original_cfdi( $e_CADENA_XML )
  {
    $o_XML = new DOMDocument();
    $o_XML->loadXML(utf8_encode($e_CADENA_XML));

    # Extrae cadena original
    $o_XSLT = new XSLTProcessor();
    $o_XSL  = new DOMDocument();
    $o_XSL->load( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_ADMIN']}/include/sitio_internet_sat/cfdiv33/xslt/cadena_original/cadenaoriginal_3_3.xslt", LIBXML_NOCDATA );

    error_reporting( 0 ); # Se deshabilitan los errores pues el xssl de la cadena esta en version 2 y eso genera algunos warnings
    $o_XSLT->importStylesheet( $o_XSL );
    $o_COMPROBANTE = $o_XML->getElementsByTagName( 'Comprobante' )->item( 0 );

    return $o_XSLT->transformToXML($o_COMPROBANTE);
  }

  private function agregar_atributo_sello_comprobante_cadena_xml( $e_CADENA_XML, $e_SELLO )
  {
    $o_XML = new DOMDocument();
    $o_XML->loadXML(utf8_encode( $e_CADENA_XML ) );

    $o_COMPROBANTE = $o_XML->getElementsByTagName('Comprobante')->item(0);
    $o_COMPROBANTE->setAttribute('Sello', $e_SELLO);

    $e_CADENA_XML = $o_XML->saveXML();
    return str_replace('<?xml version="1.0" encoding="UTF-8"?>', "<?xml version='1.0' encoding='UTF-8'?>", utf8_decode($e_CADENA_XML));
  }

  private function agregar_timbre_cadena_xml( $e_CADENA_XML_SELLADA, $e_CADENA_TIMBRE )
  {
    $o_XML = new DOMDocument();
    $o_XML->loadXML( utf8_encode( $e_CADENA_XML_SELLADA ) );

    $o_SOBRETIMBRE = new DOMDocument();
    $o_SOBRETIMBRE->loadXML( $e_CADENA_TIMBRE );

    $o_XMLTIMBRE = new DOMDocument( '1.0', 'UTF-8' );
    # Extrae el nodo
    $o_TIMBREFISCAL = $o_SOBRETIMBRE->getElementsByTagNameNS( 'http://www.sat.gob.mx/TimbreFiscalDigital', 'TimbreFiscalDigital' )->item( 0 );

    $o_TIMBREFISCAL = $o_XMLTIMBRE->importNode( $o_TIMBREFISCAL, true );
    $o_XMLTIMBRE->appendChild( $o_TIMBREFISCAL );

    $o_COMPLEMENTO = $o_XML->getElementsByTagName( 'Complemento' )->item( 0 );

    $o_TIMBREFISCAL_XML = $o_XMLTIMBRE->getElementsByTagNameNS('http://www.sat.gob.mx/TimbreFiscalDigital', 'TimbreFiscalDigital')->item( 0 );
    $o_TIMBREFISCAL_XML = $o_XML->importNode( $o_TIMBREFISCAL_XML, true) ;
    $o_COMPLEMENTO->appendChild( $o_TIMBREFISCAL_XML );
    $m_RESPUESTA['VALOR'] = $o_XML->saveXML();
    $m_RESPUESTA['VALOR'] = str_replace('schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital ', 'xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital ', $m_RESPUESTA['VALOR']);
    $m_RESPUESTA['VALOR'] = str_replace('<tfd:TimbreFiscalDigital ', "\t<tfd:TimbreFiscalDigital ", $m_RESPUESTA['VALOR']);
    $m_RESPUESTA['VALOR'] = str_replace('/></cfdi:Complemento>', "/>\r\n\t</cfdi:Complemento>", $m_RESPUESTA['VALOR']);
    return $m_RESPUESTA['VALOR'];
  }

  private function validar_esquema( $e_CADENA_XML )
  {
    return array('VALIDACION' => true, 'ERRORES' => '');

    libxml_use_internal_errors(true);
    $o_XML = new DOMDocument();
    $o_XML->loadXML(utf8_encode($e_CADENA_XML));

    $o_NODO_COMPLEMENTO            = $o_XML->getElementsByTagName('Complemento')->item(0);
    $o_NODO_COMERCIO_EXTERIOR      = $o_XML->getElementsByTagName('ComercioExterior')->item(0);
    $o_NODO_PAGOS                  = $o_XML->getElementsByTagName('Pagos')->item(0);
    $e_RESULTADO_COMPROBANTE       = true;
    $e_RESULTADO_COMERCIO_EXTERIOR = true;
    $e_RESULTADO_PAGOS10           = true;
    $e_CADENA_ERRORES_XML          = '';

    //VALIDACION NODO COMERCIO EXTERIOR
    if($o_NODO_COMERCIO_EXTERIOR)
    {
      $o_XML_CE  = new DomDocument('1.0', 'UTF-8');
      $o_NODE_CE = $o_XML_CE->importNode($o_NODO_COMERCIO_EXTERIOR, true);
      $o_XML_CE->appendChild($o_NODE_CE);
      if(!$o_XML_CE->schemaValidate("{$GLOBALS['SYSTEM']['CONFIG']['PATH_ADMIN']}include/sitio_internet_sat/cfdiv33/xsd/ComercioExterior11.xsd"))
      {
      	$errors = libxml_get_errors();
        $cont   = 1;
        $e_CADENA_ERRORES_XML .= "\r\n======================== ERRORES VALIDACION NODO COMERCIO EXTERIOR ======================\r\n";
        //Leer todos los errores generados
        foreach ($errors as $error)
          $e_CADENA_ERRORES_XML .= 'SQUEMA error (' . $cont++ . '): ' . $error->message . '" [' . $error->level . '] (Code ' . $error->code . ') in ' . $error->file . ' on line ' . $error->line . ' column ' . $error->column . "\r\n";

        $e_CADENA_ERRORES_XML .= "\r\n========================   FIN VALIDACION NODO COMERCIO EXTERIOR   ======================\r\n";

        libxml_clear_errors();
        libxml_use_internal_errors(false);
        unset($o_XML_CE, $o_NODE_CE, $errors, $cont);
        $e_RESULTADO_COMERCIO_EXTERIOR = false;
      }
    }

    //VALIDACION NODO PAGOS
    if( $o_NODO_PAGOS )
    {
      $o_XML_PG  = new DomDocument( '1.0', 'UTF-8' );
      $o_NODE_PG = $o_XML_PG->importNode( $o_NODO_PAGOS, true );
      $o_XML_PG->appendChild( $o_NODE_PG );
      if( !$o_XML_PG->schemaValidate( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_ADMIN']}include/sitio_internet_sat/cfdiv33/xsd/Pagos10.xsd" ) )
      {
    	  $errors = libxml_get_errors();
        $cont   = 1;
        $e_CADENA_ERRORES_XML  .= "\r\n============================== ERRORES VALIDACION NODO PAGOS ==========================\r\n";
        //Leer todos los errores generados
        foreach ($errors as $error)
          $e_CADENA_ERRORES_XML .= 'XML error (' . $cont++ . '): ' . $error->message . '" [' . $error->level . '] (Code ' . $error->code . ') in ' . $error->file . ' on line ' . $error->line . ' column ' . $error->column . "\r\n";

        $e_CADENA_ERRORES_XML .= "\r\n==============================   FIN VALIDACION NODO PAGOS   ==========================\r\n";

        libxml_clear_errors();
        libxml_use_internal_errors( false );
        unset( $o_XML_PG, $o_NODE_PG, $errors, $cont );
        $e_RESULTADO_PAGOS10 = false;
      }
    }

    //VALIDACION DOCUMENTO SIN NODO COMPLEMENTO
    if( $o_NODO_COMPLEMENTO )
    {
      $CENode = $o_NODO_COMPLEMENTO;
      $CENode->parentNode->removeChild($CENode);
    }

    if( !$o_XML->schemaValidate("{$GLOBALS['SYSTEM']['CONFIG']['PATH_ADMIN']}include/sitio_internet_sat/cfdiv33/xsd/cfdv33.xsd" ) )
    {
      $errors = libxml_get_errors();
      $cont   = 1;
      $e_CADENA_ERRORES_XML .= "\r\n====================== ERRORES VALIDACION DOCUMENTO =======================\r\n";
      //Leer todos los errores generados
      foreach ( $errors as $error )
        $e_CADENA_ERRORES_XML .= 'XML error (' . $cont++ . '): ' . $error->message . '" [' . $error->level . '] (Code ' . $error->code . ') in ' . $error->file . ' on line ' . $error->line . ' column ' . $error->column . "\r\n";

      $e_CADENA_ERRORES_XML .= "\r\n======================   FIN VALIDACION DOCUMENTO   =======================\r\n";
      libxml_clear_errors();
      libxml_use_internal_errors( false );
      $e_RESULTADO_COMPROBANTE = false;
    }

    if ( !( $e_RESULTADO_COMPROBANTE && $e_RESULTADO_COMERCIO_EXTERIOR && $e_RESULTADO_PAGOS10 ) )
      $GLOBALS[SYSTEM][OUTPUT][TOP][ERROR][] = "Problemas al validar el esquema, revise el xml generado";

    return array('VALIDACION' => ( $e_RESULTADO_COMPROBANTE && $e_RESULTADO_COMERCIO_EXTERIOR && $e_RESULTADO_PAGOS10 ), 'ERRORES' => $e_CADENA_ERRORES_XML);
  }

  private function obtener_unidad( $m_DATOS ) // array( 'ID_PRODUCTO' => $e_ID_PRODUCTO, 'ID_UNIDAD_EQUIVALENCIA' => $e_ID_UNIDAD_EQUIVALENCIA )
  {

    $e_QUERY  = "SELECT LIS_ABREVIACION AS CLAVE_UNIDAD_SAT, LIS_NOMBRE AS UNIDAD\n";
    $e_QUERY .= "  FROM sys_listas\n";

    if( $m_DATOS['ID_UNIDAD_EQUIVALENCIA'] )
    {
      switch( $this->OBTENER_UNIDAD )
      {
        case 'EQUIVALENCIA':
          $e_QUERY .= "  JOIN cat_productos_equivalencias ON PRO_EQU_UNIDAD = LIS_ID\n";
          $e_QUERY .= " WHERE PRO_EQU_ID = '{$m_DATOS['ID_UNIDAD_EQUIVALENCIA']}'";
          break;

        case 'DOCUMENTO':
          $e_QUERY .= " WHERE LIS_ID = '{$m_DATOS['ID_UNIDAD_EQUIVALENCIA']}'";
          break;
      }
    }
    else
    {
      $e_QUERY .= "  JOIN cat_productos ON CAM_PER_UNIDAD = LIS_ID\n";
      $e_QUERY .= " WHERE PRO_ID = '{$m_DATOS['ID_PRODUCTO']}'";
    }

    $m_RETURN = $GLOBALS['SYSTEM']['DBS']['DB']->get_array('MY_QUERY', $e_QUERY);

    if ( !is_array( $m_RETURN ) )
    {
      //validar configuracion de campo
      switch( $this->OBTENER_UNIDAD )
      {
        case 'EQUIVALENCIA':
        case 'DOCUMENTO':
         $e_TIPO_PRODUCTO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'PRO_GRU_TIPO', "SELECT PRO_GRU_TIPO FROM cat_productos_grupos JOIN cat_productos ON PRO_GRU_ID = PRO_GRUPO WHERE PRO_ID = '{$m_DATOS['ID_PRODUCTO']}'");
         $e_UNIDAD = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY','LIS_ABREVIACION',"SELECT LIS_ABREVIACION FROM sys_listas WHERE LIS_CATALOGO = 'UNIDAD_MEDIDA' AND LIS_NOMBRE = 'UNIDAD' AND LIS_ACTIVO = '1'");
         if( $e_TIPO_PRODUCTO == 'SERVICIO')
           $m_RETURN = array('CLAVE_UNIDAD_SAT' => 'E48', 'UNIDAD' => 'Unidad de servicio');
         else
           $m_RETURN = array('CLAVE_UNIDAD_SAT' => $e_UNIDAD ? $e_UNIDAD: 'XUN', 'UNIDAD' => 'Unidad');
          break;
        default:
          $m_RETURN = false;
      }
    }

    return $m_RETURN;
  }

  private function obtener_addendas( $e_SIGLA_STATUS,$e_NOMBRE_ADDENDA,$e_XML, $m_DATOS_EXTRAS = array() )
  {
    require_once( "{$GLOBALS['SYSTEM']['CONFIG']['PATH_SYSTEM']}/include/lib/php/facturacion_electronica/addendas/" . strtolower( $e_NOMBRE_ADDENDA ) . ".php" );
    $e_CADENA_XML = obtener_addenda_externa( $e_XML, $this, $e_SIGLA_STATUS );
    return $e_CADENA_XML;
  }

  private function obtener_numero_pedimento( $e_ID_DOCUMENTO, $e_ID_DETALLE='' )
  {
    $e_FILTRO_DETALLES = $e_ID_DETALLE ? "AND DOC_REL_DETALLE='{$e_ID_DETALLE}'" : '';

    $e_EXISTE_CAMPO_ADUANDA = $GLOBALS['SYSTEM']['DBS']['DB']->get_field( 'MY_QUERY', 'EXISTE', " SELECT COUNT(*) AS EXISTE FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'INV_MOV_PEDIMENTO_ADUANA' AND TABLE_NAME = 'dat_inventario_movimientos' AND TABLE_SCHEMA = '{$GLOBALS['SYSTEM']['DBS']['DB']->NAME}'");
    if( !$e_EXISTE_CAMPO_ADUANDA )
      return false;

    if( $GLOBALS[SYSTEM][CONFIG][ALIAS] == 'multiplastic' )
    {
      $e_ID_AFECTACION     = Utils::catalogo_obtener_atributo('sys_status_afectaciones_inventario', 'STA_AFE_INV_ID', 'STA_AFE_INV_NOMBRE', 'PEDAP_CAP - Salida (FISICO)');
      $e_ID_RELACION       = Utils::catalogo_obtener_atributo('sys_status_relaciones', 'STA_REL_ID', 'STA_REL_NOMBRE', 'Pedido por entregar a factura');

      $e_QUERY_PEDIMENTOS  = " SELECT INV_MOV_PEDIMENTO AS PEDIMENTO, INV_MOV_PEDIMENTO_FECHA AS FECHA, ENT_NOMBRE AS ADUANA\r\n";
      $e_QUERY_PEDIMENTOS .= " FROM doc_documentos_relacionados\r\n";
      $e_QUERY_PEDIMENTOS .= " JOIN dat_inventario_movimientos ON DOC_REL_DOCUMENTO_PADRE=INV_MOV_DOCUMENTO AND DOC_REL_DETALLE_PADRE=INV_MOV_DETALLE AND INV_MOV_AFECTACION = '{$e_ID_AFECTACION}'\r\n";
      $e_QUERY_PEDIMENTOS .= " LEFT JOIN cat_entidades ON INV_MOV_PEDIMENTO_ADUANA = ENT_ID\r\n";
      $e_QUERY_PEDIMENTOS .= " WHERE DOC_REL_DOCUMENTO='{$e_ID_DOCUMENTO}' AND DOC_REL_RELACION='{$e_ID_RELACION}' {$e_FILTRO_DETALLES} AND INV_MOV_PEDIMENTO!=''\r\n";
      $e_QUERY_PEDIMENTOS .= " GROUP BY INV_MOV_PEDIMENTO, INV_MOV_PEDIMENTO_FECHA, ENT_NOMBRE\r\n";
      $e_QUERY_PEDIMENTOS .= " ORDER BY INV_MOV_ID ASC";
    }
    else
    {
      $e_ID_AFECTACION     = Utils::catalogo_obtener_atributo('sys_status_afectaciones_inventario', 'STA_AFE_INV_ID', 'STA_AFE_INV_NOMBRE', 'COM_CAP - Entrada (FISICO)');
      $e_ID_RELACION       = Utils::catalogo_obtener_atributo('sys_status_relaciones', 'STA_REL_ID', 'STA_REL_NOMBRE', 'COMPRA - VENTA');

      $e_QUERY_PEDIMENTOS  = "   SELECT INV_MOV_PEDIMENTO AS PEDIMENTO, INV_MOV_PEDIMENTO_FECHA AS FECHA, ENT_NOMBRE AS ADUANA\r\n";
      $e_QUERY_PEDIMENTOS .= "     FROM dat_inventario_movimientos\r\n";
      $e_QUERY_PEDIMENTOS .= "LEFT JOIN cat_entidades ON INV_MOV_PEDIMENTO_ADUANA = ENT_ID\r\n";
      $e_QUERY_PEDIMENTOS .= "    WHERE INV_MOV_DOCUMENTO = '{$this->DOCUMENTO_PADRE->ID}'\r\n";
      $e_QUERY_PEDIMENTOS .= "      AND INV_MOV_ENCABEZADO = '{$this->DOCUMENTO_PADRE->ID_ENCABEZADO}'\r\n";
      $e_QUERY_PEDIMENTOS .= "      AND INV_MOV_TIPO='S'\r\n";
      $e_QUERY_PEDIMENTOS .= "      AND INV_MOV_DETALLE = '{$e_ID_DETALLE}'\r\n";
      $e_QUERY_PEDIMENTOS .= "      AND INV_MOV_PEDIMENTO != ''\r\n";
      $e_QUERY_PEDIMENTOS .= " GROUP BY INV_MOV_PEDIMENTO\r\n";
      $e_QUERY_PEDIMENTOS .= " ORDER BY INV_MOV_ID ASC";
    }

    if(!$e_ID_AFECTACION || !$e_ID_RELACION)
    {
      return false;
    }

    $m_PEDIMENTOS = array();

    if($r_PEDIMENTOS = $GLOBALS['SYSTEM']['DBS']['DB']->get_resource('MY_QUERY', $e_QUERY_PEDIMENTOS))
    {
      while($a_PEDIMENTOS = $GLOBALS['SYSTEM']['DBS']['DB']->fetch_array($r_PEDIMENTOS))
      {
        $e_NUMERO_PEDIMENTO = $a_PEDIMENTOS['PEDIMENTO'];

        if(!preg_match('/^([0-9]{2})([0-9]{2})([0-9]{4})([0-9])([0-9]{6})$/',$e_NUMERO_PEDIMENTO,$matches))
        {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El <b>n&uacute;mero de pedimento</b> no es v&aacute;lido, favor de corregir.";
          return  false;
        }
        else
        {
          //A�O DE VALIDACION
          $m_ENCONTRADO = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_array('MY_QUERY', "SELECT EJERCICIO FROM cat_cfdi_numpedimentoaduana WHERE EJERCICIO LIKE '%$matches[1]'");
          if( !$m_ENCONTRADO )
             $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; el A&ntilde;o de validaci&oacute;n <b>$matches[1]</b> en los cat&aacute;logos del SAT";

          //ADUANA
          if ( !$this->validar_valor_uncatalogo_sat( 'numpedimentoaduana', 'ADUANA', $matches[2] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la Aduana <b>{$matches[2]}</b> en los cat&aacute;logos del SAT";

          //NUMERO DE LA PATENTE
          if ( !$this->validar_valor_uncatalogo_sat( 'numpedimentoaduana', 'PATENTE', $matches[3] ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la Patente <b>{$matches[3]}</b> en los cat&aacute;logos del SAT";

          //A�O EN CURSO substr($string, -1);
          $e_ANNIO_ACTUAL = substr( date( "y" ), -1 );
          if ( ( $matches[4] > $e_ANNIO_ACTUAL ) ||  ( $matches[4] < ( $e_ANNIO_ACTUAL - 3 ) ) )
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "El d&iacute;gito del A&ntilde;o en curso <b>{$matches[4]}</b> es inv&aacute;lido para el n&uacute;mero de pedimento";

          //consultar convinacion correcta
          $e_CANTIDAD = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field( 'MY_QUERY', 'CANTIDAD', "SELECT CANTIDAD  FROM cat_cfdi_numpedimentoaduana WHERE EJERCICIO LIKE '%$matches[1]' AND ADUANA = '{$matches[2]}' AND PATENTE LIKE '%{$matches[3]}'");

          if( $e_CANTIDAD )
          {
            //NUMERACION PROGRESIVA
            if($matches[5]<=$e_CANTIDAD)
              $e_CADENA_PEDIMENTO = $matches[1]."  ".$matches[2]."  ".$matches[3]."  ".$matches[4].$matches[5];
            else
              $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; La numeraci&oacute;n progresiva <b>{$matches[4]}</b> en los cat&aacute;logos del SAT";
          }
          else
          {
            $e_CADENA_PEDIMENTO = $matches[1]."  ".$matches[2]."  ".$matches[3]."  ".$matches[4].$matches[5];
            $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; El n&uacute;mero de pedimento <b>{$e_CADENA_PEDIMENTO}</b> en los cat&aacute;logos del SAT";
          }
        }
        $m_PEDIMENTOS[] = array('NUMERO' => $e_CADENA_PEDIMENTO );
      }
    }
    else
      return false;

    return $m_PEDIMENTOS;
  }

  private function obtener_nodo_comercio_exterior( $e_SIGLA_STATUS )
  {
    //======================== Prueba para obtener Tipo Cambio  Inicio =======================
    $e_PREFIJO            = $this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->CONF[PREFIJO];
    $e_FECHA_DOCUMENTO    = $this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->DATA["{$e_PREFIJO}_FECHA"];
    list($year,$mon,$day) = explode('-',$e_FECHA_DOCUMENTO);
    $e_DIA_ANTERIOR       = date('d/m/Y',mktime(0,0,0,$mon,$day-1,$year));
    $e_URL                = "http://dof.gob.mx/indicadores_detalle.php?cod_tipo_indicador=158&dfecha={$e_DIA_ANTERIOR}&hfecha={$e_DIA_ANTERIOR}";

    $r_PROCESS            = curl_init( $e_URL);
    curl_setopt( $r_PROCESS, CURLOPT_HTTPHEADER, array( 'Content-Type: text/xml', 'charset=utf-8' ) );
    curl_setopt( $r_PROCESS, CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $r_PROCESS, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $r_PROCESS, CURLOPT_POST, true );
    $e_RESPUESTA = curl_exec($r_PROCESS);
    curl_close($r_PROCESS);

    if ( preg_match ('/width="[0-9]{1,3}%" align="center" class="txt">([0-9]{1,3}\.[0-9]{4,6})/', $e_RESPUESTA, $m_MATCHES ) )
      $e_VALOR_TIPO_CAMBIO = $m_MATCHES[1];

    //======================== Fin ===========================================================

    $e_ERROR = 0;
    $e_EXISTE_CAMPO_ARANCELARIA = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'CAM_PER_ID', "SELECT CAM_PER_ID  FROM sys_campos_personalizados WHERE CAM_PER_CAMPO='CAM_PER_PARTIDA_ARANCELARIA' AND CAM_PER_TABLA = 'cat_productos' and CAM_PER_ACTIVO='1'");//
    if(!$e_EXISTE_CAMPO_ARANCELARIA)
      return false;

    $e_DECIMALES_MONEDA_DOCUMENTO = $this->obtener_decimales_validos_documento($e_SIGLA_STATUS);
    $e_AGRUPACION    = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('QUERY_CFDI_PAIS', 'AGRUPACIONES', "AND PAIS = '{$this->RECEPTOR['SIGLA_PAIS']}'");

    if($e_AGRUPACION == 'Uni�n Europea' )
      //si la sigla del pais tiene Agrupacion crear campo
      $e_NUMERO_EXPORTADOR = "NumeroExportadorConfiable=' A569874-35' ";
    else
      $e_NUMERO_EXPORTADOR = '';

    $e_EXISTE_CAMPO_INCOTERM = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'STA_CAM_ID', "SELECT STA_CAM_ID  FROM sys_status_campos WHERE STA_CAM_CAMPO='PEDN_PEN_INCOTERM' AND STA_CAM_ACTIVO='1'");//

    if( !$e_EXISTE_CAMPO_INCOTERM )
      return false;

    if($this->TIPO_DOCUMENTO == 'FACTURA')
      $e_UNION_TABLAS = "JOIN doc_documentos_relacionados AS pedido_por_entregar ON pedido_por_entregar.DOC_REL_DOCUMENTO = DOC_DOC_ID";
    elseif($this->TIPO_DOCUMENTO == 'NDC')
    {
      $e_UNION_TABLAS  = "JOIN doc_documentos_relacionados AS factura ON factura.DOC_REL_DOCUMENTO = DOC_DOC_ID\r\n";
      $e_UNION_TABLAS .= "JOIN doc_documentos_relacionados AS pedido_por_entregar ON pedido_por_entregar.DOC_REL_DOCUMENTO = factura.DOC_REL_DOCUMENTO_PADRE";
    }

    $e_QUERY_INCOTERM  = "SELECT ENT_NOMBRE\r\n";
    $e_QUERY_INCOTERM .= "  FROM doc_documentos\r\n";
    $e_QUERY_INCOTERM .= "{$e_UNION_TABLAS}\r\n";
    $e_QUERY_INCOTERM .= "  JOIN doc_documentos_relacionados AS pedido_por_surtir ON pedido_por_surtir.DOC_REL_DOCUMENTO = pedido_por_entregar.DOC_REL_DOCUMENTO_PADRE\r\n";
    $e_QUERY_INCOTERM .= "  JOIN doc_pedn ON PEDN_DOCUMENTO = pedido_por_surtir.DOC_REL_DOCUMENTO_PADRE\r\n";
    $e_QUERY_INCOTERM .= "  JOIN cat_entidades ON ENT_ID = PEDN_PEN_INCOTERM\r\n";
    $e_QUERY_INCOTERM .= " WHERE DOC_DOC_ID = '{$this->DOCUMENTO_PADRE->ID}'\r\n";
    $e_QUERY_INCOTERM .= " GROUP BY DOC_DOC_ID";

    //SOLO PARA MP
    $e_NUM_ICOTERM     = $GLOBALS[SYSTEM][DBS][DB]->get_field('MY_QUERY', 'ENT_NOMBRE', $e_QUERY_INCOTERM);
    $e_MON_ID          = $GLOBALS[SYSTEM][DBS][DB]->get_field('MY_QUERY', 'MON_ID', "SELECT MON_ID FROM cat_monedas WHERE MON_ABREVIATURA ='USD' AND MON_ACTIVO='1'");
    $e_FECHA           = Tool::str_to_date($this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->POST["{$this->DOCUMENTO_PADRE->STATUS[$e_SIGLA_STATUS]->CONF[PREFIJO]}_FECHA"]);

    if( ( $GLOBALS[SYSTEM][CONFIG][ALIAS] == 'multiplastic' ) || ( $GLOBALS[SYSTEM][CONFIG][ALIAS] == 'tmp_sandbox' ) )
      $e_TIPO_CAMBIO_USA = $GLOBALS[SYSTEM][DBS][DB]->get_field('MY_QUERY', 'TIP_CAM_OFI_TIPO_CAMBIO', "SELECT TIP_CAM_OFI_TIPO_CAMBIO FROM cat_per_tipo_cambio_oficial WHERE TIP_CAM_OFI_FECHA = '$e_FECHA' AND TIP_CAM_OFI_MONEDA = '{$e_MON_ID}'");
    else
      $e_TIPO_CAMBIO_USA = Monedas::tdc( $e_MON_ID, $e_FECHA );


    if( $e_TIPO_CAMBIO_USA != $e_VALOR_TIPO_CAMBIO )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Nodo ComercioExterior: Tipo de cambio Incorrecto, validado en el DOF tipo de cambio sugerido es: <b>{$e_VALOR_TIPO_CAMBIO}</b>";
      $e_ERROR++;
      return false;
    }

    $m_CONCEPTOS         = $this->generar_elemento_conceptos($e_SIGLA_STATUS);
    $m_TOTALES_DOCUMENTO = array_pop( $m_CONCEPTOS );
    $e_TOTAL_DOCUMENTO   = ( $m_TOTALES_DOCUMENTO['SUBTOTAL'] - $m_TOTALES_DOCUMENTO['DESCUENTO'] ) + ( $m_TOTALES_DOCUMENTO['IMPUESTOS']['TRASLADADOS']['GRAN_TOTAL'] - $m_TOTALES_DOCUMENTO['IMPUESTOS']['RETENIDOS']['GRAN_TOTAL'] );
    $e_TOTAL_USD         = $e_TOTAL_DOCUMENTO;

    //Validaciones
    if($this->EMISOR[0]['SIGLA_PAIS'] != 'MEX')
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Nodo ComercioExterior: No se encontr&oacute; la <b>abreviatura del pais del emisor</b> en los cat&aacute;logos del SAT";
      $e_ERROR++;
    }

    if ( !$this->validar_valor_uncatalogo_sat( 'estado', 'ESTADO', $this->EMISOR[0]['SIGLA_ESTADO'] ))
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Nodo ComercioExterior: No se encontr&oacute; la <b>abreviatura del estado del emisor</b> en los cat&aacute;logos del SAT";
      $e_ERROR++;
    }

    if ( empty( $this->RECEPTOR['SIGLA_ESTADO'] ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Nodo ComercioExterior: El campo <b>Estado del Receptor</b> es requerido";
      $e_ERROR++;
    }
    if( empty( $e_NUM_ICOTERM ) )
    {
      $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Nodo ComercioExterior: El campo <b>Incoterm</b> es requerido";
      $e_ERROR++;
    }

    $e_XML_COMERCIO = "\r\n\t\t<cce11:ComercioExterior xmlns:cce11='http://www.sat.gob.mx/ComercioExterior11' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' Version='1.1' TipoOperacion='2' ClaveDePedimento='A1' CertificadoOrigen='0' {$e_NUMERO_EXPORTADOR}Incoterm='{$e_NUM_ICOTERM}' Subdivision='0' TipoCambioUSD='{$e_TIPO_CAMBIO_USA}' TotalUSD='{$this->remplazar_caracteres_xml( $this->formatear_decimales_hacienda( $e_TOTAL_USD, 2 ) ) }' xsi:schemaLocation='http://www.sat.gob.mx/ComercioExterior11 http://www.sat.gob.mx/sitio_internet/cfd/ComercioExterior11/ComercioExterior11.xsd'>";
    if( $this->EMISOR[0]['CURP'] )
      $e_XML_COMERCIO .= "\r\n\t\t\t<cce11:Emisor Curp='{$this->EMISOR[0]['CURP']}'>";
    else
      $e_XML_COMERCIO .= "\r\n\t\t\t<cce11:Emisor >";

    $e_XML_COMERCIO .= "\r\n\t\t\t\t<cce11:Domicilio Calle='{$this->EMISOR[0]['CALLE']}' CodigoPostal='{$this->EMISOR[0]['CODIGO_POSTAL']}' Colonia='{$this->DATOS_EMISOR_NODO_CE['COLONIA']}' Estado='{$this->EMISOR[0]['SIGLA_ESTADO']}' Localidad='{$this->DATOS_EMISOR_NODO_CE['LOCALIDAD']}' Municipio='{$this->DATOS_EMISOR_NODO_CE['MUNICIPIO']}' Pais='{$this->EMISOR[0]['SIGLA_PAIS']}'/>";
    $e_XML_COMERCIO .= "\r\n\t\t\t</cce11:Emisor>";
    $e_XML_COMERCIO .= "\r\n\t\t\t<cce11:Receptor>";
    $e_XML_COMERCIO .= "\r\n\t\t\t\t<cce11:Domicilio Calle='{$this->RECEPTOR['CALLE_NUMERO']}' Estado='{$this->RECEPTOR['SIGLA_ESTADO']}' Pais='{$this->RECEPTOR['SIGLA_PAIS']}' CodigoPostal='{$this->RECEPTOR['COD_POSTAL']}'/>";
    $e_XML_COMERCIO .= "\r\n\t\t\t</cce11:Receptor>";
    $e_XML_COMERCIO .= "\r\n\t\t\t<cce11:Mercancias>";

    foreach( $m_CONCEPTOS as $a_DETALLE )
    {
      $e_FRACCION_ARANCELARIA = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'CAM_PER_PARTIDA_ARANCELARIA', "SELECT CAM_PER_PARTIDA_ARANCELARIA FROM cat_productos WHERE PRO_ID = '{$a_DETALLE['ID_PRODUCTO']}' AND PRO_ACTIVO = '1'");
      if( $e_FRACCION_ARANCELARIA )
      {
        $e_UNIDAD_ADUANA = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('MY_QUERY', 'UNIDAD_MEDIDA', "SELECT UNIDAD_MEDIDA FROM cat_cfdi_fraccionarancelaria WHERE FRACCION_ARANCELARIA = '{$e_FRACCION_ARANCELARIA}'");
        if( !$e_UNIDAD_ADUANA )
        {
          $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la unidad respecto a la fraccion arancelaria en los cat&aacute;logos del SAT";
          $e_ERROR++;
        }
        else
        {
          $e_DESCRIPCION_UNIDAD = $GLOBALS['SYSTEM']['DBS']['DB_SAT']->get_field('MY_QUERY', 'DESCRIPCION', "SELECT DESCRIPCION FROM cat_cfdi_unidadaduana WHERE UNIDAD_MEDIDA = '{$e_UNIDAD_ADUANA}'");
          //revisar si es kilogramo
          if( $e_DESCRIPCION_UNIDAD == 'KILO' )
          {
            $e_PESO = $GLOBALS['SYSTEM']['DBS']['DB']->get_field('MY_QUERY', 'PESO', "SELECT MED_PRO_EMP_PESO AS PESO FROM cat_per_medidas_productos_empaques JOIN sys_listas ON LIS_ID = MED_PRO_EMP_UNIDAD WHERE MED_PRO_EMP_PRODUCTO = '{$a_DETALLE['ID_PRODUCTO']}' AND MED_PRO_EMP_ACTIVO = '1'");
            if( $e_PESO )
            {
              $e_CANTIDAD_ADUANA = $a_DETALLE['CANTIDAD'] *( $e_PESO/1000);
              $e_VALOR_UNITARIO_KILOS=$a_DETALLE['IMPORTE']/$e_CANTIDAD_ADUANA;
              $e_VALOR_UNITARIO = $e_VALOR_UNITARIO_KILOS;
            }
            else
            {
              $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "Nodo Comercio Exterior: El producto no tiene regitrado un peso";
              $e_ERROR++;
            }
          }
          else
          {
            $e_CANTIDAD_ADUANA = $a_DETALLE['CANTIDAD'];
			      $e_VALOR_UNITARIO = $a_DETALLE['VALORUNITARIO']/$e_CANTIDAD_ADUANA;
          }
        }
      }
      else
      {
         $GLOBALS['SYSTEM']['OUTPUT']['TOP']['ERROR'][] = "No se encontr&oacute; la fraccion arancelaria en los cat&aacute;logos del SAT";
         $e_ERROR++;
      }

      $e_VALOR_DOLARES = $a_DETALLE['IMPORTE'];
      $e_XML_COMERCIO .= "\r\n\t\t\t\t<cce11:Mercancia NoIdentificacion='{$a_DETALLE['NOIDENTIFICACION']}' CantidadAduana='{$this->formatear_decimales_hacienda( $e_CANTIDAD_ADUANA, $e_DECIMALES_MONEDA_DOCUMENTO + 1 )}' UnidadAduana='{$e_UNIDAD_ADUANA}' FraccionArancelaria='{$e_FRACCION_ARANCELARIA}' ValorUnitarioAduana='{$this->formatear_decimales_hacienda( $e_VALOR_UNITARIO, $e_DECIMALES_MONEDA_DOCUMENTO )}' ValorDolares='{$this->formatear_decimales_hacienda( $e_VALOR_DOLARES, $e_DECIMALES_MONEDA_DOCUMENTO )}'/>";
    }

    $e_XML_COMERCIO.= "\r\n\t\t\t</cce11:Mercancias>";
    $e_XML_COMERCIO.= "\r\n\t\t</cce11:ComercioExterior>";

    if( $e_ERROR )
      return false;
    else
      return $e_XML_COMERCIO;
  }
}
?>
